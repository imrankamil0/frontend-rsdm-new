<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Mobile Web-app fullscreen -->

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">

    <!-- Meta tags -->

    <meta name="description" content="Rumah Sunat dr Mahdian pendaftraan online">
    <meta name="author" content="DokterM">
    <link rel="icon" href="/assets/svg/favicon.ico">

    <!-- Title -->

    <title><?= (!empty($title)) ? $title : 'RSDM - Pendaftaran' ?></title>

    <!-- lib stylesheets -->

    <link rel="stylesheet" media="all" href="/assets/css/lib_css/animate.css" />
    <link rel="stylesheet" media="all" href="/assets/css/lib_css/font-awesome.css" />
    <link rel="stylesheet" media="all" href="/assets/css/lib_css/linear-icons.css" />
    <link rel="stylesheet" media="all" href="/assets/css/lib_css/owl.carousel.css" />
    <link rel="stylesheet" media="all" href="/assets/css/lib_css/jquery.lavalamp.css" />

    <!-- Template stylesheets -->

    <link rel="stylesheet" media="all" href="/assets/css/style.css" />
    <link rel="stylesheet" media="all" href="/assets/css/my_style.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

    <!--HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!--[if lt IE 9]><link rel="stylesheet" type="text/css" href="https://npmcdn.com/flatpickr/dist/ie.css"><script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script> <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

    <?= $this->renderSection('lib_css') ?>

    <style type="text/css">
        .pap-backdrop.show {
            opacity: 0.5;
            display: block;
        }

        .pap-backdrop {
            display: none;
            position: fixed;
            top: 0;
            left: 0;
            z-index: 1040;
            width: 100vw;
            height: 100vh;
            background-color: #000;
            transition: opacity 0.15s linear;
        }
    </style>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-194909498-14"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-194909498-14');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '578994843378538');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=578994843378538&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->

</head>

<body>
    <!-- <div class="loader">
        <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div> -->

    <?= $this->include('includes/header') ?>

    <?= $this->renderSection('content') ?>

    <div class="modal fade" id="modal_add_success" tabindex="-1" aria-labelledby="modal_add_successLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 300px;">
            <div class="modal-content border-0">

                <div class="modal-body py-5">
                    <center>
                        <img src="<?= base_url() ?>/assets/images/add-cart.png" class="mb-2 img-fluid rounded" alt="gambar add sukses" style="max-width:160px; max-height:160px">

                        <h5 class="ff-poppins fw-b mt-2 mb-1">1+ Layanan</h5>

                        <p class="text-muted mb-4" style="font-size: 14px;">Telah ditambahkan ke keranjang</p>

                        <button id="tambah-keranjang" class="btn btn-primary btn-sm rounded box-shadow-none btn-block toggle-show" data-show="cartComponent">Lihat Keranjang</button>
                    </center>
                </div>

            </div>
        </div>
    </div>


    <div class="modal fade" id="modal_belum_login" tabindex="-1" aria-labelledby="modal_belum_loginLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 400px;">
            <div class="modal-content border-0">

                <div class="modal-body py-5">
                    <center>
                        <img src="<?= base_url() ?>/assets/images/pengguna-baru.png" class="mb-2 img-fluid rounded" alt="gambar Belum login" style="max-width:160px; max-height:160px">

                        <h5 class="ff-poppins fw-b mt-2 mb-1">Apakah Anda pengguna baru?</h5>
                        </h5>

                        <p class="text-muted mb-4" style="font-size: 14px;">Silahkan daftar terlebih dahulu untuk melanjutkan transaksi</p>

                        <div class="d-flex justify-content-center align-items-center">
                            <button type="button" class="btn btn-sm btn-outline-secondary belum-login" data-toggle="modal" data-target="#loginComponent" style="width: 130px;">Sign In</button>
                            <span class="mx-2 justify-self-center">atau</span>
                            <button class="btn btn-primary btn-sm rounded box-shadow-none belum-login" data-toggle="modal" data-target="#CreateComponent" style="width: 130px;">Daftar Sekarang</button>
                        </div>
                    </center>
                </div>

            </div>
        </div>
    </div>

    <?= $this->include('includes/footer') ?>

    <!-- lib Scripts -->
    <script src="/assets/js/lib_js/jquery.min.js"></script>
    <script src="/assets/js/lib_js/bootstrap.bundle.js"></script>
    <script src="/assets/js/lib_js/in-view.min.js"></script>
    <script src="/assets/js/lib_js/jquery.lavalamp.js"></script>
    <script src="/assets/js/lib_js/owl.carousel.js"></script>
    <script src="/assets/js/lib_js/rellax.js"></script>
    <script src="/assets/js/lib_js/wow.js"></script>
    <script src="/assets/js/lib_js/tabzy.js"></script>
    <script src="/assets/js/lib_js/isotope.pkgd.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js" integrity="sha512-qTXRIMyZIFb8iQcfjXWCO8+M5Tbc38Qi5WzdPOYZHIlZpzBHG3L3by84BBBOiRGiEb7KKtAOAs5qYdUiZiQNNQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <?= $this->renderSection('lib_js') ?>

    <!-- Template Scripts -->
    <script>
        const TOKEN = "<?= session('token') ?>"
        const IS_LOGIN = <?= (session('is_login')) ? 'true' : 'false' ?>;
        const MY_PROVINSI = <?= (session('id_provinsi')) ? session('id_provinsi') : '0' ?>;

        const MY_DATA = {
            'name': "<?= session('name') ?>",
            'email': "<?= session('email') ?>",
            'nik': "<?= session('nik') ?>",
            'telepon': "<?= session('telepon') ?>",
            'tanggal_lahir': "<?= date('Y-m-d', strtotime(session('tanggal_lahir'))) ?>",
            'berat_badan': "<?= session('berat_badan') ?>",
            'tinggi_badan': "<?= session('tinggi_badan') ?>",
            'alamat': "<?= session('alamat') ?>"
        }

        // Data 
        const LIST_DATA = <?= json_encode($list_data) ?>;
        const LIST_PROVINSI = <?= json_encode($list_provinsi) ?>;
        const LIST_KABUPATENKOTA = <?= json_encode($list_kabupatenkota) ?>;
        const DATE_NOW = '<?= date('Y-m-d') ?>';

        console.log('LIST : ', LIST_PROVINSI, LIST_KABUPATENKOTA)
    </script>

    <script src="/assets/js/main.js"></script>
    <script src="/assets/js/custom.js"></script>

    <script src="/assets/js/config.js"></script>
    <script src="/assets/js/auth.js"></script>

    <!-- <script src="/assets/js/cart.js"></script> -->
    <script src="/assets/js/backend_api.js"></script>
    <script src="/assets/js/my_cart.js"></script>

    <?= $this->renderSection('script') ?>


    <script type="text/javascript">
        <?php if (!empty(session('notif'))) echo 'alert("' . session('notif')[1] . '")' ?>


        $("#clickme").click(function() {
            var result = confirm("Apakah Anda yakin untuk menghapus?");

            if (result) {
                $("#book").hide("slow", function() {

                });
            } else {
                // Do nothing; they cancelled
            }

        });
        $("#clickme-2").click(function() {
            var result = confirm("Apakah Anda yakin untuk menghapus?");

            if (result) {
                $("#book-2").hide("slow", function() {

                });
            } else {
                // Do nothing; they cancelled
            }

        });

        $("#BuatAkun").click(function() {
            $("#loginComponent").modal("hide");
            return;
        });

        $("#lupa-password").click(function() {
            $("#loginComponent").modal("hide");
            return;
        });

        $(".belum-login").click(function() {
            $("#modal_belum_login").modal("hide");
            return;
        });
        $("#kirim-instruksi").click(function() {
            $("#ForgotPassword").modal("hide");
            return;
        });

        $("#blm-login").click(function() {
            $("#cartComponent").removeClass("show");
            return;
            $("#modal_belum_login").modal("show");
            return;

        });
        $("#tambah-keranjang").click(function() {
            $("#modal_add_success").modal("hide");



        });

        $(document).ready(function() {
            refresh_produk_keranjang()
            // $(".flatficker_input").flatpickr();
        });

        function toggle_password(el) {
            let el_pass = $(el).parent().next();
            if (el_pass.prop('type') == 'password') {
                el_pass.prop('type', 'text')
                return
            }

            el_pass.prop('type', 'password')
        }

        function get_params(params){
            let url_string = window.location.href;
            let url = new URL(url_string);
            return url.searchParams.get(params)
        }
    </script>

    <div class="pap-backdrop"></div>

</body>

</html>