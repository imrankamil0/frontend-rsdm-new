<?php //dd($ringkasan); 
?>
<?= $this->extend('template') ?>

<?= $this->section('content') ?>
<section class="section bg-white">
    <div class="container nav-container-fi bo-shd">
        <div class="row justify-content-center">
            <div class="col-lg-12 pl-4 pr-4 mt-4">
                <!-- <a class="text-left text-muted" href="index.php?pages=orderdetail"><i class="fa fa-angle-left" aria-hidden="true"></i> Kembali</a> -->
                <h2 class="text-primary mb-3 text-center ff-poppins">
                    <strong>Ringkasan</strong>
                </h2>
                <hr class="mt-1">
            </div>

        </div>
        <div class="row justify-content-center">
            <div class="col-lg-9 pl-4 pr-4">
                <p class="text-muted mb-2">
                    Layanan didampingi oleh:
                </p>
                <p class="ff-poppins">
                    <strong><?= session('name') ?></strong>
                    <br>
                    <span><?= session('telepon') ?></span>
                </p>

            </div>
        </div>

        <hr class="mt-3">

        <?php $total = 0;
        $total_potongan = 0;
        $total_biaya_uniq_code = 0;
        if (!empty($ringkasan['orderdetail'])) : ?>
            <?php $i = 1;
            foreach ($ringkasan['orderdetail'] as $r) : ?>
                <div class="row justify-content-center mt-3 ring-data pt-3">
                    <div class="col-lg-2 col-3 pl-4 pr-4 img-data">
                        <?php if (!empty($r['productklinik']['image_product_klinik'])) : ?>
                            <img src="<?= $r['productklinik']['image_product_klinik'] ?>" class="img-fluid img-ringkasan">
                        <?php endif ?>
                    </div>
                    <div class="col-lg-7 col-9 pl-4 pr-4 data-data">
                        <h6 class="mb-0 ff-poppins text-muted">Pasien ke-<?= $i++ ?></h6>
                        <p style="font-size: 1.5rem" class="text-primary mb-2 ff-poppins"><strong><?= (!empty($r['productklinikdetail']['name'])) ? $r['productklinikdetail']['name'] : 'Default Nama Produk' ?></strong></p>
                        <table class="data-pasien">
                            <tbody>
                                <tr>
                                    <td class="text-muted" data-label="Nama Pasien">Nama Pasien</td>
                                    <td data-label="Nama Pasien">:<strong> <?= (!empty($r['datapasien']['nama'])) ? $r['datapasien']['nama'] : 'Default Nama Pasien' ?></strong></td>
                                </tr>
                                <tr>
                                    <td class="text-muted" scope="row" data-label="Lokasi Klinik">Lokasi Klinik</td>
                                    <td data-label="Kota Tangerang - Kelapa Indah">:<strong> <?= $r['klinik']['name'] ?></strong></td>
                                </tr>
                                <tr>
                                    <td class="text-muted" scope="row" data-label="Account">Alamat</td>
                                    <td data-label="Due Date">:<strong> <?= $r['datapasien']['alamat'] ?></strong></td>
                                </tr>
                                <!-- <tr>
                                    <td class="text-muted" scope="row" data-label="Acount">Berat</td>
                                    <td data-label="Due Date">:<strong> <?= $r['datapasien']['berat_badan'] ?> Kg</strong></td>
                                </tr>
                                <tr>
                                    <td class="text-muted" scope="row" data-label="Acount">Tinggi</td>
                                    <td data-label="Due Date">:<strong> <?= $r['datapasien']['tinggi_badan'] ?> Cm</strong></td>
                                </tr> -->
                                <tr>
                                    <td class="text-muted" scope="row" data-label="Acount">Tanggal Lahir</td>
                                    <td data-label="Due Date">:<strong> <?= $r['datapasien']['tanggal_lahir'] ?></strong></td>
                                </tr>
                                <tr>
                                    <td class="text-muted" scope="row" data-label="Acount">Tanggal Tindakan</td>
                                    <td data-label="Due Date">:<strong> <?= date('Y-m-d', strtotime($r['datapasiendetail']['tanggal_tindakan'])) ?></strong></td>
                                </tr>

                                <?php if (!empty($r['voucher'])) : ?>
                                    <tr>
                                        <td class="text-muted" scope="row" data-label="Acount"> <i class="fa fa-gift text-warning"></i> Kupon</td>
                                        <td data-label="Due Date">:<strong> <?= $r['voucher']['code_voucher'] ?></strong> (- Rp <?= number_format($r['voucher']['value_voucher']) ?>) </td>
                                    </tr>
                                <?php endif ?>

                            </tbody>
                        </table>
                    </div>

                    <div class="col-lg-9 pl-4 pr-4 mt-3">
                        <hr class="mt-2 mb-4">
                        <div class="ff-poppins mb-4">
                            <p class="d-inline-block">Subtotal:
                            <p class="text-primary float-right">
                                <?php
                                // Biaya Uniq Code
                                $biaya_uniq_code = 0;
                                // (Sunat Anak zona  1 2 3, Dewasa zona 1 2 3, sunat gemuk)
                                if (in_array($r['productklinik']['id_product'], ['1', '2', '3'])) $biaya_uniq_code = 100;
                                // (Sunat Rumah)
                                if (in_array($r['productklinik']['id_product'], ['5']))  $biaya_uniq_code = 160;

                                $total_biaya_uniq_code +=  $biaya_uniq_code;
                                ?>
                                <strong>
                                    Rp <?= number_format($r['harga'] - $biaya_uniq_code + ((!empty($r['voucher'])) ? $r['voucher']['value_voucher'] : 0)) ?>
                                    <!-- <small>(+ Rp 50.000)</small> -->
                                </strong>
                            </p>
                            <?php $total += $r['harga'] + ((!empty($r['voucher'])) ? $r['voucher']['value_voucher'] : 0) ?></p>
                        </div>

                    </div>
                </div>

                <?php $total_potongan += (!empty($r['voucher'])) ? $r['voucher']['value_voucher'] : 0 ?>
            <?php endforeach ?>
        <?php endif ?>

        <!-- PASIEN 2 -->
        <!--<div class="row justify-content-center mt-3 ring-data pt-3">
            <div class="col-lg-2 col-3 pl-4 pr-4 img-data">
                <img src="assets/images/dirumah-01.png" class="img-fluid img-ringkasan">
            </div>
            <div class="col-lg-7 col-9 pl-4 pr-4 data-data">
                <p style="font-size: 1.5rem" class="text-primary"><strong>Sunat Di Rumah</strong></p>
                <table class="data-pasien">
                    <tbody>
                        <tr>
                            <td class="text-muted" data-label="Layanan Sunat">Layanan Sunat</td>
                            <td data-label="Sunat Di Rumah">:<strong> Sunat Di Rumah</strong></td>
                        </tr>
                        <tr>
                            <td class="text-muted" scope="row" data-label="Lokasi Klinik">Lokasi Klinik</td>
                            <td data-label="Kota Tangerang - Kelapa Indah">:<strong> Kota Tangerang - Kelapa Indah</strong></td>
                        </tr>
                        <tr>
                            <td class="text-muted" scope="row" data-label="Account">Alamat</td>
                            <td data-label="Due Date">:<strong> Jl. Asia Arika no 440 gg. 60 RT 03 RW 02, Jakarta Selatan</strong></td>
                        </tr>
                        <tr>
                            <td class="text-muted" scope="row" data-label="Acount">Berat</td>
                            <td data-label="Due Date">:<strong> 30 Kg</strong></td>
                        </tr>
                        <tr>
                            <td class="text-muted" scope="row" data-label="Acount">Tinggi</td>
                            <td data-label="Due Date">:<strong> 90 Cm</strong></td>
                        </tr>
                        <tr>
                            <td class="text-muted" scope="row" data-label="Acount">Tanggal Lahir</td>
                            <td data-label="Due Date">:<strong> 24 September 1993</strong></td>
                        </tr>
                        <tr>
                            <td class="text-muted" scope="row" data-label="Acount">Tanggal Tindakan</td>
                            <td data-label="Due Date">:<strong> 19 November 2021</strong></td>
                        </tr>

                    </tbody>
                </table>
            </div>
             <div class="col-lg-9 pl-4 pr-4 mt-3">
	                <hr class="mt-1 mb-1">
	                <form class="mt-4">
	                    <div class="form-group">
	                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
	                            <input type="checkbox" class="custom-control-input" id="customExampleCheck">
	                            <label class="custom-control-label" for="customExampleCheck">Tes Antigen Pasien Rp59.000</label>
	                            <p class="text-muted"><small>Tindakan di klinik wajib menyertakan hasil test Covid yang telah diambil minimal pada 7 hari sebelumnya, jika belum ada wajib tes antigen terlebih dahulu.</small></p>
	                        </div>
	                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3">
	                            <input type="checkbox" class="custom-control-input" id="customExampleCheck">
	                            <label class="custom-control-label" for="customExampleCheck">Tes Antigen Pendamping Rp89.000</label>
	                            <p class="text-muted"><small>Pendamping di klinik wajib menyertakan hasil test Covid yang telah diambil minimal pada 7 hari sebelumnya, jika belum ada wajib tes antigen terlebih dahulu.</small></p>
	                            <label class="pendamping-cls">
	                                <small>Total Pendamping: </small>
	                                <select name="pendamping">
	                                    <option value="1 Orang" selected><small>1 Orang</small></option>
	                                    <option value="2 Orang"><small>2 Orang</small></option>
	                                </select>
	                            </label>
	                        </div>
	                    </div>
	                </form>
	            </div> -->

        <!-- <div class="col-lg-9 pl-4 pr-4 mt-3">
            <hr class="mt-1 mb-1">
            <div class="">
                <p class="d-inline-block">Subtotal:</p>
                <p class="text-primary float-right"><strong>Rp <?= number_format($total) ?></strong></p>
            </div>

        </div> -->
    </div>

    <div class="container nav-container-fi bo-shd">

        <!-- DONASI -->
        <!-- <div class="row justify-content-center ring-data">
            <div class="col-lg-9 pl-4 pr-4 mt-3">
                <form class="mt-4">
                    <div class="form-group">
                        <div class="custom-control custom-checkbox custom-checkbox-primary mb-3 px-2">
                            <h5 class="ff-poppins mb-0">Donasi Sunatan Massal</h5>
                            <h6 class="text-muted ff-poppins mb-2 mt-3">Nominal Lain:</h6>
                            <form id="form_donasi">
                                <div class="form-row">
                                    <div class="col-md-4 col-6 mb-2">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="nominal_donasi1" name="nominal_donasi" value="50000" class="custom-control-input">
                                            <label class="custom-control-label" for="nominal_donasi1">50.000</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-6 mb-2">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="nominal_donasi2" name="nominal_donasi" value="100000" class="custom-control-input">
                                            <label class="custom-control-label" for="nominal_donasi2">100.000</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-6 mb-2">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="nominal_donasi3" name="nominal_donasi" value="200000" class="custom-control-input">
                                            <label class="custom-control-label" for="nominal_donasi3">200.000</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-6 mb-2">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="nominal_donasi4" name="nominal_donasi" value="300000" class="custom-control-input">
                                            <label class="custom-control-label" for="nominal_donasi4">300.000</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-6 mb-2">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="nominal_donasi5" name="nominal_donasi" value="500000" class="custom-control-input">
                                            <label class="custom-control-label" for="nominal_donasi5">500.000</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-6 mb-2">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" id="nominal_donasi6" name="nominal_donasi" class="custom-control-input" value="custom">
                                            <label class="custom-control-label" for="nominal_donasi6"><input type="number" name="nominal" min="10000" class="form-control form-control-simple nominal-apa pt-1 pb-1" placeholder="Nominal Lainnya" id="nominal" aria-describedby="nominal_help" required></label>

                                        </div>
                                    </div>
                                    <div class="col-md-12 col-12 mb-3">
                                        <h6 class="text-muted ff-poppins mb-2">Tuliskan doa-mu</h6>
                                        <textarea name="doa_harapan" id="doa_harapan" class="form-control" placeholder="Berikan Doa / Harapanmu disini"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </form>
            </div>

        </div> -->

        <!-- RINGKASAN PEMBELIAN -->

        <div class="row justify-content-center my-4 pb-2 ring-data">
            <div class="col-lg-9 pl-4 pr-4 mt-3">
                <p class="ff-poppins">Ringkasan Pembelian</p>
                <div class="ff-poppins">
                    <p class="d-inline-block text-muted">Total Harga (<?= count($ringkasan['orderdetail']) ?> Layanan)</p>
                    <p class="text-primary float-right">Rp <?= number_format($total - $total_biaya_uniq_code) ?></p>
                </div>
                <!-- <div class="">
                    <p class="d-inline-block text-muted">Biaya tambahan APD tim Klinik</p>
                    <p class="text-primary float-right">+ Rp (<?= number_format(50000 * count($ringkasan['orderdetail'])) ?>)</p>
                </div> -->
                <div class="">
                    <p class="d-inline-block text-muted">Unique Code</p>
                    <p class="text-primary float-right">+ Rp (<?= number_format($total_biaya_uniq_code) ?>)</p>
                </div>
                <div class="is_donasi" style="display: none;">
                    <p class="d-inline-block text-muted">Donasi</p>
                    <p class="text-primary float-right is_donasi_jumlah">Rp (000.000)</p>
                </div>
                <?php if (!empty($total_potongan)) : ?>
                    <div class="">
                        <p class="d-inline-block text-muted">Potongan harga</p>
                        <p class="text-primary float-right">Rp (-<?= number_format($total_potongan) ?>)</p>
                    </div>
                <?php endif ?>
            </div>

        </div>

        <!-- TOTAL PEMBELIAN -->

        <div class="row justify-content-center mt-4 ring-data pt-3 ff-poppins">
            <div class="col-lg-9 pl-4 pr-4 mt-1 pb-3">


                <div class="d-inline-block">
                    <p class="mb-1 ff-poppins">Total Harga</p>
                    <h3 class="text-primary ff-poppins"><strong class="total_harga">Rp <?= number_format($total - $total_potongan) ?></strong></h3>
                </div>
                <div class="d-inline-block float-right">
                    <?php if ($ringkasan['status_payments'] == 'unpaid') : ?>
                        <!-- <a class="btn btn-primary btn-rounded p-2 pr-4 box-shadow-none pl-4 btn-sm font-size-lg btn-full-radius ff-poppins" href="javascript:proses_pembayaran()">
                            Bayar
                        </a> -->
                        <a class="btn btn-primary btn-rounded p-2 pr-4 box-shadow-none pl-4 btn-sm font-size-lg btn-full-radius ff-poppins" href="<?= base_url('pendaftaran/pembayaran/' . $ringkasan['id_order']) ?>">
                            Bayar
                        </a>
                    <?php else : ?>
                        <a class="btn btn-success btn-rounded p-2 pr-4 box-shadow-none pl-4 btn-sm font-size-lg btn-full-radius ff-poppins" disabled>
                            Sudah dibayar
                        </a>
                    <?php endif ?>
                </div>

            </div>

        </div>
    </div>

    </div>
</section>

<div class="modal fade" id="modal_pembayaran" tabindex="-1" role="dialog" aria-labelledby="modal_pembayaranLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="height: 100vh">
            <div class="modal-header p-0">
                <!-- <h5 class="modal-title" id="modal_pembayaranLabel">Modal title</h5> -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body pt-0">
                <div class="embed-responsive embed-responsive-16by9" style="height: 90vh">
                    <iframe class="embed-responsive-item" id="container_pembayaran" src="" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script>
    const total_harga = <?= $total - $total_potongan ?>;
    let total_donasi;

    $('input[name=nominal]').on('keyup', function() {
        if ($(this).val() == '') {
            $('input#nominal_donasi6').prop('checked', false)
            return
        } else {
            $('input#nominal_donasi6').prop('checked', true)
            total_donasi = $(this).val();
            text_donasi()
        }
    });

    $('input[name=nominal_donasi]').on('change', function() {
        if ($(this).val() != 'custom') {
            $('input[name=nominal]').val('')
            total_donasi = $('input[name=nominal_donasi]:checked').val();
            text_donasi();
            return
        }
    });

    function text_donasi() {
        if (total_donasi) {
            $('.is_donasi').show();
            $('.is_donasi_jumlah').html(`Rp ${Number(total_donasi).toLocaleString()}`)
        } else {
            $('.is_donasi').hide();
            $('.is_donasi_jumlah').html(`Rp ${Number(0).toLocaleString()}`)
        }

        $('.total_harga').html(`Rp ${Number(total_harga+Number(total_donasi)).toLocaleString()}`)
    }

    function proses_pembayaran() {
        let nominal;
        if ($('input[name=nominal_donasi]:checked').val() == 'custom') {
            nominal = $('input[name=nominal]').val();
            if (nominal == '') {
                alert('Nominal Donasi harus diisi');
                $('input[name=nominal]').focus()
                return
            }

            if (nominal < 10000) {
                alert('Nominal minimal Rp. 10.000');
                $('input[name=nominal]').focus()
                return
            }
        } else {
            nominal = $('input[name=nominal_donasi]:checked').val();
        }
        $.ajax({
            type: "post",
            url: "/keranjang/proses_pembayaran",
            data: {
                donasi: nominal,
                doa_harapan: $('#doa_harapan').val(),
                id_order: <?= $ringkasan['id_order'] ?>
            },
            dataType: "json",
            success: function(response) {
                if (response.code == 200) {
                    $('#container_pembayaran').prop('src', response.order.invoice_url);
                    $('#modal_pembayaran').modal('show');
                } else {
                    alert('Terjadi Error. Silahkan Refresh Kembali')
                }
            },
            fail: function(xhr) {
                alert('Mohon maaf, terjadi kesalahan')
            }
        });
    }
</script>
<?= $this->endSection() ?>