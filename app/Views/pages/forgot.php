<?= $this->extend('template') ?>

<?= $this->section('lib_css') ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<?= $this->endSection() ?>


<?= $this->section('content') ?>

<section class="section bg-white riwayat-pem">
	<div class="container nav-container-fi bo-shd pb-4 px-4">
		<div class="row justify-content-center">
			<div class="col-lg-12 px-4">
				<!-- <a class="text-left text-muted" href="index.php?pages=ringkasan"><i class="fa fa-angle-left" aria-hidden="true"></i> Kembali</a> -->
				<h2 class="text-primary my-3 text-center">
					<strong>Lupa Password</strong>
				</h2>
				<hr class="mt-1">
			</div>
		</div>
		<form id="form_reset_password" method="post">
			<input type="hidden" name="token" id="token_reset" value="<?= $token ?>">
			<input type="hidden" name="id_user" id="id_user_reset" value="<?= $id_user ?>">

			<div class="form-group">
				<label for="new_password">Password Baru <i class="fa fa-eye text-secondary" style="cursor:pointer" onclick="toggle_password(this)"></i></label>
				<input type="password" class="form-control" id="new_password" name="password" aria-describedby="new_password" placeholder="Ketikkan Password Baru kamu" required>
			</div>
			<div class="form-group">
				<label for="conf_password">Ulangi Password <i class="fa fa-eye text-secondary" style="cursor:pointer" onclick="toggle_password(this)"></i></label>
				<input type="password" class="form-control" id="conf_password" name="conf_password" aria-describedby="conf_password" placeholder="Ulangi Password baru kamu" required>
			</div>

			<button type="submit" class="btn btn-primary box-shadow-none">Simpan Password</button>
		</form>

	</div>
</section>

<?= $this->endSection() ?>

<?= $this->section('lib_js') ?>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script>
	$(document).ready(function() {

	});
</script>
<?= $this->endSection() ?>