<?php // dd($ringkasan) 
?>

<?= $this->extend('template') ?>

<?= $this->section('content') ?>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header p-0">
				<!-- <h5 class="modal-title" id="exampleModalLabel">Modal title</h5> -->
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body text-center">
				<img src="assets/images/Thank-You.png" class="img-fluid">
				<p>Terima Kasih Telah Mendaftar!</p>
				<a class="btn btn-primary btn-rounded p-2 pr-4 pl-4 btn-sm font-size-lg btn-full-radius" data-toggle="modal" data-target="#exampleModal" href="#">
					Daftarkan Teman
				</a>
			</div>
			<!-- <div class="modal-footer">

			</div> -->
		</div>
	</div>
</div>

<div class="modal fade" id="modal_pembayaran" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="modal_pembayaranLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header p-0">
				<!-- <h5 class="modal-title" id="modal_pembayaranLabel">Modal title</h5> -->
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body text-center pt-0">
				<h2 class="mb-4">Menunggu Pembayaran</h2>
				<h1 class="mb-0 pb-0 text-underline"><u>0000 0000 0000 0000</u></h1>
				<small> ( BANK ) </small>
				<p class="text-muted mt-4">Silahkan Lakukan pembayaran ke Virtual Akun diatas <br /> <small>Pembayaran akan kadaluarsa 1x24 Jam</small></p>
				<!-- <div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" id="container_pembayaran" src="" allowfullscreen></iframe>
				</div> -->
			</div>
			<!-- <div class="modal-footer">

			</div> -->
		</div>
	</div>
</div>

<!-- Modal CC -->
<div class="modal fade" id="modal_cc" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="modal_ccLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header p-0">
				<!-- <h5 class="modal-title" id="modal_ccLabel">Modal title</h5> -->
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body pb-0">
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label for="ccnumber">Credit Card Number</label>
							<div class="input-group">
								<input class="form-control" type="number" placeholder="0000 0000 0000 0000" required id="card_number">
								<div class="input-group-append">
									<span class="input-group-text">
										<i class="mdi mdi-credit-card"></i>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-sm-4">
						<label for="card_exp_month">Month</label>
						<select class="form-control" id="card_exp_month">
							<option value="">Pilih Bulan</option>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
							<option value="6">6</option>
							<option value="7">7</option>
							<option value="8">8</option>
							<option value="9">9</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
						</select>
					</div>
					<div class="form-group col-sm-4">
						<label for="card_exp_year">Year</label>
						<select class="form-control" id="card_exp_year">
							<option value="0">Pilih Tahun</option>
							<option value="2014">2014</option>
							<option value="2015">2015</option>
							<option value="2016">2016</option>
							<option value="2017">2017</option>
							<option value="2018">2018</option>
							<option value="2019">2019</option>
							<option value="2020">2020</option>
							<option value="2021">2021</option>
							<option value="2022">2022</option>
							<option value="2023">2023</option>
							<option value="2024">2024</option>
							<option value="2025">2025</option>
						</select>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label for="card_cvv">CVV/CVC</label>
							<input class="form-control" id="card_cvv" type="text" placeholder="123">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-success" onclick="proses_cc()">Proses</button>
			</div>
		</div>
	</div>
</div>

<section class="section bg-white">
	<div class="container nav-container-fi bo-shd">
		<div class="row justify-content-center">
			<div class="col-lg-12 pl-4 pr-4 mt-5">
				<!-- <a class="text-left text-muted" href="index.php?pages=ringkasan"><i class="fa fa-angle-left" aria-hidden="true"></i> Kembali</a> -->
				<h2 class="text-primary mb-1 mt-3 text-center">
					<strong>Metode Pembayaran</strong>
				</h2>
				<hr class="mt-1">
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-lg-9 pl-4 pr-4">
				<form id="pembayaran">
					<div class="accordion" id="accordion1">
						<!--Item 1-->
						<div class="card card-panel">
							<div class="card-header py-4" id="headingOne2" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1">
								Virtual accounts
							</div>
							<div id="collapseOne1" class="collapse show" aria-labelledby="headingOne1" data-parent="#accordion1">
								<div class="card-body">

									<div class="form-check">
										<input class="form-check-input pembayaran" data-by="xendit" data-tipe="va" data-bank="bca" type="radio" name="payment_method" id="bank_bca" value="BCA" required>
										<label class="form-check-label" for="bank_bca">Bank BCA</label>
									</div>

									<div class="form-check">
										<input class="form-check-input pembayaran" data-by="xendit" data-tipe="va" data-bank="permata" type="radio" name="payment_method" id="bank_permata" value="PERMATA" required>
										<label class="form-check-label" for="bank_permata">Bank Permata</label>
									</div>

									<div class="form-check">
										<input class="form-check-input pembayaran" data-by="xendit" data-tipe="va" data-bank="mandiri" type="radio" name="payment_method" id="bank_mandiri" value="MANDIRI" required>
										<label class="form-check-label" for="bank_mandiri">Bank Mandiri</label>
									</div>

									<div class="form-check">
										<input class="form-check-input pembayaran" data-by="xendit" data-tipe="va" data-bank="bni" type="radio" name="payment_method" id="bank_bni" value="BNI" required>
										<label class="form-check-label" for="bank_bni">Bank BNI</label>
									</div>

									<div class="form-check">
										<input class="form-check-input pembayaran" data-by="xendit" data-tipe="va" data-bank="bri" type="radio" name="payment_method" id="bank_bri" value="BRI" required>
										<label class="form-check-label" for="bank_bri">Bank BRI</label>
									</div>


									<!-- <div class="form-check">
										<input class="form-check-input pembayaran" data-by="midtrans" data-tipe="bank_transfer" type="radio" name="payment_method" id="bank_bca" data-bank="bca" value="bank_transfer" required>
										<label class="form-check-label" for="bank_bca">Bank BCA</label>
									</div> -->
									<!-- <div class="form-check">
										<input class="form-check-input pembayaran" data-by="midtrans" data-tipe="bank_transfer" type="radio" name="payment_method" id="bank_bni" data-bank="bni" value="bank_transfer" required>
										<label class="form-check-label" for="bank_bni">Bank BNI</label>
									</div>
									<div class="form-check">
										<input class="form-check-input pembayaran" data-by="midtrans" data-tipe="bank_transfer" type="radio" name="payment_method" id="bank_bri" data-bank="bri" value="bank_transfer" required>
										<label class="form-check-label" for="bank_bri">Bank BRI</label>
									</div> -->
									<!-- <div class="form-check">
										<input class="form-check-input pembayaran" data-by="midtrans" data-tipe="bank_transfer" type="radio" name="payment_method" id="echannel" data-bank="echannel" value="echannel" required>
										<label class="form-check-label" for="echannel">Mandiri Bill</label>
									</div> -->
									<!-- <div class="form-check">
										<input class="form-check-input pembayaran" data-by="midtrans" data-tipe="bank_transfer" type="radio" name="payment_method" id="bank_permata" data-bank="permata" value="permata" required>
										<label class="form-check-label" for="bank_permata">Bank Permata</label>
									</div> -->
								</div>
							</div>
						</div>
						<!--Item 2-->
						<div class="card card-panel">
							<div class="card-header py-4 collapsed" id="headingTwo2" data-toggle="collapse" data-target="#collapseTwo1" aria-expanded="false" aria-controls="collapseTwo1">
								eWallets
							</div>
							<div id="collapseTwo1" class="collapse" aria-labelledby="headingTwo1" data-parent="#accordion1">
								<div class="card-body">
									<div class="form-check">
										<input class="form-check-input pembayaran" data-by="xendit" data-tipe="qrcode" type="radio" name="payment_method" value="QRCODE" id="qrcode" required>
										<label class="form-check-label" for="qrcode">QRIS</label>
									</div>
									<!-- <div class="form-check">

										<input class="form-check-input pembayaran" data-by="midtrans" data-tipe="ewallet" type="radio" name="payment_method" value="qris" id="qris" required>

										<label class="form-check-label" for="qris">QRIS</label>

									</div> -->
									<!-- <div class="form-check">

										<input class="form-check-input pembayaran" data-by="xendit" data-tipe="ewallet" type="radio" name="payment_method" value="OVO" id="ovo" required>

										<label class="form-check-label" for="ovo">OVO</label>

									</div> -->


									<div class="form-check">

										<input class="form-check-input pembayaran" data-by="xendit" data-tipe="ewallet" type="radio" name="payment_method" value="DANA" id="dana" required>

										<label class="form-check-label" for="dana">Dana</label>

									</div>

									<div class="form-check">

										<input class="form-check-input pembayaran" data-by="xendit" data-tipe="ewallet" type="radio" name="payment_method" value="LINKAJA" id="linkaja" required>

										<label class="form-check-label" for="linkaja">Link Aja</label>

									</div>
									<!-- <div class="form-check">

										<input class="form-check-input pembayaran" data-by="xendit" data-tipe="ewallet" type="radio" name="payment_method" value="SHOPEEPAY" id="shopeepay" required>

										<label class="form-check-label" for="shopeepay">Shopee Pay</label>

									</div> -->
									<!-- <div class="form-check">
										<input class="form-check-input pembayaran" data-by="midtrans" data-tipe="ewallet" type="radio" name="payment_method" value="gopay" id="gopay" required>
										<label class="form-check-label" for="gopay">GoPay</label>
									</div> -->
								</div>
							</div>
						</div>
						<!--Item 3-->
						<!-- <div class="card card-panel">
							<div class="card-header py-4 collapsed" id="headingThree1" data-toggle="collapse" data-target="#collapseThree1" aria-expanded="false" aria-controls="collapseThree1">
								QR Codes
							</div>
							<div id="collapseThree1" class="collapse" aria-labelledby="headingThree1" data-parent="#accordion1">
								<div class="card-body">
									<div class="form-check">

										<input class="form-check-input pembayaran" data-by="xendit" data-tipe="danamon_online" type="radio" name="payment_method" value="danamon_online" required>

										<label class="form-check-label" for="bank_bri">QRIS</label>

									</div>
								</div>
							</div>
						</div> -->

						<!-- Item 4 -->
						<!-- <div class="card card-panel">
							<div class="card-header py-4 collapsed" id="headingFour1" data-toggle="collapse" data-target="#collapseFour1" aria-expanded="false" aria-controls="collapseFour1">
								Credit Card
							</div>
							<div id="collapseFour1" class="collapse" aria-labelledby="headingFour1" data-parent="#accordion1">
								<div class="card-body">
									<div class="form-check">

										<input class="form-check-input pembayaran" data-by="midtrans" id="credit_card" data-tipe="credit_card" type="radio" name="payment_method" value="credit_card" required>

										<label class="form-check-label" for="credit_card">Visa, Mastercard, JCB, American Express</label>

									</div>
								</div>
							</div>
						</div> -->

						<!-- Item 5 -->
						<!-- <div class="card card-panel">
							<div class="card-header py-4 collapsed" id="headingFive1" data-toggle="collapse" data-target="#collapseFive1" aria-expanded="false" aria-controls="collapseFive1">
								Direct Debit
							</div>
							<div id="collapseFive1" class="collapse" aria-labelledby="headingFive1" data-parent="#accordion1">
								<div class="card-body">
									<div class="form-check">
										<input class="form-check-input pembayaran" data-by="midtrans" id="bca_klikpay" data-tipe="bca_klikpay" type="radio" name="payment_method" value="bca_klikpay" required>
										<label class="form-check-label" for="bca_klikpay">BCA KlikPay</label>
									</div>

									<div class="form-check">
										<input class="form-check-input pembayaran" data-by="midtrans" id="cimb_clicks" data-tipe="cimb_clicks" type="radio" name="payment_method" value="cimb_clicks" required>
										<label class="form-check-label" for="cimb_clicks">CIMB CLICKS</label>
									</div>

									<div class="form-check">
										<input class="form-check-input pembayaran" data-by="midtrans" id="danamon_online" data-tipe="danamon_online" type="radio" name="payment_method" value="danamon_online" required>
										<label class="form-check-label" for="danamon_online">Danamon Online</label>
									</div>
								</div>
							</div>
						</div> -->

						<!-- Item 6 -->
						<div class="card card-panel">
							<div class="card-header py-4 collapsed" id="headingSix1" data-toggle="collapse" data-target="#collapseSix1" aria-expanded="false" aria-controls="collapseSix1">
								Retail
							</div>
							<div id="collapseSix1" class="collapse" aria-labelledby="headingSix1" data-parent="#accordion1">
								<div class="card-body">
									<!-- <div class="form-check">
										<input class="form-check-input pembayaran" data-by="xendit" id="indomaret" data-tipe="retail" type="radio" name="payment_method" value="INDOMARET" required>
										<label class="form-check-label" for="indomaret">Indomaret</label>
									</div> -->

									<div class="form-check">
										<input class="form-check-input pembayaran" data-by="xendit" id="alfamart" data-tipe="retail" type="radio" name="payment_method" value="ALFAMART" required>
										<label class="form-check-label" for="alfamart">Alfamart</label>
									</div>

								</div>
							</div>
						</div>

					</div>
					<div class="d-block text-center mt-5 mb-4">
						<a class="btn btn-primary btn-rounded p-2 pr-4 pl-4 btn-sm font-size-lg box-shadow-none btn-full-radius" href="javascript:proses_pembayaran()">
							Bayar Sekarang
						</a>
					</div>

				</form>
			</div>
		</div>
	</div>
</section>

<?= $this->endSection() ?>

<?= $this->section('lib_js') ?>
<!-- <script id="midtrans-script" type="text/javascript" src="https://api.midtrans.com/v2/assets/js/midtrans-new-3ds.min.js" data-environment="sandbox" data-client-key="SB-Mid-client-5daMg6tI2MF-SE1T"></script> -->
<script id="midtrans-script" type="text/javascript" src="https://api.midtrans.com/v2/assets/js/midtrans-new-3ds.min.js" data-environment="production" data-client-key="Mid-client-eVTDxoWoMHxvj-ko"></script>
<script src="/assets/js/lib_js/jquery.qrcode.min.js"></script>
<?= $this->endSection() ?>


<?= $this->section('script') ?>
<script>
	// window.history.replaceState({}, '', '/dashboard');
	// window.history.pushState('Object', 'Title', '/new-url');
	// window.history.pushState({
	// 	id: 35
	// }, 'testing', '/order-pembayatan/<?= $ringkasan['id_order'] ?>');
	let cc_token_id = '';

	function proses_pembayaran() {
		let input = $('input[name=payment_method]:checked');
		let url, payment_method, bank;
		if (!input.val()) {
			alert('Silahkan Pilih salah satu Method pembayaran')
			return false
		}

		payment_method = input.val();
		bank = input.data('bank');

		switch (input.data('tipe')) {
			case 'va': // Xendit Bank Transfer
				url = 'checkout-order-va/';
				break;
			case 'qrcode':
				url = 'checkout-order-qrcode/'
				if (<?= $ringkasan['total_harga'] ?> <= 1500) {
					alert('Transaksi tidak bisa dilanjutkan karena minimum nominal, yaitu : Rp. 1.500 \nSilahkan Pilih Metode Pembayaran yang lain.')
					return false
				}

				if (<?= $ringkasan['total_harga'] ?> >= 5000000) {
					alert('Transaksi tidak bisa dilanjutkan karena Maximum nominal, yaitu : Rp. 5.000.000 \nSilahkan Pilih Metode Pembayaran yang lain.')
					return false
				}
				break;
			case 'ewallet':
				url = 'checkout-order-ewallet/'
				break;
			case 'retail':
				url = 'checkout-order-retail/'
				if (input.val() == 'INDOMARET' && <?= $ringkasan['total_harga'] ?> >= 5000000) {
					alert('Transaksi tidak bisa dilanjutkan karena melebihi maximum nominal, yaitu : Rp. 5.000.000 \nSilahkan Pilih Metode Pembayaran yang lain.')
					return false
				}

				if (input.val() == 'ALFAMART' && <?= $ringkasan['total_harga'] ?> >= 2500000) {
					alert('Transaksi tidak bisa dilanjutkan karena melebihi maximum nominal, yaitu : Rp. 2.500.000 \nSilahkan Pilih Metode Pembayaran yang lain.')
					return false
				}
				break;
			case 'bca_klikpay':
			case 'cimb_clicks':
			case 'danamon_online':
			case 'bri_epay':
			case 'bank_transfer': // Midtrans Bank Transfer
				url = 'checkout-order-midtrans/'
				break;
			case 'credit_card':
				url = 'checkout-order-credit_card/'
				if (cc_token_id == '') {
					$('#modal_cc').modal('show');
					return
				}
				break;
			default:
				alert('Payment Method tidak terdaftar');
				return
				break;
		}

		$.ajax({
			type: "post",
			url: BASE_API + url + <?= $ringkasan['id_order'] ?>,
			data: {
				payment_method: payment_method,
				mobile: '<?= session('telepon') ?>',
				token_id: cc_token_id,
				bank: bank,
			},
			dataType: "json",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Authorization", "Bearer " + TOKEN);
			},
			success: function(response) {
				if (response.code == 200) {
					let result = response.order;
					console.log(result)


					// XENDIT Retail
					if (result.hasOwnProperty('retail_outlet_name')) {
						$('#modal_pembayaran').find('.modal-body').html(`<h2 class="mb-4">Menunggu Pembayaran</h2>
							<h1 class="mb-0 pb-0 text-underline"><u>${result.payment_code}</u></h1>
							<small> ( ${input.val()} ) - Rp. ${Number(result.expected_amount).toLocaleString()} </small>
							<p class="text-muted mt-4">Tunjukkan Kode diatas pada kasir <br /> <small>Pembayaran akan kadaluarsa 1x24 Jam</small> </p>`)
					}

					// Xendit QRIS
					if (result.hasOwnProperty('qr_string')) {
						$('#modal_pembayaran').find('.modal-body').html(`<h2 class="mb-4">Menunggu Pembayaran</h2>
							<div id="qrcode"></div>
							<p>Rp. ${Number(result.amount).toLocaleString()}</p>
							<p class="text-muted mt-4">Scan QRCODE diatas melalui Aplikasi QRIS kamu <br /> <small>Pembayaran akan kadaluarsa 1x24 Jam</small> </p>`)

						$('#qrcode').qrcode({
							width: 200,
							height: 200,
							text: result.qr_string
						});
					}

					// EWallet
					if (result.hasOwnProperty('actions') && result.actions != null) {
						// Gopay Midtrans
						if (payment_method == 'gopay') {

							let qr_code = result.actions[0].url;
							let link_pay_to_app = result.actions[1].url;

							$('#modal_pembayaran').find('.modal-body').html(`<h2 class="mb-4">Menunggu Pembayaran</h2>
							<img src="${qr_code}" alt="qrcode Gopay" width="200px" height="200px">
							<p class="text-muted mt-4">Scan QRCODE diatas melalui Aplikasi GOPAY kamu atau <a href="${link_pay_to_app}">Klik disini</a> <br /> <small>Pembayaran akan kadaluarsa 1x24 Jam</small> </p>`)

							$('#modal_pembayaran').modal('show');

							return
						}

						// QRIS Midtrans
						if (payment_method == 'qris') {

							let qr_code = result.actions[0].url;

							$('#modal_pembayaran').find('.modal-body').html(`<h2 class="mb-4">Menunggu Pembayaran</h2>
								<img src="${qr_code}" alt="qrcode Gopay" width="200px" height="200px">
								<p class="text-muted mt-4">Scan QRCODE diatas melalui Aplikasi QRIS kamu atau <a href="${qr_code}">Klik disini</a> <br /> <small>Pembayaran akan kadaluarsa 1x24 Jam</small> </p>`)

							$('#modal_pembayaran').modal('show');

							return
						}

						if (payment_method == 'SHOPEEPAY') {

							let qr_code = result.actions.qr_checkout_string;
							let link_pay_to_app = result.actions.mobile_deeplink_checkout_url;

							$('#modal_pembayaran').find('.modal-body').html(`<h2 class="mb-4">Menunggu Pembayaran</h2>
								<div id="qrcode"></div>
								<p>Rp. ${Number(result.charge_amount).toLocaleString()}</p>
								<p class="text-muted mt-4">Scan QRCODE diatas melalui Aplikasi SHOPEEPAY kamu atau <a href="${link_pay_to_app}">Klik disini</a> <br /> <small>Pembayaran akan kadaluarsa 1x24 Jam</small> </p>`)
							$('#modal_pembayaran').modal('show');

							$('#qrcode').qrcode({
								width: 200,
								height: 200,
								text: qr_code
							});

							return
						}

						$('#modal_pembayaran').find('.modal-body').html(`<h1 class="mb-0">Mohon Menunggu</h1><p class="text-muted">Kami sedang mengarahkan anda pada halaman Pembayaran <br /> <small>Pembayaran akan kadaluarsa 1x24 Jam</small></p>`)

						if (result.actions.hasOwnProperty('mobile_deeplink_checkout_url') && result.actions.mobile_deeplink_checkout_url != null) window.location.href = result.actions.mobile_deeplink_checkout_url

						if (result.actions.hasOwnProperty('desktop_web_checkout_url') && result.actions.desktop_web_checkout_url != null) window.location.href = result.actions.desktop_web_checkout_url

						if (result.actions.hasOwnProperty('mobile_web_checkout_url') && result.actions.mobile_web_checkout_url != null) window.location.href = result.actions.mobile_web_checkout_url
					}

					// Virtual Account
					if (result.hasOwnProperty('bank_code')) {
						let tata_cara_pembayaran = '';
						// Tata Cara BCA 
						if (bank == 'bca') {
							tata_cara_pembayaran = `<div class="text-left"><p class="mb-0"><strong>ATM</strong></p><ol><li>Masukkan kartu ATM dan PIN BCA Anda</li><li>Di menu utama, pilih "Transaksi Lainnya". Pilih "Transfer". Pilih "Ke BCA Virtual Account"</li><li>Masukkan nomor Virtual Account ${result.account_number}</li><li>Pastikan data Virtual Account Anda benar, kemudian masukkan angka yang perlu Anda bayarkan, kemudian pilih "Benar"</li><li>Cek dan perhatikan konfirmasi pembayaran dari layar ATM, jika sudah benar pilih "Ya", atau pilih "Tidak" jika data di layar masih salah</li><li>Transaksi Anda sudah selesai. Pilih "Tidak" untuk tidak melanjutkan transaksi lain</li><li>Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</li></ol><p class="mb-0"><strong>IBanking</strong></p><ol><li>Login ke KlikBCA Individual</li><li>Pilih "Transfer", kemudian pilih "Transfer ke BCA Virtual Account"</li><li>Masukkan nomor Virtual Account ${result.account_number}</li><li>Pilih "Lanjutkan" untuk melanjutkan pembayaran</li><li>Masukkan "RESPON KEYBCA APPLI 1" yang muncul pada Token BCA Anda, lalu klik tombol "Kirim"</li><li>Transaksi Anda sudah selesai</li><li>Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</li></ol><p class="mb-0"><strong>BCA M-Mobile</strong></p><ol><li>Buka Aplikasi BCA Mobile</li><li>Pilih "m-BCA", kemudian pilih "m-Transfer"</li><li>Pilih "BCA Virtual Account"</li><li>Masukkan nomor Virtual Account ${result.account_number}, lalu pilih "OK"</li><li>Klik tombol "Send" yang berada di sudut kanan atas aplikasi untuk melakukan transfer</li><li>Klik "OK" untuk melanjutkan pembayaran</li><li>Masukkan PIN Anda untuk meng-otorisasi transaksi</li><li>Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</li></ol></div>`
						}

						if (bank == 'mandiri') {
							tata_cara_pembayaran = `<div class="text-left"><p class="mb-0"><strong>ATM</strong></p><ol><li>Masukkan kartu ATM dan pilih "Bahasa Indonesia"</li><li>Ketik nomor PIN dan tekan BENAR</li><li>Pilih menu “BAYAR/BELI”</li><li>Pilih menu “MULTI PAYMENT”</li><li>Ketik kode perusahaan, yaitu "88608" (Xendit 88608), tekan "BENAR"</li><li>Masukkan nomor Virtual Account (${result.account_number})</li><li>Isi NOMINAL, kemudian tekan "BENAR"</li><li>Muncul konfirmasi data customer. Pilih Nomor 1 sesuai tagihan yang akan dibayar, kemudian tekan "YA"</li><li>Muncul konfirmasi pembayaran. Tekan "YA" untuk melakukan pembayaran</li><li>Bukti pembayaran dalam bentuk struk agar disimpan sebagai bukti pembayaran yang sah dari Bank Mandiri</li><li>Transaksi Anda sudah selesai</li></ol><p class="mb-0"><strong>Mobile Banking</strong></p><ol><li>Log in Mobile Banking Mandiri Online (Install Mandiri Online by PT. Bank Mandiri (PerseroTbk.) dari <a data-cke-saved-href="https://itunes.apple.com/id/app/mandiri-online/id1117312908?mt=8" target="_blank" href="https://itunes.apple.com/id/app/mandiri-online/id1117312908?mt=8">App Store</a> atau <a data-cke-saved-href="https://play.google.com/store/apps/details?id=com.bankmandiri.mandirionline&amp;hl=en" href="https://play.google.com/store/apps/details?id=com.bankmandiri.mandirionline&amp;hl=en">Play Store</a>)</li><li>Klik “Icon Menu” di sebelah kiri atas</li><li>Pilih menu “Pembayaran”</li><li>Pilih “Buat Pembayaran Baru”</li><li>Pilih “Multi Payment”</li><li>Pilih Penyedia Jasa “Xendit 88608”</li><li>Pilih “No. Virtual”</li><li>Masukkan no virtual account (${result.account_number}) dengan kode perusahaan “88608” lalu pilih “Tambah Sebagai Nomor Baru”</li><li>Masukkan “Nominal” lalu pilih “Konfirmasi”</li><li>Pilih “Lanjut”</li><li>Muncul tampilan konfirmasi pembayaran</li><li>Scroll ke bawah di tampilan konfirmasi pembayaran lalu pilih “Konfirmasi”</li><li>Masukkan “PIN” dan transaksi telah selesai</li></ol><p class="mb-0"><strong>Internet Banking</strong></p><ol><li>Kunjungi website Mandiri Internet Banking: https://ibank.bankmandiri.co.id/retail3/</li><li>Login dengan memasukkan USER ID dan PIN</li><li>Pilih "Pembayaran"</li><li>Pilih "Multi Payment"</li><li>Pilih "No Rekening Anda"</li><li>Pilih Penyedia Jasa "Xendit 88608"</li><li>Pilih "No Virtual Account"</li><li>Masukkan nomor Virtual Account anda (${result.account_number})</li><li>Masuk ke halaman konfirmasi 1</li><li>Apabila benar/sesuai, klik tombol tagihan TOTAL, kemudian klik "Lanjutkan"</li><li>Masuk ke halaman konfirmasi 2</li><li>Masukkan Challenge Code yang dikirimkan ke Token Internet Banking Anda, kemudian klik "Kirim"</li><li>Anda akan masuk ke halaman konfirmasi jika pembayaran telah selesai<br><br></li></ol></div>`
						}

						if (bank == 'bri') {
							tata_cara_pembayaran = `<div class="text-left"><p class="mb-0"><strong>ATM</strong></p><ol><li>Pilih menu Transaksi Lain</li><li>Pilih menu Lainnya</li><li>Pilih menu Pembayaran</li><li>Pilih menu Lainnya</li><li>Pilih menu BRIVA</li><li>Masukkan Nomor BRI Virtual Account (${result.account_number}), lalu tekan “Benar”</li><li>Konfirmasi pembayaran, tekan “Ya” bila sesuai</li></ol><p class="mb-0"><strong>Internet Banking</strong></p><ol><li>Masukan User ID dan Password</li><li>Pilih menu Pembayaran</li><li>Pilih menu BRIVA</li><li>Pilih rekening Pembayar</li><li>Masukkan Nomor Virtual Account BRI Anda (${result.account_number})</li><li>Masukkan nominal yang akan dibayar</li><li>Masukkan password dan Mtoken anda</li></ol><p class="mb-0"><strong>Mobile Banking</strong></p><ol><li>Log in ke Mobile Banking</li><li>Pilih menu Pembayaran</li><li>Pilih menu BRIVA</li><li>Masukkan nomor BRI Virtual Account (${result.account_number}) dan jumlah pembayaran</li><li>Masukkan nomor PIN anda</li><li>Tekan “OK” untuk melanjutkan transaksi</li><li>Transaksi berhasil</li><li>SMS konfirmasi akan masuk ke nomor telepon anda</li></ol></div>`
						}

						if (bank == 'bni') {
							tata_cara_pembayaran = `<div class="text-left"><p class="mb-0"><strong>ATM</strong></p><ol><li>Masukkan Kartu Anda.</li><li>Pilih Bahasa.</li><li>Masukkan PIN ATM Anda.</li><li>Pilih "Menu Lainnya".</li><li>Pilih "Transfer".</li><li>Pilih Jenis rekening yang akan Anda gunakan (Contoh; "Dari Rekening Tabungan").</li><li>Pilih “Virtual Account Billing” .</li><li>Masukkan nomor Virtual Account Anda (${result.account_number}).</li><li>Konfirmasi, apabila telah sesuai, lanjutkan transaksi.</li><li>Transaksi Anda telah selesai</li></ol><p class="mb-0"><strong>Internet Banking BNI</strong></p><ol><li>Ketik alamat https://ibank.bni.co.id kemudian klik “Enter”.</li><li>Masukkan User ID dan Password.</li><li>Pilih menu “Transfer”</li><li>Pilih “Virtual Account Billing”.</li><li>Kemudian masukan nomor Virtual Account Anda (${result.account_number}) yang hendak dibayarkan. Lalu pilih rekening debet yang akan digunakan. Kemudian tekan ‘’lanjut’’</li><li>Periksa ulang mengenai transaksi yang anda ingin lakukan</li><li>Masukkan Kode Otentikasi Token.</li><li>Pembayaran Anda berhasil</li></ol><p class="mb-0"><strong>Mobile Banking BNI</strong></p><ol><li>Akses BNI Mobile Banking dari Handphone kemudian masukan user ID dan password.</li><li>Pilih menu "transfer"</li><li>Pilih menu "Virtual Account Billing" kemudian pilih rekening debet.</li><li>Masukan nomor Virtual Account Anda (${result.account_number}) pada menu "input baru"&nbsp;</li><li>Konfirmasi transaksi anda</li><li>Masukan Password transaksi</li><li>Pembayaran anda telah berhasil<br><br></li></ol></div>`
						}

						if (bank == 'prima') {
							tata_cara_pembayaran = `<div class="text-left"><p class="mb-0"><strong>ATM PRIMA / ALTO</strong></p><ol><li>Pada menu utama, pilih transaksi lain</li><li>Pilih Pembayaran Transfer</li><li>Pilih pembayaran lainnya</li><li>Pilih pembayaran Virtual Account</li><li>Masukkan nomor virtual account anda (${result.account_number})</li><li>Pada halaman konfirmasi, akan muncul nominal yang dibayarkan, nomor, dan nama merchant, lanjutkan jika sudah sesuai</li><li>Pilih sumber pembayaran anda dan lanjutkan</li><li>Transaksi anda selesai</li><li>Ketika transaksi anda sudah selesai, invoice anda akan diupdate secara otomatis. Ini mungkin memakan waktu hingga 5 menit.</li></ol><strong></strong></div>`
						}

						if (bank == 'permata') {
							tata_cara_pembayaran = `<div class="text-left"><p class="mb-0"><strong>Permata Mobile X</strong></p><ol><li>Buka Permata Mobile X dan Login</li><li>Pilih Pay "Pay Bills"/ "Pembayaran Tagihan"</li><li>Pilih “Virtual Account”</li><li>Masukkan Nomor Virtual Account anda (${result.account_number})</li><li>Detail pembayaran anda akan muncul di layar</li><li>Nominal yang ditagihkan akan muncul di layar. Pilih sumber pembayaran</li><li>Konfirmasi transaksi anda</li><li>Masukkan kode response token anda</li><li>Transaksi anda telah selesai</li><li>Ketika transaksi anda sudah selesai, invoice anda akan diupdate secara otomatis. Ini mungkin memakan waktu hingga 5 menit.</li></ol><p class="mb-0"><strong>Permata Mobile</strong></p><ol><li>Buka Permata Mobile dan Login</li><li>Pilih Pay "Pay Bills"/ "Pembayaran Tagihan"</li><li>Pilih menu “Transfer”</li><li>Pilih sumber pembayaran</li><li>Pilih “daftar tagihan baru”</li><li>Masukan nomor Virtual Account Anda (${result.account_number})</li><li>Periksa ulang mengenai transaksi yang anda ingin lakukan</li><li>Konfirmasi transaksi anda</li><li>Masukkan SMS token respons</li><li>Pembayaran Anda berhasil</li><li>Ketika transaksi anda sudah selesai, invoice anda akan diupdate secara otomatis. Ini mungkin memakan waktu hingga 5 menit.</li></ol><p class="mb-0"><strong>Internet Banking Permata (PermataNet)</strong></p><ol><li>Buka situs https://new.permatanet.com dan login</li><li>Pilih menu “pembayaran”.</li><li>Pilih menu “Pembayaran Tagihan”.</li><li>Pilih “Virtual Account”</li><li>Pilih sumber pembayaran</li><li>Pilih menu "Masukkan Daftar Tagihan Baru"</li><li>Masukan nomor Virtual Account Anda (${result.account_number})</li><li>Konfirmasi transaksi anda</li><li>Masukkan SMS token response</li><li>Pembayaran Anda berhasil</li><li>Ketika transaksi anda sudah selesai, invoice anda akan diupdate secara otomatis. Ini mungkin memakan waktu hingga 5 menit.</li></ol></div>`
						}

						$('#modal_pembayaran').find('.modal-body').html(`<h2 class="mb-4">Menunggu Pembayaran</h2>
							<h1 class="mb-0 pb-0 text-underline"><u>${result.account_number}</u></h1>
							<small> ( ${result.bank_code} ) - Rp. ${Number(result.expected_amount).toLocaleString()} </small>
							<br /> <small>Pembayaran akan kadaluarsa 1x24 Jam</small>
							${tata_cara_pembayaran == ''? `<p class="text-muted mt-4">Silahkan Lakukan pembayaran ke Virtual Akun diatas </p>`: tata_cara_pembayaran}
						`)
					}

					// CC, bca_klikpay, cimb_clicks, danamon_online
					if (result.hasOwnProperty('payment_type') && ['credit_card', 'bca_klikpay', 'cimb_clicks', 'danamon_online', 'bri_epay'].indexOf(result.payment_type) !== -1) {
						$('#modal_pembayaran').find('.modal-body').html(`<h1 class="mb-0">Berhasil</h1><p class="text-muted">Kami sedang mengarahkan anda pada halaman Berikutnya <br /> <small>Pembayaran akan kadaluarsa 1x24 Jam</small></p>`)
						window.location.href = result.redirect_url
					}

					// Midtrans Bank Transfer
					if (input.data('by') == 'midtrans' && input.data('tipe') == 'bank_transfer') {
						if (input.val() == 'permata') {
							$('#modal_pembayaran').find('.modal-body').html(`<h2 class="mb-4">Menunggu Pembayaran</h2>
								<h1 class="mb-0 pb-0 text-underline"><u>${result.permata_va_number}</u></h1>
								<small> ( PERMATA ) - Rp. ${Number(result.gross_amount).toLocaleString()} </small>
								<p class="text-muted mt-4">Silahkan Lakukan pembayaran ke Virtual Akun diatas <br /> <small>Pembayaran akan kadaluarsa 1x24 Jam</small> </p>`)
						} else if (input.val() == 'echannel') {
							$('#modal_pembayaran').find('.modal-body').html(`<h2 class="mb-4">Menunggu Pembayaran</h2>
								<h1 class="mb-0 pb-0 text-underline"><u>${result.bill_key} (${result.biller_code})</u></h1>
								<small> ( MANDIRI BILL ) - Rp. ${Number(result.gross_amount).toLocaleString()} </small>
								<p class="text-muted mt-4">Silahkan Lakukan pembayaran ke Virtual Akun diatas <br /> <small>Pembayaran akan kadaluarsa 1x24 Jam</small> </p>`)
						} else {
							$('#modal_pembayaran').find('.modal-body').html(`<h2 class="mb-4">Menunggu Pembayaran</h2>
								<h1 class="mb-0 pb-0 text-underline"><u>${result.va_numbers[0].va_number}</u></h1>
								<small> ( ${result.va_numbers[0].bank} ) - Rp. ${Number(result.gross_amount).toLocaleString()} </small>
								<p class="text-muted mt-4">Silahkan Lakukan pembayaran ke Virtual Akun diatas <br /> <small>Pembayaran akan kadaluarsa 1x24 Jam</small> </p>`)
						}
					}

					$('#modal_pembayaran').modal('show');

					setInterval(cek_payment_success, 4000)

				} else {
					alert('Terjadi Error. Silahkan Refresh Kembali')
				}
			},
			error: function(xhr) {
				// console.log(xhr)
				alert('Mohon maaf, terjadi kesalahan. Error : '.xhr.responseText)
			}
		});
	}

	function proses_cc() {
		let validasi = validasi_cc();
		if (!validasi) return;

		let cardData = {
			"card_number": $('#card_number').val(),
			"card_exp_month": $('#card_exp_month').val(),
			"card_exp_year": $('#card_exp_year').val(),
			"card_cvv": $('#card_cvv').val(),
		};

		let options = {
			onSuccess: function(response) {
				// Success to get card token_id, implement as you wish here
				console.log('Success to get card token_id, response:', response);
				cc_token_id = response.token_id;
				$('#modal_cc').modal('hide');

				console.log('This is the card token_id:', cc_token_id);
				// Implement sending the token_id to backend to proceed to next step
				proses_pembayaran()
			},
			onFailure: function(response) {
				// Fail to get card token_id, implement as you wish here
				console.log('Fail to get card token_id, response:', response);
				alert(response.validation_messages.join(','));

				// you may want to implement displaying failure message to customer.
				// Also record the error message to your log, so you can review
				// what causing failure on this transaction.
			}
		};

		MidtransNew3ds.getCardToken(cardData, options);
	}

	function validasi_cc() {
		if ($('#card_number').val() == '') {
			alert('Card Number Harus terisi');
			$('#card_number').focus();
			return false
		}
		if ($('#card_exp_month').val() == '') {
			alert('Card EXP Month Harus terisi');
			$('#card_exp_month').focus();
			return false
		}
		if ($('#card_exp_year').val() == '') {
			alert('Card EXP Year Harus terisi');
			$('#card_exp_year').focus();
			return false
		}

		if ($('#card_cvv').val() == '') {
			alert('Card CVV Year Harus terisi');
			$('#card_cvv').focus();
			return false
		}

		return true
	}

	function cek_payment_success() {
		$.ajax({
			type: "get",
			url: BASE_API + 'riwayat-order/' + <?= $ringkasan['id_order'] ?>,
			data: {
				id_order: <?= $ringkasan['id_order'] ?>,
			},
			dataType: "json",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Authorization", "Bearer " + TOKEN);
			},
			success: function(response) {
				if (response.code == 200) {
					let result = response.riwayat;

					if (result || result != '' && result.status_payments == 'paid' && result.status_order == 'success') {
						window.location.href = '/pendaftaran?payment=success';
					}

				} else {
					alert('Terjadi Error. Silahkan Refresh Kembali')
				}
			},
			error: function(xhr) {
				// console.log(xhr)
				alert('Mohon maaf, terjadi kesalahan. Error : '.xhr.responseText)
			}
		});
	}


	$('#modal_pembayaran').on('hide.bs.modal', function(event) {
		let r = confirm("Nomor VA / QRCode akan berubah dan harus memasukkan dari awal lagi");
		if (r == true) {
			// window.location.href = "/pendaftaran/detail_order/<?= $ringkasan['id_order'] ?>"
		} else {
			event.preventDefault();
		}
	})
</script>
<?= $this->endSection() ?>