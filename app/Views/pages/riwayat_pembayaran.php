<?= $this->extend('template') ?>

<?= $this->section('lib_css') ?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap4.min.css">
<?= $this->endSection() ?>


<?= $this->section('content') ?>

<style type="text/css">
	/*
	Max width before this PARTICULAR table gets nasty. This query will take effect for any screen smaller than 760px and also iPads specifically.
	*/
	@media only screen and (max-width: 760px),
	(min-device-width: 768px) and (max-device-width: 1024px) {

		/* Force table to not be like tables anymore */
		.riwayat-pem table,
		thead,
		tbody,
		th,
		td,
		tr {
			display: block;
		}

		/* Hide table headers (but not display: none;, for accessibility) */
		.riwayat-pem thead tr {
			position: absolute;
			top: -9999px;
			left: -9999px;
		}

		.riwayat-pem tr {
			margin: 0 0 1rem 0;
		}

		.riwayat-pem tr:nth-child(odd) {
			background: #ccc;
		}

		.riwayat-pem td {
			/* Behave  like a "row" */
			border: none;
			border-bottom: 1px solid #eee;
			position: relative;
			padding-left: 50% !important;
		}

		.riwayat-pem td:before {
			/* Now like a table header */
			position: absolute;
			/* Top/left values mimic padding */
			top: 0;
			left: 6px;
			width: 45%;
			padding: 0.75rem;
			padding-right: 10px;
			white-space: nowrap;
		}

		/*
		Label the data
    You could also use a data-* attribute and content for this. That way "bloats" the HTML, this way means you need to keep HTML and CSS in sync. Lea Verou has a clever way to handle with text-shadow.
		*/
		.riwayat-pem td:nth-of-type(1):before {
			content: "Tanggal";
		}

		.riwayat-pem td:nth-of-type(2):before {
			content: "Transaction ID";
		}

		.riwayat-pem td:nth-of-type(3):before {
			content: "Jumlah";
		}

		.riwayat-pem td:nth-of-type(4):before {
			content: "Total Harga";
		}

		.riwayat-pem td:nth-of-type(5):before {
			content: "Status";
		}

		.riwayat-pem td:nth-of-type(6):before {
			content: "Aksi";
		}
	}
</style>


<div class="modal fade" id="modal_pembayaran" tabindex="-1" role="dialog" aria-labelledby="modal_pembayaranLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header p-0">
				<!-- <h5 class="modal-title" id="modal_pembayaranLabel">Modal title</h5> -->
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
			</div>
			<div class="modal-body pt-0">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" id="container_pembayaran" src="" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</div>

<section class="section bg-white riwayat-pem">
	<div class="container nav-container-fi bo-shd pb-4 px-4">
		<div class="row justify-content-center">
			<div class="col-lg-12 px-4">
				<!-- <a class="text-left text-muted" href="index.php?pages=ringkasan"><i class="fa fa-angle-left" aria-hidden="true"></i> Kembali</a> -->
				<h2 class="text-primary my-3 text-center">
					<strong>Riwayat Pembayaran</strong>
				</h2>
				<hr class="mt-1">
			</div>
		</div>

		<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
			<li class="nav-item" role="presentation">
				<a class="nav-link active" id="pills-keranjang-tab" data-toggle="pill" href="#pills-keranjang" role="tab" aria-controls="pills-keranjang" aria-selected="true">Keranjang</a>
			</li>
			<li class="nav-item" role="presentation">
				<a class="nav-link" id="pills-pending-tab" data-toggle="pill" href="#pills-pending" role="tab" aria-controls="pills-pending" aria-selected="false">Pending</a>
			</li>
			<li class="nav-item" role="presentation">
				<a class="nav-link" id="pills-lunas-tab" data-toggle="pill" href="#pills-lunas" role="tab" aria-controls="pills-lunas" aria-selected="false">Lunas</a>
			</li>
		</ul>
		<div class="tab-content" id="pills-tabContent">
			<div class="tab-pane fade show active" id="pills-keranjang" role="tabpanel" aria-labelledby="pills-keranjang-tab">
				<div class="table-responsive" style="width: 100%;">
					<table id="list_keranjang" class="table v-middle table-striped">
						<thead>
							<tr>
								<th>Tgl</th>
								<th>Jumlah</th>
								<th>Total Harga</th>
								<th>Status</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<?php foreach ($list_cart as $cart) : ?>
							<tr>
								<td class="col-4"><?= date('d M Y - H:i:s', strtotime($cart['created_at'])) ?></td>
								<td><?= count($cart['orderdetail']) ?></td>
								<td>Rp. <?= number_format($cart['total_harga']) ?></td>
								<td><?= $cart['status_payments'] ?></td>
								<td>
									<a href="<?= base_url('pendaftaran/ringkasan/' . $cart['id_order']) ?>" class="btn  btn-sm btn-primary box-shadow-none">Bayar</a>
								</td>
							</tr>
						<?php endforeach ?>
					</table>
				</div>
			</div>

			<div class="tab-pane fade" id="pills-pending" role="tabpanel" aria-labelledby="pills-pending-tab">
				<div class="table-responsive" style="width: 100%;">
					<table id="list_pending" class="table table-striped v-middle">
						<thead>
							<tr>
								<th>Tgl</th>
								<th>Transaction ID</th>
								<th>Jumlah</th>
								<th>Total Harga</th>
								<th>Via</th>
								<th>Status</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<?php foreach ($riwayat_pending as $pending) : ?>
							<tr>
								<td class="col-4"><?= date('d M Y - H:i:s', strtotime($pending['created_at'])) ?></td>
								<td><?= $pending['codeorder'] ?></td>
								<td><?= $pending['jumlah'] ?></td>
								<td>Rp. <?= number_format($pending['total_harga']) ?></td>
								<td><?= $pending['payment_method'] ?></td>
								<td><?= $pending['status_payments'] ?></td>
								<td>
									<a href="<?= base_url('pendaftaran/detail_order/' . $pending['id_order']) ?>" class="btn btn-sm btn-secondary box-shadow-none">Menunggu Pembayaran</a>
								</td>
							</tr>
						<?php endforeach ?>
					</table>
				</div>
			</div>

			<div class="tab-pane fade" id="pills-lunas" role="tabpanel" aria-labelledby="pills-lunas-tab">
				<div class="table-responsive" style="width: 100%;">
					<table id="list_lunas" class="table table-striped v-middle">
						<thead>
							<tr>
								<th>Tgl</th>
								<th>Transaction ID</th>
								<th>Jumlah</th>
								<th>Total Harga</th>
								<th>Via</th>
								<!-- <th>Status</th> -->
								<th>Aksi</th>
							</tr>
						</thead>
						<?php foreach ($riwayat_lunas as $lunas) : ?>
							<tr>
								<td class="col-3"><?= date('d M Y - H:i:s', strtotime($lunas['created_at'])) ?></td>
								<td><?= $lunas['codeorder'] ?></td>
								<td><?= $lunas['jumlah'] ?></td>
								<td>Rp. <?= number_format($lunas['total_harga']) ?></td>
								<td><?= $lunas['payment_method'] ?></td>
								<!-- <td><?= $lunas['status_payments'] ?></td> -->
								<td>
									<a href="<?= base_url('pendaftaran/detail_order/' . $lunas['id_order']) ?>" class="btn btn-sm btn-success box-shadow-none">Lunas</a>
								</td>
							</tr>
						<?php endforeach ?>
					</table>
				</div>
			</div>
		</div>

	</div>
</section>

<?= $this->endSection() ?>

<?= $this->section('lib_js') ?>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap4.min.js"></script>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script>
	$(document).ready(function() {
		$('#list_keranjang').DataTable({
			"order": [
				[0, "desc"]
			]
		});

		$('#list_pending').DataTable({
			"order": [
				[0, "desc"]
			]
		});

		$('#list_lunas').DataTable({
			"order": [
				[0, "desc"]
			]
		});
	});

	function proses_pembayaran(id_order) {
		$.ajax({
			type: "post",
			url: "/keranjang/proses_pembayaran",
			data: {
				id_order: id_order
			},
			dataType: "json",
			success: function(response) {
				if (response.code == 200) {
					$('#container_pembayaran').prop('src', response.order.invoice_url);
					$('#modal_pembayaran').modal('show');
				} else {
					alert('Terjadi Error. Silahkan Refresh Kembali')
				}
			},
			fail: function(xhr) {
				alert('Mohon maaf, terjadi kesalahan')
			}
		});
	}
	// render_list_produk()
</script>
<?= $this->endSection() ?>