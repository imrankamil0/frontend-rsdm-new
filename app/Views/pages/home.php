<?= $this->extend('template') ?>

<?= $this->section('content') ?>
<section class="section bg-white" style="padding-bottom: 0.25rem">
    <div class="container nav-container-fi">
        <!-- <header class="wow fadeInUp" data-wow-delay=".1s">
            <div class="row justify-content-center pt-3">
                <div class="col-lg-12 bg-primary text-white pl-4 pr-4 pt-5 pb-3" style="border-radius: 15px;">
                    <label class="pre-label mb-0 text-white">Rumah Sunat Dr Mahdian</label>
                    <h2 class="display-4">
                        <strong>The First Modern</strong> Khitan In Indonesia
                    </h2>
                </div>
            </div>
        </header> -->
    </div>
</section>

<div class="bg-white">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-4 px-5 pt-4 pb-5 box-home">
                <div class="img-res text-center">
                    <h2 class="text-primary">
                        <strong>Pilih Area & Layanan Sunat</strong>
                    </h2>
                    <img src="assets/images/pilih-layanan-sunat.png" class="img-fluid" style="border-radius: 5px;">
                    <!-- <p class="text-primary">Pilih lokasi sunat terlebih dahulu</p> -->
                </div>

                <div class="lokasi-layanan mt-4">
                    <form id="form_filter">
                        <div class="form-group">
                            <label class="label text-capitalize font-size-base text-primary" for="InputGender">Jenis Kelamin Pasien<span class="text-danger">*</span></label>
                            <br>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="lakilaki" name="jenis_kelamin_filter" class="custom-control-input" value="pria" checked>
                                <label class="custom-control-label" for="lakilaki">Laki-laki</label>
                            </div>
                            <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="perempuan" name="jenis_kelamin_filter" class="custom-control-input" value="wanita">
                                <label class="custom-control-label" for="perempuan">Perempuan</label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="label text-capitalize font-size-base text-primary" for="lokasi_filter">Lokasi Layanan Terdekat<span class="text-danger">*</span></label>
                            <select class="form-control form-select select-lokasi-klinik" id="lokasi_filter" aria-label="select" name="id_klinik" required="">
                                <option value="" selected="">Pilih Klinik</option>
                            </select>
                        </div>
                        <!-- <?php if (!session('is_login')) : ?>
                        <?php else : ?>
                            <input type="hidden" id="lokasi_filter" name="id_klinik" value="<?= session('id_provinsi') ?>">
                        <?php endif ?> -->

                        <div class="text-center">
                            <button type="button" id="btn_submit_filter" class="btn btn-primary btn-block box-shadow-none">Cari</button>
                            <div id="alert-button-provinsi" class="alert alert-warning" role="alert" style="display: none">
                                <small>Silahkan Pilih Lokasi Klinik Terdekat Terlebih Dahulu</small>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>


<div class="bg-white" id="container_filter" style="display:none">
    <div class="container nav-container-fi">
        <div class="row justify-content-center">
            <div class="col-lg-12 pl-4 pr-4 pt-5 pb-3">
                <div class="filter-layanan text-left">
                    <p>Hasil '<span class="text-primary"><strong id="result_klinik">Kota - Klinik</strong></span>' dan '<span class="text-primary"><strong id="result_gender">Laki-Laki</strong></span>'</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container nav-container-fi">
        <div class="row justify-content-start" id="container_list_produk">
            <div class="col-lg-3 col-6 mb-3">
                <div class="wow fadeInUp" data-wow-delay=".0s" style="visibility: visible; animation-delay: 0s; animation-name: fadeInUp;">
                    <div class="card card-fill">
                        <div class="card-image">
                            <a href="pendaftaran/detail">
                                <img src="assets/images/dirumah-01.png" class="card-img-top img-hover" data-img="assets/images/dirumah-01.png" data-img-hover="assets/images/dirumah-01.png" alt="...">
                            </a>
                            <small class="card-badge badge badge-warning text-uppercase mr-2 p-2">Payday</small>
                        </div>
                        <div class="card-body p-3 p-lg-3">
                            <div class="justify-content-between align-items-center">
                                <div>
                                    <h2 class="card-title mb-1 h5 mb-2">
                                        <a href="pendaftaran/detail" class="text-dark">
                                            Sunat Di Rumah
                                        </a>
                                    </h2>
                                    <small class="text-muted">
                                        Lokasi: <span class="text-primary"><strong>Kota Tangerang - Kelapa Indah</strong></span>
                                    </small>
                                    <p class="mt-2">Rp <span>2.000.000 </span> </p>
                                </div>
                                <!-- <div>
                                        <a href="pendaftaran/detail" class="d-inline-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to cart">
                                            <i class="icon icon-cart font-size-xl"></i>
                                        </a>
                                    </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal_payment_success" tabindex="-1" role="dialog" aria-labelledby="modal_payment_successLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header p-0">
                <!-- <h5 class="modal-title" id="modal_payment_successLabel">Modal title</h5> -->
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <img src="assets/images/Thank-You.png" class="img-fluid mb-4">
                <p class="mb-0">Terima Kasih, Pembayaran anda berhasil!</p>
                <small class="mb-2">Silahkah Cek Email Anda untuk Melihat Bukti Invoice Pembayaran</small> <br>
                <a class="btn btn-primary btn-rounded p-2 pr-4 mt-3 pl-4 btn-sm box-shadow-none font-size-lg btn-full-radius" href="<?= base_url('pendaftaran') ?>">
                    Daftarkan Teman
                </a>
            </div>
            <!-- <div class="modal-footer">

			</div> -->
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script>
    $(document).ready(function() {
        <?php if (!empty(get_value_method('payment'))) : ?>
            <?php if (get_value_method('payment') == 'success') : ?>
                $('#modal_payment_success').modal('show')
            <?php endif ?>

            <?php if (get_value_method('payment') == 'failed') : ?>
                $('#modal_payment_failed').modal('show')
            <?php endif ?>

            <?php if (get_value_method('payment') == 'cancel') : ?>
                $('#modal_payment_cancel').modal('show')
            <?php endif ?>
        <?php endif ?>

        // Reset Password Berhasil
        <?php if (!empty(get_value_method('reset-password'))) : ?>
            <?php if (get_value_method('reset-password') == 'success') : ?>
                $("#loginComponent").modal("show");
            <?php endif ?>
        <?php endif ?>
    });
</script>
<?= $this->endSection() ?>