<?= $this->extend('template') ?>

<?= $this->section('content') ?>

<section class="section bg-white">
    <div class="container nav-container-fi">
        <div class="row justify-content-center">
            <div class="col-lg-5 pl-4 pr-4 mt-5 rounded shadow">
                <div class="owl-carousel produk-owl">
                    <img src="<?= $detail['image_product_klinik'] ?>" class="img-fluid">
                </div>
            </div>
            <div class="col-lg-7 pl-4 pr-4 mt-5">
                <div class="title-layanan">
                    <h2 class="text-primary mb-2">
                        <strong id="detail_nama"><?= $detail['name'] ?></strong>
                    </h2>
                    <p class="text-muted mb-1 font-size-base">
                        Lokasi: <span class="text-primary "><strong id="detail_klinik"><?= $detail['nameKlinik'] ?> - <?= $detail['alamatKlinik'] ?></strong></span>
                    </p>
                    <div class="text-left text-muted mt-1">
                        <div class="list-inline-item text-dark fw-b" id="detail_harga" style="font-size:30px">Rp <?= number_format($detail['harga_online']) ?></div>
                        <?php if ($detail['harga_online'] < $detail['harga_fix_online']) : ?>
                            <small><del id="detail_harga_fix" style="font-size: 18px;">Rp <?= number_format($detail['harga_fix_online']) ?></del></small>
                        <?php endif ?>

                        <?php if (!empty($detail['namePromo'])) : ?>
                            <br>
                            <small class="badge badge-warning text-uppercase mr-2 p-1 mt-1" id="detail_promo"><?= $detail['namePromo'] ?></small>
                        <?php endif ?>
                    </div>
                </div>
                <div class="d-flex align-items-center box-client mt-4 p-2" style="border-radius: 15px;box-shadow: 0px 0px 2px #d1d1d1;">
                    <div class="img-client">
                        <img src="/assets/images/logo-rsdm.png" class="img-responsive">
                    </div>
                    <div class="info-client mt-3">
                        <span class="text-primary d-block" style="line-height: 5px;"><strong>Rumah Sunat dr Mahdian</strong></span>
                        <span class="text-muted"><small>Klinik Sunat</small></span>
                    </div>
                </div>
                <div class="d-none d-sm-block align-items-center mt-4 p-2">
                    <div class="chat-produk mr-2 list-inline-item">
                        <a class="wa-chat btn btn-outline-success btn-icon btn-sm btn-full-radius" target="_blank" href="https://api.whatsapp.com/send/?phone=628111661005&text=Untuk+Layanan+%0ASunat+Anak+%28Regular%29+%2F+Dewasa+%2F+Gemuk+%2F+Perempuan+%0AUsia+%3A%0ABerat+Badan%3A%0ATinggi+Badan%3A%0ADomisili+%3A&app_absent=0">
                            <i class="fa fa-whatsapp" aria-hidden="true"></i>
                        </a>
                    </div>
                    <?php if (session('is_login')) : ?>
                        <div class="add-produk list-inline-item">
                            <a class="btn btn-primary btn-rounded p-2 pr-4 pl-4 btn-sm box-shadow-none font-size-lg btn-full-radius" onclick="javascript:add_to_cart(<?= $detail['id_product_klinik_detail'] ?>)">
                                Tambah Keranjang
                            </a>
                        </div>
                    <?php else : ?>
                        <div class="add-produk list-inline-item">
                            <a class="btn btn-primary btn-rounded p-2 pr-4 pl-4 btn-sm box-shadow-none font-size-lg btn-full-radius" onclick="$('#modal_belum_login').modal('show')">
                                Beli
                            </a>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mb-5">
            <div class="col-lg-12 pl-4 pr-4 mt-5">
                <h2 class="text-primary" style="font-size: 1.5rem">
                    <strong>Detail Layanan</strong>
                </h2>
                <div class="deskripsi-layanan">
                    <?= (!empty($detail['deskripsiProduct']) ? $detail['deskripsiProduct'] : '') ?>
                </div>
            </div>
        </div>

        <?php if (!empty($detail["rekomendasiProductKlinik"])) : ?>
            <div class="mt-5">
                <h2 class="text-primary" style="font-size: 1.5rem">
                    <strong>Produk Klinik Lainnya</strong>
                </h2>
                <div class="row justify-content-start">
                    <?php foreach ($detail["rekomendasiProductKlinik"] as $produk_lainnya) : ?>
                        <div class="col-lg-3 col-6 mb-3">
                            <div class="wow fadeInUp" data-wow-delay=".0s" style="visibility: visible; animation-delay: 0s; animation-name: fadeInUp;">
                                <div class="card card-fill">
                                    <div class="card-image">
                                        <a href="/pendaftaran/detail/<?= $produk_lainnya['id_product_klinik_detail'] ?>">
                                            <!-- <img src="<?php //echo $produk_lainnya['image_product_klinik'] 
                                                            ?>" class="card-img-top img-hover" data-img="<?= $produk_lainnya['image_product_klinik'] ?>" data-img-hover="<?= $produk_lainnya['image_product_klinik'] ?>" alt="..."> -->
                                        </a>
                                        <?php if (!empty($produk_lainnya["promo"])) : ?>
                                            <small class="card-badge badge badge-warning text-uppercase mr-2 p-2"><?= $produk_lainnya['promo'] ?></small>
                                        <?php endif ?>
                                    </div>
                                    <div class="card-body p-3 p-lg-3">
                                        <div class="justify-content-between align-items-center">
                                            <div>
                                                <h2 class="card-title mb-1 h5 mb-2 ff-poppins fb-l" style="font-size: 20px">
                                                    <a href="<?= base_url() ?>/pendaftaran/detail/<?= $produk_lainnya['id_product_klinik_detail'] ?>" class="text-dark ff-poppins">
                                                        <?= $produk_lainnya['name'] ?>
                                                    </a>
                                                </h2>
                                                <small class="text-muted">
                                                    Lokasi: <span class="text-primary"><strong><?= $detail['nameKlinik'] ?></strong></span>
                                                </small>
                                                <p class="">Rp <span> <?= number_format($produk_lainnya['harga_online']) ?> </span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        <?php endif ?>
    </div>
</section>


<!-- Button Fixed Mobile -->
<div class="fixed-bottom d-block d-sm-none align-items-center p-2 produk-button-book">
    <div class="row justify-content-between ">
        <div class="col-2">
            <a class="wa-chat btn btn-outline-success btn-icon btn-sm rounded" target="_blank" href="#" style="width: 3.9rem; height: 2.8rem;">
                <i class="fa fa-2x fa-whatsapp" aria-hidden="true"></i>
            </a>
        </div>
        <?php if (session('is_login')) : ?>
            <div class="col-10">
                <a class="btn btn-primary btn-block btn-sm font-size-lg rounded box-shadow-none" onclick="javascript:add_to_cart(<?= $detail['id_product_klinik_detail'] ?>)">
                    Tambah Keranjang
                </a>
            </div>
        <?php else : ?>
            <div class="col-10">
                <a class="btn btn-primary btn-block btn-sm font-size-lg rounded box-shadow-none" onclick="$('#modal_belum_login').modal('show')">
                    Beli
                </a>
            </div>
        <?php endif ?>

    </div>
    <!-- <div class="chat-produk mr-2 list-inline-item">
    </div>
    <div class="add-produk list-inline-item">
    </div> -->
</div>
<!-- End Button Fixed Mobile -->


<?= $this->endSection() ?>