    <!-- Navigation -->

    <style type="text/css">
        .nav-container-fi {
            max-width: 900px;
        }
    </style>

    <nav class="navbar navbar-sticky fixed-top navbar-expand-lg py-2">
        <div class="container nav-container-fi">

            <!-- <a class="navbar-brand text-dark" href="<?= (getSegment(2) != 'pembayaran') ? 'javascript:window.history.back()' : '/' ?>">
                <i class="fa fa-angle-left" aria-hidden="true"></i>
                Kembali
            </a> -->

            <a class="navbar-brand text-dark" href="javascript:window.history.back()">
                <i class="fa fa-angle-left" aria-hidden="true"></i>
                Kembali
            </a>

            <div class="d-flex d-lg-none">
                <ul class="navbar-nav flex-row">
                    <?php if (session('is_login')) : ?>
                        <li class="nav-item">
                            <span id="btn_open_profile">
                                <a href="#" class="d-flex nav-link toggle-show" data-show="after_login">
                                    <div class="px-2 py-1"><i class="fa fa-user" aria-hidden="true"></i> Profile</div>
                                </a>
                            </span>
                        </li>
                        <li class="nav-item">
                            <span>
                                <a href="/pendaftaran/riwayat_pembayaran" class="d-flex nav-link">
                                    <div class="px-2 py-1"><i class="fa fa-history" aria-hidden="true"></i> Riwayat</div>
                                </a>
                            </span>
                        </li>
                    <?php else : ?>
                        <li class="nav-item">
                            <span id="btn_open_login">
                                <a href="#" class="d-flex nav-link" data-toggle="modal" data-target="#loginComponent">
                                    <div class="px-2 py-1"><i class="fa fa-user" aria-hidden="true"></i> Login</div>
                                </a>
                            </span>
                        </li>
                    <?php endif ?>
                    <li class="nav-item">
                        <a href="#" class="d-flex nav-link toggle-show" data-show="cartComponent" onclick="refresh_produk_keranjang()">
                            <div class="px-2 py-1"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart <sup class="total_produk_keranjang">0</sup></div>
                        </a>
                    </li>
                    <!-- <li class="nav-item">
                        <a href="#" class="d-flex nav-link toggle-show" data-show="searchComponent">
                            <div class="px-2 py-1"><span class="icon icon-magnifier"></span></div>
                        </a>
                    </li> -->
                </ul>
                <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNavbar" aria-controls="furnitureNavbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="icon icon-menu"></span>
                </button> -->
            </div>

            <!-- Navbar collapse -->

            <div class="navbar-collapse collapse navbar-collapse-sidebar" id="mainNavbar">

                <!-- Mobile search -->

                <!-- <div class="d-block d-lg-none">
                    <div class="p-3">
                        <div class="d-flex justify-content-between align-items-center">
                            <div>
                                <i class="icon icon-layers"></i>
                                <strong>Reveal</strong>
                            </div>
                            <button class="btn p-0" type="button" data-toggle="collapse" data-target="#mainNavbar" aria-controls="mainNavbar" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="icon icon-cross font-size-lg"></span>
                            </button>
                        </div>
                    </div>
                    <div class="bg-light">
                        <div class="form-group form-group-icon">
                            <input type="text" class="form-control form-control-simple" placeholder="Search site">
                            <button class="btn btn-clean"><i class="icon icon-magnifier"></i></button>
                        </div>
                    </div>
                </div> -->

                <ul class="navbar-nav ml-auto align-items-lg-center">
                    <!-- <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown px-lg-3">
                        <a class="nav-link dropdown-toggle" href="#" id="dropdown-shop" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Shop
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdown-shop">
                            <a class="dropdown-item" href="#">Shop</a>
                            <a class="dropdown-item" href="#">Product</a>
                            <a class="dropdown-item" href="#">Checkout</a>
                        </div>
                    </li>
                    <li class="nav-item px-lg-3">
                        <a class="nav-link" href="#">Man's</a>
                    </li>
                    <li class="nav-item px-lg-3">
                        <a class="nav-link" href="#">Woman's</a>
                    </li>
                    <li class="nav-item px-lg-3">
                        <a class="nav-link" href="#">Kid's</a>
                    </li> -->
                    <?php if (session('is_login')) : ?>
                        <li class="nav-item d-none d-lg-inline ml-lg-5">
                            <a href="#" class="d-flex nav-link toggle-show" data-show="after_login">
                                <div class="px-2 py-1"><i class="fa fa-user" aria-hidden="true"></i> Profile</div>
                            </a>
                        </li>
                        <li class="nav-item d-none d-lg-inline">
                            <a href="/pendaftaran/riwayat_pembayaran" class="d-flex nav-link">
                                <div class="py-1"><i class="fa fa-history" aria-hidden="true"></i></div>
                            </a>
                        </li>
                    <?php else : ?>
                        <li class="nav-item d-none d-lg-inline ml-lg-5">
                            <a href="#" class="nav-link" data-toggle="modal" data-target="#loginComponent">
                                <span class="icon icon-user"></span>
                            </a>
                        </li>
                    <?php endif ?>
                    <li class="nav-item d-none d-lg-inline">
                        <a href="#" class="nav-link toggle-show d-flex" data-show="cartComponent" onclick="refresh_produk_keranjang()">
                            <div class="px-2 py-1">
                                <span class="icon icon-cart"></span>
                                <sup class="total_produk_keranjang">0</sup>
                            </div>
                        </a>
                    </li>
                    <!-- <li class="nav-item d-none d-lg-inline">
                        <a href="#" class="nav-link toggle-show" data-show="searchComponent">
                            <span class="icon icon-magnifier"></span>
                        </a>
                    </li> -->
                </ul>
            </div>

        </div>
    </nav>

    <!-- create account  -->
    <div id="CreateComponent" class="modal fade" role="dialog" style="z-index: 99999">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header pt-2 pb-1 ">
                    <!-- <h2 class="modal-title text-center text-primary" id="exampleModalLabel">Login</h2> -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>

                </div>
                <div class="modal-body">
                    <div class="d-flex w-100 align-items-center">
                        <div class="w-100">
                            <div class="p-4 p-lg-5">
                                <div class="mb-5 text-center">
                                    <!-- <img src="/assets/svg/reveal-symbol.svg" alt="" width="80" /> -->
                                    <h2>Buat Akun Baru</h2>
                                </div>
                                <form id="form_buat_akun">
                                    <input type="hidden" name="slug_sosmed" value="<?= get_value_method('via_link') ?>">
                                    <div class="form-group">
                                        <label class="label" for="nama">Nama Lengkap</label>
                                        <input type="text" class="form-control" name="name" aria-describedby="Nama Lengkap" required>
                                    </div>

                                    <div class="form-group">
                                        <label class="label" for="telepon">No Whatsapp</label>
                                        <input type="number" class="form-control" name="telepon" required>
                                    </div>

                                    <!-- <div class="form-group">
                                        <label class="label" for="nik">No. KTP</label>
                                        <input type="number" class="form-control" name="nik" id="nik" required>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="label" for="id_provinsi">Provinsi</label>
                                                <select class="custom-select" name="id_provinsi" id="id_provinsi_daftar" required>
                                                    <option value="" selected>Pilih Provinsi</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label class="label" for="id_kabupatenkota">Kabupaten Kota</label>
                                                <select class="custom-select" name="id_kabupatenkota" id="id_kabupatenkota_daftar" required>
                                                    <option value="" selected>Pilih Kabupaten / Kota</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="label" for="alamat">Alamat</label>
                                        <textarea name="alamat" class="form-control" rows="2"></textarea>
                                    </div> -->


                                    <!-- Sementara -->
                                    <input type="hidden" name="id_negara" value="1">
                                    <!-- <input type="hidden" name="id_provinsi" value="1">
                                    <input type="hidden" name="id_kabupatenkota" value="1"> -->
                                    <input type="hidden" name="id_kecamatan" value="1">
                                    <input type="hidden" name="id_kelurahan" value="1">

                                    <hr>

                                    <div class="form-group">
                                        <label class="label" for="username">Username</label>
                                        <input type="text" class="form-control" name="username" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="label" for="email">Email</label>
                                        <input type="email" class="form-control" name="email" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="label" for="Password">Password <i class="fa fa-eye text-secondary" style="cursor:pointer" onclick="toggle_password(this)"></i></label>
                                        <input type="password" class="form-control" name="password" required>
                                    </div>
                                    <div class="row justify-content-center pt-3">
                                        <div class="col-md-10">
                                            <button type="submit" class="btn btn-block btn-dark box-shadow-none btn-rounded px-5">Buat Akun</button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                </div>
            </div>

        </div>
    </div>
    <!-- end create account -->

    <!-- Forgot Password -->

    <div id="ForgotPassword" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header pt-2 pb-1 ">
                    <!-- <h2 class="modal-title text-center text-primary" id="exampleModalLabel">Login</h2> -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>

                </div>
                <div class="modal-body pt-0">
                    <div class="d-flex w-100 align-items-center">
                        <div class="w-100">
                            <div class="p-4 p-lg-5">
                                <div class="mb-5 text-center">
                                    <img src="/assets/svg/reveal-symbol.svg" alt="" width="80" />
                                    <h2>Lupa Password</h2>
                                    <small>Kami akan mengirimkan intruksi Reset Password melalui Email Anda</small>
                                </div>
                                <form id="form_forgot">
                                    <div class="form-group">
                                        <label class="label" for="email_forgot">Email</label>
                                        <input type="email" class="form-control" id="email_forgot" placeholder="Masukkan email kamu">
                                    </div>
                                    <div class="row justify-content-center pt-3">
                                        <div class="col-md-10">
                                            <button id="kirim-instruksi" type="submit" class="btn btn-block btn-dark box-shadow-none btn-rounded px-5">Kirim Intruksi</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                </div>
            </div>

        </div>
    </div>
    <!-- end forgotr password -->

    <!-- Sidebar login -->
    <div id="loginComponent" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header pt-2 pb-1 ">
                    <!-- <h2 class="modal-title text-center text-primary" id="exampleModalLabel">Login</h2> -->
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>

                </div>
                <div class="modal-body pt-0">
                    <div class="d-flex w-100 align-items-center">
                        <div class="w-100">
                            <div class="pr-3 pl-3 pb-3">
                                <div class="mb-5 text-center">
                                    <!-- <img src="assets/images/logo-rsdm.png" alt="" width="80" /> -->
                                    <!-- <h4 class="display-4">Login</h4> -->
                                </div>
                                <form id="form_login" method="post" action="<?= base_url('pendaftaran/proses_login') ?>">
                                    <div class="form-group">
                                        <label class="label" for="username_login">Username</label>
                                        <input type="text" class="form-control" name="username" id="username_login" aria-describedby="emailHelp" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="label" for="password_login">Password <i class="fa fa-eye text-secondary" style="cursor:pointer" onclick="toggle_password(this)"></i></label>
                                        <input type="password" class="form-control" name="password" id="password_login" required>
                                    </div>
                                    <div class="form-group d-flex justify-content-between">
                                        <div class="custom-control custom-control-sm custom-checkbox custom-checkbox-primary">
                                            <input type="checkbox" class="custom-control-input" id="customExampleCheck">
                                            <label class="custom-control-label" for="customExampleCheck">Ingatkan Saya</label>
                                        </div>
                                        <small>
                                            <a id="lupa-password" href="#" class="text-muted" data-toggle="modal" data-target="#ForgotPassword">Lupa password?</a>
                                        </small>
                                    </div>
                                    <div class="row justify-content-center pt-3">
                                        <div class="col-md-10">
                                            <button type="submit" class="btn btn-block btn-dark box-shadow-none btn-rounded px-5">Login</button>

                                            <!-- <div class="divider-separator">
                                                <span>Or</span>
                                            </div>
                
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-sm btn-block btn-rounded btn-google px-3">Login with Google</button>
                                                <button type="submit" class="btn btn-sm btn-block btn-rounded btn-facebook px-3">Login with Facebook</button>
                                            </div> -->
                                        </div>
                                    </div>
                                </form>
                                <div class="text-center">
                                    <small>
                                        <a id="BuatAkun" href="#" class="text-muted" data-toggle="modal" data-target="#CreateComponent">Buat Akun?</a> <br />
                                        <!-- <a href="#" class="text-muted">Create an account</a> -->
                                    </small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                </div>
            </div>

        </div>
    </div>

    <!-- End Login -->

    <!-- Sidebar After login -->
    <nav id="after_login" class="sidebar bg-white border-right">
        <div class="sidebar-content sidebar-content-warning">

            <!-- Sidebar header -->

            <div class="sidebar-header p-3">
                <button type="button" class="toggle-show close" data-show="after_login">
                    <span class="icon icon-cross"></span>
                </button>
            </div>

            <!-- Sidebar content -->

            <div class="d-flex w-100 align-items-center">
                <div class="w-100">
                    <div class="p-4 p-lg-5">
                        <div class="mb-5 text-center">
                            <img src="/assets/svg/reveal-symbol.svg" alt="" width="80" />
                            <h1>Profile</h1>
                        </div>
                        <table>
                            <tr>
                                <th>Nama</th>
                                <td>: <span id="data_login_name"><?= session('name') ?></span></td>
                            </tr>
                            <tr>
                                <th>Telepon</th>
                                <td>: <span id="data_login_nik"><?= session('telepon') ?></span></td>
                            </tr>
                            <tr>
                                <th>Username</th>
                                <td>: <span id="data_login_username"><?= session('username') ?></span></td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>: <span id="data_login_email"><?= session('email') ?></span></td>
                            </tr>
                        </table>
                        <div class="row justify-content-center pt-3">
                            <div class="col-md-10">
                                <a href="<?= base_url('pendaftaran/logout') ?>" class="btn box-shadow-none btn-block  btn-dark btn-rounded px-5">Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <!-- End After Login -->

    <!-- Sidebar cart -->

    <nav id="cartComponent" class="sidebar sidebar-right bg-white border-left">
        <div class="sidebar-content">

            <!-- Sidebar header -->

            <div class="sidebar-header px-3 pt-3">
                <button type="button" class="toggle-show close" data-show="cartComponent" onclick="refresh_produk_keranjang()">
                    <span class="icon icon-cross"></span>
                </button>
                <div><b> <span class="icon icon-cart mr-2"></span> Keranjang</b></div>
            </div>

            <hr class="mb-0" />

            <!-- Sidebar items -->

            <div class="pt-4">
                <ul class="list-group list-group-flush list_produk_keranjang">
                    <!-- <li id="book" class="close-m list-group-item p-4">

                        <div class="row no-gutters align-items-center">

                            <div class="col-md-2 mb-2 mb-lg-0">
                                <img src="/assets/images/dirumah-01.png" alt="" class="img-fluid br-sm" />
                            </div>
                            <div class="col-6 col-md-5 pl-md-2 mb-2 mb-lg-0 pr-1">
                                <p class="my-0">Sunat Di Rumah</p>
                                <small class="text-muted">Lokasi: <span class="text-primary"><strong>Kota Tangerang - Kelapa Indah</strong></span></small>
                            </div>

                            <div class="col-6 col-md-4 text-right text-muted">
                                <div>Rp 2.000.000</div>
                                <small><del>Rp 2.500.000</del></small>
                            </div>
                            <div class="col-md-1 mb-1 mb-lg-0 close-tr">
                                <div style="cursor: pointer;" class="text-center cur-close" id="clickme">X</div>
                            </div>
                        </div>

                        <div class="row no-gutters align-items-center">
                            <div class="col-md-12 p-2 pt-2 pb-4 pr-3 pl-3 bg-light" style="border-radius: 15px;">

                                <form class="form-edit">
                                    <div class="form-group">
                                        <label class="text-muted" for="exampleInputEmail1"><small>Nama Pasien</small></label>
                                        <input type="text" class="form-control form-control-simple" id="exampleInputEmail1" aria-describedby="emailHelp">
                                    </div>
                                    <div class="form-group">
                                        <label class="text-muted" for="exampleInputalamat"><small>Alamat Pasien</small></label>
                                        <br>
                                        <div class="alamat-sesuai pl-3 mb-2">
                                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                                            <label class="custom-control-label" for="customCheck1">Alamat sesuai akun terdaftar</label>
                                        </div>
                                        <textarea name="alamat" class="form-control form-control-simple" rows="2" cols="30"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-muted" for="berat"><small>Berat</small></label>
                                        <input type="number" class="form-control form-control-simple" id="berat"> kg
                                    </div>
                                    <div class="form-group">
                                        <label class="text-muted" for="tinggi"><small>Tinggi</small></label>
                                        <input type="number" class="form-control form-control-simple" id="tinggi"> cm
                                    </div>
                                    <div class="form-group">
                                        <label class="text-muted" for="tinggi"><small>Tanggal Lahir</small></label>
                                        <input type="date" class="form-control form-control-simple" id="tinggi">
                                    </div>
                                    <div class="form-group">
                                        <label class="text-muted" for="tinggi"><small>Tanggal Tindakan</small></label>
                                        <input type="date" class="form-control form-control-simple" id="tinggi">
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class=" btn btn-primary"><i class="fa fa-sign-in" aria-hidden="true"></i> Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </li>

                    <li id="book-2" class="close-m list-group-item p-4">

                        <div class="row no-gutters align-items-center">

                            <div class="col-md-2 mb-2 mb-lg-0">
                                <img src="/assets/images/dirumah-01.png" alt="" class="img-fluid br-sm" />
                            </div>
                            <div class="col-6 col-md-5 pl-md-2 mb-2 mb-lg-0 pr-1">
                                <p class="my-0">Sunat Di Rumah</p>
                                <small class="text-muted">Lokasi: <span class="text-primary"><strong>Kota Tangerang - Kelapa Indah</strong></span></small>
                            </div>

                            <div class="col-6 col-md-4 text-right text-muted">
                                <div>Rp 2.000.000</div>
                                <small><del>Rp 2.500.000</del></small>
                            </div>
                            <div class="col-md-1 mb-1 mb-lg-0 close-tr">
                                <div style="cursor: pointer;" class="text-center cur-close" id="clickme-2">X</div>
                            </div>
                        </div>

                        <div class="row no-gutters align-items-center">
                            <div class="col-md-12 p-2 pt-2 pb-4 pr-3 pl-3 bg-light" style="border-radius: 15px;">

                                <form class="form-edit">
                                    <div class="form-group">
                                        <label class="text-muted" for="exampleInputEmail1"><small>Nama Pasien</small></label>
                                        <input type="text" class="form-control form-control-simple" id="exampleInputEmail1" aria-describedby="emailHelp">
                                    </div>
                                    <div class="form-group">
                                        <label class="text-muted" for="exampleInputalamat"><small>Alamat Pasien</small></label>
                                        <br>
                                        <div class="alamat-sesuai pl-3 mb-2">
                                            <input type="checkbox" class="custom-control-input" id="customCheck1">
                                            <label class="custom-control-label" for="customCheck1">Alamat sesuai akun terdaftar</label>
                                        </div>
                                        <textarea name="alamat" class="form-control form-control-simple" rows="2" cols="30"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="text-muted" for="berat"><small>Berat</small></label>
                                        <input type="number" class="form-control form-control-simple" id="berat"> kg
                                    </div>
                                    <div class="form-group">
                                        <label class="text-muted" for="tinggi"><small>Tinggi</small></label>
                                        <input type="number" class="form-control form-control-simple" id="tinggi"> cm
                                    </div>
                                    <div class="form-group">
                                        <label class="text-muted" for="tinggi"><small>Tanggal Lahir</small></label>
                                        <input type="date" class="form-control form-control-simple" id="tinggi">
                                    </div>
                                    <div class="form-group">
                                        <label class="text-muted" for="tinggi"><small>Tanggal Tindakan</small></label>
                                        <input type="date" class="form-control form-control-simple" id="tinggi">
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class=" btn btn-primary"><i class="fa fa-sign-in" aria-hidden="true"></i> Simpan</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </li> -->

                    <li class="list-group-item pt-5 p-4 bg-light text-center text-muted">
                        <b>Keranjang Kosong</b> <br>
                        <span>~ Silahkan Pilih produk terlebih dahulu ~</span>
                    </li>

                </ul>

                <!-- Sidebar Buttons -->

                <div class="row justify-content-center py-1 m-1" id="btn_container_checkout" style="display: none;">
                    <?php if (session('is_login')) : ?>
                        <div class="col-9" style="padding-top: 15px;padding-bottom: 15px">
                            <!-- <a href="/pendaftaran/ringkasan" type="submit" class="btn btn-rounded btn-sm btn-block btn-primary px-5">Lanjutkan</a> -->
                            <a href="javascript:checkout_cart()" type="submit" class="btn btn-rounded btn-block btn-primary px-5 box-shadow-none">Lanjutkan</a>
                        </div>
                    <?php else : ?>
                        <div class="col-8" style="padding-top: 15px;padding-bottom: 15px">
                            <a id="blm-login" href="#" class="btn btn-rounded btn-block box-shadow-none btn-primary px-5" onclick="$('#modal_belum_login').modal('show')">Lanjutkan</a>
                        </div>
                    <?php endif ?>
                </div>

                </form>

            </div>

        </div>
    </nav>




    <!-- Sidebar search -->

    <!-- <nav id="searchComponent" class="sidebar sidebar-right bg-white border-left">
        <div class="sidebar-content">
            <div class="sidebar-header px-3 pt-3">
                <button type="button" class="toggle-show close" data-show="searchComponent">
                    <span class="icon icon-cross"></span>
                </button>
                <div>Search content</div>
            </div>
            <hr />
            <div class="p-4">
                <div class="form-group">
                    <label class="label" for="searchContent">Enter keyword</label>
                    <input type="email" class="form-control form-control-simple" id="searchContent" placeholder="Search key words like furniture, sofa...">
                </div>
                <div class="row justify-content-center pt-3">
                    <div class="col-md-10">
                        <button type="submit" class="btn btn-sm btn-block btn-dark btn-rounded px-5">Search the site</button>
                    </div>
                </div>
            </div>
        </div>
    </nav> -->

    <!-- Header -->