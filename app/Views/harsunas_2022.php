<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="msapplication-TileImage" content="https://harsunas.com/wp-content/uploads/2021/04/Harsunas-Site-Icon-300x300.png" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.9/flatpickr.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    <!-- <link rel="stylesheet" href="./assets/css/flashsale.css?24"> -->
    <link rel="stylesheet" href="./assets/css/harsunas.css?3" />
    <link rel="stylesheet" href="./assets/owl-carousel/assets/owl.carousel.min.css" />
    <link rel="stylesheet" href="./assets/owl-carousel/assets/owl.theme.default.min.css" />
    <style>
        .owl-item,
        .owl-item.active {
            opacity: 0.4;
        }

        .owl-item.active.center,
        .owl-item.cloned.active.center {
            opacity: 1;
        }

        .owl-dots button.owl-dot {
            outline: none;
        }

        .owl-theme .owl-dots .owl-dot.active span,
        .owl-theme .owl-dots .owl-dot:hover span {
            background: #EB3431;
        }
    </style>
    <link rel="icon" href="https://harsunas.com/wp-content/uploads/2021/04/Harsunas-Site-Icon-150x150.png" sizes="32x32" />
    <link rel="icon" href="https://harsunas.com/wp-content/uploads/2021/04/Harsunas-Site-Icon-300x300.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="https://harsunas.com/wp-content/uploads/2021/04/Harsunas-Site-Icon-300x300.png" />
    <title>Pendaftaran Hari Sunat Nasional</title>
</head>

<body>
    <!-- <div class="text-center mb-3" style="background-color: #0455A4;"><div class="d-none d-md-block"><img src="./assets/img/hari-sunat-nasional-desktop.jpg?2" class="img-fluid" id="banner_dekstop" alt="Hari Sunat Nasional"></div><div class="d-block d-sm-block d-md-none"><img src="./assets/img/hari-sunat-nasional-mobile.jpg?2" class="img-fluid" id="banner_mobile" alt="Hari Sunat Nasional"></div></div> -->
    <div class="container" style="max-width: 800px">
        <form id="form" action="https://pendaftaran.rumahsunatan.com/harsunas/proses_pertanyaan" autocomplete="off">
            <input type="hidden" name="program" value="harsunas2022" />
            <input type="hidden" name="via" value="harsunas" />
            <input type="hidden" name="id_produk" value="" />
            <input type="hidden" name="info" value="" />
            <!-- <input type="hidden" name="zona" value="1" /> -->
            <div id="step_1">
                <div class="card bg-white my-4 border-0 mb-2" style="border-radius: 2em">
                    <div class="card-body-original px-3">
                        <h1 class="text-center text-daftar mb-0">
                            Pendaftaran
                            <span id="jenis_sunat"></span>
                        </h1>
                    </div>
                </div>

                <!-- <div class="owl-carousel owl-theme mt-4">
                    <div class="item" data-id_tindakan="1" data-title="Sunat Anak" data-info="anak" data-hash="anak">
                        <img style="max-height:180px; max-width:180px" src="./assets/img/sunat-anak.PNG" alt="Anak" class="rounded-circle" />
                    </div>
                    <div class="item" data-id_tindakan="2" data-title="Sunat Dewasa" data-info="dewasa" data-hash="dewasa">
                        <img style="max-height:180px; max-width:180px" src="./assets/img/sunat-dewasa.PNG" alt="Dewasa" class="rounded-circle" />
                    </div>
                    <div class="item" data-id_tindakan="3" data-title="Sunat Gemuk" data-info="gemuk" data-hash="gemuk">
                        <img style="max-height:180px; max-width:180px" src="./assets/img/sunat-anak-gemuk.PNG" alt="Gemuk" class="rounded-circle" />
                    </div>
                    <div class="item" data-id_tindakan="4" data-title="Sunat Premium" data-info="premium" data-hash="premium">
                        <img style="max-height:180px; max-width:180px" src="./assets/img/sunat-premium.PNG" alt="Premium" class="rounded-circle" />
                    </div>
                    <div class="item" data-id_tindakan="5" data-title="Sunat di Rumah" data-info="housecall" data-hash="dirumah">
                        <img style="max-height:180px; max-width:180px" src="./assets/img/sunat-dirumah.PNG" alt="di rumah" class="rounded-circle" />
                    </div>
                    <div class="item" data-id_tindakan="6" data-title="Sunat Anak Berkebutuhan Khusus" data-info="autis" data-hash="abk">
                        <img style="max-height:180px; max-width:180px" src="./assets/img/sunat-autis.PNG" alt="Anak Berkebutuhan Khusus" class="rounded-circle" />
                    </div>
                </div> -->

                <!-- <div class="card mt-2 border-0" style="background-color: #EB3431;" id="container_lokasi_tanggal">
                    <div class="card-header-original pt-4">
                        <h4 class="font-montserrat text-white text-center mb-0">Pilih Lokasi Klinik</h4>
                    </div>
                    <div class="card-body-original px-4 pb-4 pt-2">
                        <div class="row mt-2 justify-content-center">
                            <div class="col-12 col-sm-10 col-md-8">
                                <div class="input-group mb-3">
                                    <label class="input-group-text" for="id_klinik" style="background: #F3E737;color: red;font-size: 30px;border: none;border-top-left-radius: 14px;border-bottom-left-radius: 14px; padding-left: 21px; padding-right: 18px;"><i class="fa fa-map-marker"></i></label>
                                    <select class="form-select select-lokasi-klinik" aria-label="select" name="id_klinik" required=""></select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->

                <div class="card bg-white mt-2 border-0" id="container_data_pasien">
                    <div class="card-body-original p-4">
                        <div class="row mt-2 justify-content-center">
                            <div class="col-sm-12">
                                <div class="mb-3">
                                    <label class="font-montserrat color-theme">Nama Pasien <span class="color-theme">*</span>
                                    </label>
                                    <input name="nama" type="text" class="form-control" placeholder="Nama Pasien" required="" minlength="4" maxlength="48" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mb-3">
                                    <label class="font-montserrat color-theme">Nomor Whatsapp Aktif <span class="color-theme">*</span></label>
                                    <input name="whatsapp" type="tel" class="form-control" placeholder="Nomor Whatsapp" required="" minlength="10" maxlength="18" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="mb-3">
                                    <label class="font-montserrat color-theme">Email Aktif <span class="color-theme">*</span></label>
                                    <input name="email" type="email" class="form-control" placeholder="Email Aktif" required="" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <center class="mb-2 mt-4">
                    <div class="form-group">
                        <button type="submit" class="btn btn-info rounded-pill mb-5 font-montserrat" id="submit" style="color: #f3e737;"> Daftar </button>
                    </div>
                </center>
            </div>
        </form>
    </div>
    <!-- <a href="https://harsunas.com/wChatHarsunas" class="floating-btn-rb animasi-floating-btn" target="_blank" title="Konsultasi Dengan Ahli Kami">
        <i class="fab fa-whatsapp icon-floating"></i>
    </a>
    <a href="https://harsunas.com" class="floating-btn-lb animasi-floating-btn" title="Kembali ke beranda">
        <i class="fas fa-home icon-floating"></i>
    </a> -->
    <!-- <footer class="footer mt-auto py-3" style="background-color: #0455A4;"><div class="container text-center"><p><img src="./assets/img/harsunas-hd.png" class="img-fluid" alt="Hari Sunat Nasional"></p><ul class="list-unstyled"><li class="text-white"><i class="fa fa-fw fa-map-marker" aria-hidden="true"></i> Jl. Warung Buncit Raya No.34 Jakarta Selatan</li><li class="text-white"><i class="fa fa-fw fa-phone" aria-hidden="true"></i><a href="https://api.whatsapp.com/send?phone=628111661005&text=[HARSUNAS]%20Saya%20ingin%20bertanya%20tentang%20sunat%20Pria%20Dewasa%20dan%20Khitan%20Anak%20Gemuk." class="link-white"> 0811-166-1005 (Ratna)</a></li><li class="text-white"><i class="fa fa-fw fa-envelope" aria-hidden="true"></i><a href="mailto:info@harsunas.com" class="link-white">info@harsunas.com</a></li></ul><p class="text-white"><b>Social Media</b></p><ul class="list-inline"><li class="list-inline-item text-white"><a href="https://www.facebook.com/RumahSunatdrMahdian/" class="link-white"><i class="fab fa-fw fa-2x fa-facebook-square" aria-hidden="true"></i></a></li><li class="list-inline-item text-white"><a href="https://www.linkedin.com/company/rumah-sunat-dr-mahdian/" class="link-white"><i class="fab fa-fw fa-2x fa-linkedin-in" aria-hidden="true"></i></a></li><li class="list-inline-item text-white"><a href="https://www.instagram.com/rumahsunatdrmahdianofficial/" class="link-white"><i class="fab fa-fw fa-2x fa-instagram" aria-hidden="true"></i></a></li><li class="list-inline-item text-white"><a href="https://www.youtube.com/channel/UCR8Vp4fzmM4EUjzU-IudRfA" class="link-white"><i class="fab fa-fw fa-2x fa-youtube" aria-hidden="true"></i></a></li></ul><p class="text-white mt-5">&copy; 2021 Organized by DokterM</p></div></footer> -->
    <!-- Alert Popup -->
    <div class="modal fade" id="alert_popup" data-bs-keyboard="false" tabindex="-1" aria-hidden="true"></div>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
    <script src="./assets/owl-carousel/owl.carousel.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/flatpickr/4.6.9/flatpickr.min.js"></script> -->
    <!-- <script src="https://app.midtrans.com/snap/snap.js" data-client-key="Mid-client-CU17MZuPmw0vKuFZ"></script> -->
    <script src="./assets/js/klinik_harsunas.js?3"></script>
    <script src="./assets/js/alert_popup.js?2"></script>
    <!-- <script src="./assets/js/pendaftaran_flashsale.js?2022-04-27-08"></script> -->
    <script>
        function mycallback_owl(event) {
            console.log(event);
            setTimeout(function() {
                let containter  = $(".owl-item.active.center");
                let item        = containter.find(".item");
                let id_tindakan = item.data("id_tindakan");
                let info        = item.data("info");
                let title       = item.data("title");

                $("#jenis_sunat").html(title);
                $("input[name=id_produk]").val(id_tindakan);
                $("input[name=info]").val(info);
                console.log(containter, item, id_tindakan, info);

                if ($("input[name=id_produk]").val()) {
                    $('#container_lokasi_tanggal').fadeIn().focus()
                    // $('#submit').fadeIn();
                }

                console.log(containter, item, id_tindakan, info);
            }, 100);
        }

        function load_klinik(){
            var provinsi = '', html = '<option value="" selected="">Pilih Klinik</option>';
            $.each(daftar_klinik, function(index, data){
            if (provinsi!=data.provinsi && provinsi==''){
                provinsi = data.provinsi; 
                html += '<optgroup label="'+provinsi+'">';
            } else if (provinsi!=data.provinsi && provinsi!=''){
                provinsi = data.provinsi; 
                html += '</optgroup><optgroup label="'+provinsi+'">';
            }
            html += '<option value="'+data.id_klinik+'">'+data.kabupaten_kota+' - '+data.cabang+'</option>';
            });
            html += '</optgroup>';
            $('select[name=id_klinik]').html('');
            $(html).appendTo('select[name=id_klinik]');
        }

        $(document).ready(function($) {
            load_klinik();

            $('select[name=id_klinik]').on('change', function() {
                let id_klinik = $('select[name=id_klinik]').val();
                // let tanggal_rencana = $('input[name=tanggal_rencana]').val();
                
                if(id_klinik){
                    $('#container_data_pasien').fadeIn().focus()
                    $('#submit').fadeIn()
                } else {
                    $('#container_lokasi_tanggal').focus()
                    // $('#container_data_pasien').fadeOut()
                    // $('#submit').fadeOut()
                }
            });

            // $(".owl-carousel").owlCarousel({
            //     margin: 20,
            //     items: 4,
            //     loop: true,
            //     center: true,
            //     URLhashListener: true,
            //     startPosition: "URLHash",
            //     checkVisible: false,
            //     // nav: true,
            //     dots: true,
            //     // dotsEach: 3,
            //     // onDragged: mycallback_owl,
            //     // onDrag:mycallback_owl,
            //     onChange: mycallback_owl,
            //     // stagePadding: 100,
            //     responsiveClass: true,
            //     responsive: {
            //         0: {
            //             items: 2,
            //             dots: false
            //         },
            //         400: {
            //             items: 4,
            //             dots: false
            //         },
            //         550: {
            //             items: 4,
            //             dots: true
            //         },
            //         1000: {
            //             items: 4,
            //             dots: true
            //         },
            //     }
            // });
            
            $('#form').submit(function(e) {
                e.preventDefault();
                let form = $(this);
                let url = form.attr('action');
                let data = form.serialize();
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    dataType: 'JSON',
                    success: function(response) {
                        if (response.success) {
                            alert_popup("#alert_popup", 'success', "<strong>Berhasil !</strong>, tim kami akan segera menghubungi kamu untuk proses lebih lanjut");
                            $('#form').trigger('reset');
                            // $('#container_data_pasien').fadeOut();
                            // $('#submit').fadeOut();
                        } else {
                            alert_popup("#alert_popup", 'warning', "<strong>Gagal !</strong>, silahkan coba lagi");
                        }
                    },
                    error: function(response) {
                        alert_popup("#alert_popup", 'danger', "<strong>ERROR !</strong>, silahkan coba beberapa saat lagi");
                    }
                });
            });
        });
    </script>
</body>

</html>