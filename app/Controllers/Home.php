<?php

namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        return view('welcome_message');
    }

    public function test_api_login()
    {
        $url = 'wilayah';

        $respon = $this->client->post('https://api.ekselen.id/api/login', [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . session('token'),
            ],
            'form_params' => [
                'email' => 'Admin@ekselen.id',
                'password'  => 'admin123'
            ]
        ]);
        $data_res = json_decode($respon->getBody(), true);

        echo $respon->getBody();
    }
}
