<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class Keranjang extends BaseController
{
    protected $cart;

    public  function __construct()
    {
        $this->cart = \Config\Services::cart();
    }

    public function index()
    {
        dd('test!!');

        try {
            $url = 'wilayah';
            if ($this->request->getVar('action') !== 'tambah') $url .= '/' . $this->request->getVar('id_wilayah');

            $respon = $this->client->post($url, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . session('token'),
                ],
                'form_params' => $body
            ]);
            $data_res = json_decode($respon->getBody(), true);
            if ($data_res['code'] === 200) return redirect()->back()->with('notif', ['success', 'Data Berhasil disimpan']);
        } catch (\Throwable $th) {
            return redirect()->back()->with('notif', ['danger', 'Terjadi Kesalahan pada Server.']);
        }

        return ($data_res['code'] === 200) ? redirect()->back()->with('notif', ['success', 'Data Berhasil disimpan']) : redirect()->back()->with('notif', ['danger', 'Data Gagal disimpan { Error : ' . $data_res['nama'] . '}']);
    }

    public function tambah()
    {
        try {
            $data = [
                'id'      => $this->request->getVar('id'),
                'qty'     => 1,
                'price'   => (int) $this->request->getVar('price'),
                'name'    => $this->request->getVar('name'),
                // 'coupon'  => $this->request->getVar('voucher'),
                // 'options' => array('Size' => 'L', 'Color' => 'Red')
            ];
            $this->cart->insert($data);

            $return = [
                'success' => true,
                'msg'     => 'Berhasil ditambahkan'
            ];
        } catch (\Throwable $th) {
            $return = [
                'success' => false,
                'msg'     => 'Gagal ditambahkan',
                'error'   => $th
            ];
        }

        echo json_encode($return);
    }

    public function edit()
    {
        try {
            $data = [
                'rowid'   => $this->request->getVar('rowid'),
                'qty'     => $this->request->getVar('qty'),
                'price'   => (int) $this->request->getVar('price'),
                'name'    => $this->request->getVar('name'),
                // 'coupon'  => $this->request->getVar('voucher'),
                // 'options' => array('Size' => 'L', 'Color' => 'Red')
            ];
            $this->cart->update($data);

            $return = [
                'success' => true,
                'msg'     => 'Keranjang berhasil di update'
            ];
        } catch (\Throwable $th) {
            $return = [
                'success' => false,
                'msg'     => 'Keranjang gagal di update',
                'error'   => $th
            ];
        }

        echo json_encode($return);
    }

    public function list()
    {
        try {
            $return = [
                'success' => true,
                'msg'     => 'List produk berhasil tersedia',
                'list_produk'   => $this->cart->contents(),
            ];
        } catch (\Throwable $th) {
            $return = [
                'success' => false,
                'msg'     => 'Terjadi Error di server',
                'error'   => $th
            ];
        }

        echo json_encode($return);
    }

    public function detail_produk()
    {
        try {
            $return = [
                'success' => true,
                'msg'     => 'Sukses',
                'detail_produk' => $this->cart->get_item($this->request->getVar('rowid')),
            ];
        } catch (\Throwable $th) {
            $return = [
                'success' => false,
                'msg'     => 'Terjadi kesalahan pada server',
                'error'   => $th
            ];
        }

        echo json_encode($return);
    }

    public function total()
    {
        try {
            $return = [
                'success'       => true,
                'total_harga'   => $this->cart->format_number($this->cart->total()),
                'total_produk'  => $this->cart->format_number($this->cart->total_items()),
            ];
        } catch (\Throwable $th) {
            $return = [
                'success' => false,
                'msg'     => 'Terjadi Error di Server',
                'error'   => $th
            ];
        }

        echo json_encode($return);
    }

    public function hapus()
    {
        try {
            $this->cart->remove($this->request->getVar('rowid'));

            $return = [
                'success' => true,
                'msg'     => 'Produk berhasil di hapus'
            ];
        } catch (\Throwable $th) {
            $return = [
                'success' => false,
                'msg'     => 'Produk gagal di hapus',
                'error'   => $th
            ];
        }

        echo json_encode($return);
    }

    public function bersihkan()
    {
        try {
            $this->cart->destroy();

            $return = [
                'success' => true,
                'msg'     => 'Keranjang berhasil di kosongkan'
            ];
        } catch (\Throwable $th) {
            $return = [
                'success' => false,
                'msg'     => 'Keranjang gagal di kosongkan',
                'error'   => $th
            ];
        }

        echo json_encode($return);
    }


    // NEW CART
    public function checkout_cart()
    {
        // var_dump($this->request->getVar('data'), session());
        // die;
        try {
            $biaya_lainnya = 50000;                                 # harga HAZMAT (APD Klinik)
            // $biaya_uniq_code = 10;
            $body = $this->request->getVar('data');
            foreach ($body['header'] as $index => $order) {
                // $body['header'][$index]['harga'] += $biaya_lainnya;

                // Biaya Uniq Code
                // (Sunat Anak zona  1 2 3, Dewasa zona 1 2 3, sunat gemuk)
                if (in_array($order['id_product'], ['1', '2', '3'])) $body['header'][$index]['harga'] += 100;
                // (Sunat Rumah)
                if (in_array($order['id_product'], ['5']))  $body['header'][$index]['harga'] += 160;

                if (!empty($body['header'][$index]['list_voucher']) && !empty($body['header'][$index]['id_voucher'])) {
                    $index_voucher = array_search($body['header'][$index]['id_voucher'], array_column($body['header'][$index]['list_voucher'], 'id_voucher'));
                    $body['header'][$index]['harga'] -= (int) $body['header'][$index]['list_voucher'][$index_voucher]['value_voucher'];
                }
            }

            // $body['header'][0]['harga'] += 10;
            // $body['header'][0]['harga_fix'] += 10;

            // echo json_encode($body['header']);
            // exit;
            // die();

            $url = 'add-cart';

            $respon = $this->client->post($url, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Authorization' => 'Bearer ' . session('token'),
                ],
                // 'form_params' => $this->request->getVar('data')
                'form_params' => $body
            ]);

            $data_res = json_decode($respon->getBody(), true);
        } catch (\Throwable $th) {
            $data_res = [
                'code' => 500,
                'msg'   => 'Error di Server',
                'error' => $th
            ];
        }

        echo json_encode($data_res);
    }

    public function get_cart()
    {
        $m_data = new \App\Models\Data;

        echo json_encode($m_data->getCart($this->request->getVar('id_order'), $this->request->getVar('id_order_detail')));
    }

    public function proses_pembayaran()
    {
        $url = 'checkout-cart/' . $this->request->getVar('id_order');
        $header = [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . session('token'),
            ],
        ];
        if (!empty($this->request->getVar('donasi'))) $header['form_params'] = [
            'harga_donasi'  => $this->request->getVar('donasi'),
            'doa_harapan'  => $this->request->getVar('doa_harapan'),
        ];

        $respon = $this->client->post($url, $header);

        $data_res = json_decode($respon->getBody(), true);
        try {
        } catch (\Throwable $th) {
            $data_res = [
                'code' => 500,
                'msg'   => 'Error di Server',
                'error' => $th
            ];
        }

        echo json_encode($data_res);
    }
}
