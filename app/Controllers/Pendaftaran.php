<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Data;

class Pendaftaran extends BaseController
{
    public function index(){
        return view('harsunas_2022');
    }

    public function index_old()
    {
        tracking_view('Index Halaman Pendaftaran');
        $m_data = new Data();
        $data = [
            'list_data' => $m_data->getListProdukByProvinsi(),
            'list_provinsi' => $m_data->getProvinsi(),
            'list_kabupatenkota' => $m_data->getKabupatenKotaByProvinsi(),
        ];
        return view('pages/home', $data);
    }

    public function detail($id = null)
    {
        if (empty($id)) throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        $m_data = new Data;

        try {
            $data_detail = $m_data->getProdukList($id);

            if (empty($data_detail)) throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        } catch (\Throwable $th) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $data = [
            'detail' => $data_detail,
            'list_data' => $m_data->getListProdukByProvinsi(),
            'list_provinsi' => $m_data->getProvinsi(),
            'list_kabupatenkota' => $m_data->getKabupatenKotaByProvinsi(),
        ];

        tracking_view('Halaman Detail Produk '. $data_detail['name']);
        return view('pages/detail', $data);
    }

    public function ringkasan($id_order = null)
    {
        if (!session('is_login') || empty($id_order)) throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();

        $m_data = new Data();

        $ringkasan = $m_data->riwayatOrderCart($id_order);
        // dd($ringkasan[array_search($id_order, array_column($ringkasan, 'id_order'))]);
        if (empty($ringkasan)) {
            return $this->detail_order($id_order);
            // throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        // dd($ringkasan);
        $data = [
            // 'ringkasan' => (!empty($ringkasan) || count($ringkasan) >= 1) ? (
            //     (!empty($id_order)) ?
            //     $ringkasan[array_search($id_order, array_column($ringkasan, 'id_order'))] :
            //     $ringkasan[count($ringkasan) - 1]) : $ringkasan,
            'ringkasan' => $ringkasan,
            'list_data' => $m_data->getListProdukByProvinsi(),
            'list_provinsi' => $m_data->getProvinsi(),
            'list_kabupatenkota' => $m_data->getKabupatenKotaByProvinsi(),
        ];

        // dd($data['ringkasan']);

        try {
        } catch (\Throwable $e) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        // dd($data['ringkasan']);
        // dd($m_data->getCartPasien());
        tracking_view('Halaman Ringkasan');
        return view('pages/ringkasan', $data);
    }

    public function detail_order($id_order = null)
    {
        if (!session('is_login') || empty($id_order)) throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();

        $m_data = new Data();

        $ringkasan = $m_data->ringkasanPembelian($id_order);
        // dd($ringkasan);
        // dd($ringkasan[array_search($id_order, array_column($ringkasan, 'id_order'))]);
        if (empty($ringkasan)) throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();

        if ($ringkasan['id_pasien'] !== session('id_pasien')) throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();

        $data = [
            // 'ringkasan' => (!empty($ringkasan) || count($ringkasan) >= 1) ? (
            //     (!empty($id_order)) ?
            //     $ringkasan[array_search($id_order, array_column($ringkasan, 'id_order'))] :
            //     $ringkasan[count($ringkasan) - 1]) : $ringkasan,
            'ringkasan' => $ringkasan,
            'list_data' => $m_data->getListProdukByProvinsi(),
            'list_provinsi' => $m_data->getProvinsi(),
            'list_kabupatenkota' => $m_data->getKabupatenKotaByProvinsi(),
        ];

        // dd($data['ringkasan'], session('id_pasien'));

        try {
        } catch (\Throwable $e) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        // dd($data['ringkasan']);
        // dd($m_data->getCartPasien());
        tracking_view('Halaman Detail Order');
        return view('pages/detail_order', $data);
    }

    public function pembayaran($id_order)
    {
        if (!session('is_login') || empty($id_order)) throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();

        $m_data = new Data();

        $ringkasan = $m_data->riwayatOrderCart($id_order);
        // dd($ringkasan[array_search($id_order, array_column($ringkasan, 'id_order'))]);
        if (empty($ringkasan)) {
            return $this->detail_order($id_order);
            // throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $data = [
            // 'ringkasan' => (!empty($ringkasan) || count($ringkasan) >= 1) ? (
            //     (!empty($id_order)) ?
            //     $ringkasan[array_search($id_order, array_column($ringkasan, 'id_order'))] :
            //     $ringkasan[count($ringkasan) - 1]) : $ringkasan,
            'ringkasan' => $ringkasan,
            'list_data' => $m_data->getListProdukByProvinsi(),
            'list_provinsi' => $m_data->getProvinsi(),
            'list_kabupatenkota' => $m_data->getKabupatenKotaByProvinsi(),
        ];
        tracking_view('Halaman Pembayaran');
        return view('pages/pembayaran', $data);
    }

    public function riwayat_pembayaran()
    {
        if (!session('is_login')) throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        $m_data = new Data();

        // dd($m_data->riwayatPayment(), $m_data->riwayatOrderCart());

        $data = [
            'riwayat_lunas' => $m_data->get_riwayat_order_by_status('lunas'),
            'riwayat_pending' => $m_data->get_riwayat_order_by_status('pending'),
            'list_cart' => $m_data->riwayatOrderCart(),
            'riwayat' => $m_data->riwayatPayment(),
            // 'riwayat' => $m_data->riwayatOrderCart(),
            'list_data' => $m_data->getListProdukByProvinsi(),
            'list_provinsi' => $m_data->getProvinsi(),
            'list_kabupatenkota' => $m_data->getKabupatenKotaByProvinsi(),
        ];

        // dd($m_data->riwayatPayment());
        tracking_view('Halaman Riwayat Pembayaran');
        return view('pages/riwayat_pembayaran', $data);
    }

    public function riwayat_order($id_order)
    {
        try {
            $m_data = new Data();
            $result = [
                'code' => 200,
                'msg'  => 'Success',
                'data' => $m_data->get_riwayat_order($id_order)
            ];
        } catch (\Throwable $e) {
            $result = [
                'code' => 400,
                'msg'  => 'Error',
                'Error' => $e
            ];
        }

        tracking_view('Halaman Riwayat Order');
        echo json_encode($result);
    }



    // Auth
    public function proses_login()
    {
        try {
            $rules = [
                'username'        => 'required',
                'password'    => 'required',
            ];
            if (!$this->validate($rules)) return redirect()->back()->withInput()->with('notif', ['warning', $this->validator->getErrors()]);

            $rule = '';

            $body = [
                'username' => $this->request->getVar('username'),
                'password'  => $this->request->getVar('password')
            ];
            $respon = $this->client->post('login-pasien', ['form_params' => $body]);
        } catch (\Throwable $th) {
            return redirect()->back()->withInput()->with('notif', ['danger', 'Email atau Password salah']);
        }

        $data = json_decode($respon->getBody(), true);
        // dd($respon->getBody());
        if (array_key_exists('code', $data)) {
            if ($data['code'] == 404) return redirect()->back()->withInput()->with('notif', ['danger', $data['messages']]);
            if ($data['code'] == 401) return redirect()->back()->withInput()->with('notif', ['danger', $data['messages']]);
        }

        $data_session = $data['pasien'];
        $data_session['is_login'] = TRUE;

        // dd($data_session);

        if (!$data_session['is_active']) return redirect()->back()->withInput()->with('notif', ['danger', 'Akun kamu belum aktif']);

        $session = session();
        $session->set($data_session);

        if (!empty($this->request->getPost('to'))) return redirect()->to(urldecode($this->request->getPost('to')));
        // dd(session('is_login'));

        tracking_view('Halaman Proses Login');
        return ($data['token']) ? redirect()->back() : redirect()->back()->with('notif', ['danger', 'Gagal login']);
    }

    public function proses_register()
    {
        $rules = [
            'nama'        => 'required',
            'email'        => 'required|valid_email|is_unique[pengguna.email]',
            'no_wa'        => 'required|min_length[10]|is_unique[pengguna.no_wa]',
            'password'    => 'required|min_length[5]',
            'confpassword'    => 'matches[password]',
        ];
        if (!$this->validate($rules)) return redirect()->back()->withInput()->with('notif', ['danger', $this->validator->getErrors()]);

        $data = [
            'nama'        => $this->request->getPost('nama'),
            'id_role'    => 4, # Client
            'email'        => $this->request->getPost('email'),
            'no_wa'        => $this->request->getPost('no_wa'),
            'password'    => password_hash($this->request->getVar('password'), PASSWORD_BCRYPT),
            'is_active'    => 1,
        ];

        if(!empty($this->request->getPost('slug_sosmed'))) $data['slug_sosmed'] = $this->request->getPost('slug_sosmed');
        $model     = new MPengguna();
        if (!$model->save($data)) return redirect()->back()->withInput()->with('notif', ['danger', 'Registrasi Gagal']);


        // Email
        $model_site = new \App\Models\MProfileWeb();
        $data['password'] = $this->request->getVar('password');
        $data['created_at'] = date('Y-m-d h:i:s');
        $data = [
            'site'    => $model_site->find(1),
            'user'    => $data,
        ];
        $pesan = view('template/email/account-created', $data);
        $delivered = send_email($this->request->getPost('email'), "Akun Berhasil dibuat", $pesan);
        if (!$delivered) return redirect()->back()->withInput()->with('notif', ['danger', 'Email gagal terkirim']);

        tracking_view('Halaman Proses Registrasi');
        return redirect()->to('/auth')->with('notif', ['success', 'Registrasi Berhasil.. Silahkan melakukan Login']);
    }



    public function forgot($token = null, $id_user = null)
    {
        if (empty($token) || empty($id_user)) throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();

        $m_data = new Data();

        $data = [
            'list_data' => $m_data->getListProdukByProvinsi(),
            'list_provinsi' => $m_data->getProvinsi(),
            'list_kabupatenkota' => $m_data->getKabupatenKotaByProvinsi(),
            "token" => $token,
            "id_user" => $id_user,
        ];

        tracking_view('Halaman Lupa Password');
        return view('pages/forgot', $data);
    }

    public function logout()
    {
        session()->destroy();
        return redirect()->to('/');
    }
}
