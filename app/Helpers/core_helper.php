<?php

function getSegment(int $number)
{
    $request = \Config\Services::request();

    if ($request->uri->getTotalSegments() >= $number && $request->uri->getSegment($number)) {
        return $request->uri->getSegment($number);
    } else {
        return false;
    }
}

function get_value_method($name)
{
    $request = \Config\Services::request();
    return $request->getVar($name);
}


function send_email($to, $title, $message, $attachment = null, $config = null)
{
    try {
        $email = \Config\Services::email();
        if (!empty($config)) $email->initialize($config);

        $email->setFrom('ayotolong@dokterm.com', 'System');
        $email->setTo($to);

        if (!empty($attachment)) {
            if (is_array($attachment)) {
                foreach ($attachment as $attach) {
                    $email->attach($attach);
                }
            } else {
                $email->attach($attachment);
            }
        }

        $email->setSubject($title);
        $email->setMessage($message);

        return $email->send();
    } catch (\Throwable $th) {
        log_message('error', $th);
        return false;
    }
}

function empty_or_not($str = null, $default = null)
{
    return (!empty($str)) ? $str : $default;
}

function dotdot_string($str, $batas = 200)
{
    return (strlen($str) > $batas) ? substr($str, 0, $batas) . ' . . .' : $str;
}

function tracking_view($menu_name=null){
	$request = \Config\Services::request();
	$agent 	 = $request->getUserAgent();
	$db = \Config\Database::connect();
    $builder = $db->table('page_views');
	$builder->insert([
		'url'	=> $request->getPath(),
		'browser'	=> $agent->getBrowser(),
		'mobile'	=> $agent->getMobile(),
		'platform'	=> $agent->getPlatform(),
		'ip_address'=> $request->getIPAddress(),
		'date'		=> date('Y-m-d H:m:s'),
        'menu_name' => $menu_name,
	]);
}