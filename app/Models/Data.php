<?php

namespace App\Models;

use CodeIgniter\Model;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

class Data extends Model
{
    protected $DBGroup              = 'default';
    protected $table                = 'datas';
    protected $primaryKey           = 'id';
    protected $useAutoIncrement     = true;
    protected $insertID             = 0;
    protected $returnType           = 'array';
    protected $useSoftDeletes       = false;
    protected $protectFields        = true;
    protected $allowedFields        = [];

    // Dates
    protected $useTimestamps        = false;
    protected $dateFormat           = 'datetime';
    protected $createdField         = 'created_at';
    protected $updatedField         = 'updated_at';
    protected $deletedField         = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks       = true;
    protected $beforeInsert         = [];
    protected $afterInsert          = [];
    protected $beforeUpdate         = [];
    protected $afterUpdate          = [];
    protected $beforeFind           = [];
    protected $afterFind            = [];
    protected $beforeDelete         = [];
    protected $afterDelete          = [];

    protected $client;

    public function __construct()
    {
        $options = [
            'baseURI' => env('BASE_API'),
            'timeout'  => 15,
            'verify' => false,
            'curl' => [CURLOPT_SSL_VERIFYPEER => false],
        ];
        $this->client = \Config\Services::curlrequest($options);
        // $this->client = new Client([
        //     'base_uri' => env('BASE_API'),
        //     // default timeout 5 detik
        //     'timeout'  => 5,
        // ]);
    }

    public function _get($url, $res)
    {
        $respon = $this->client->request('GET', $url, [
            'headers' => [
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . session('token'),
            ],
        ]);

        $data_res = json_decode($respon->getBody(), true);
        $list_master = ($data_res['code'] === 200) ? $data_res[$res] : [];
        try {
        } catch (\Throwable $th) {
            $list_master = [];
        }

        return $list_master;
    }

    public function getProdukList($id_produk_klinik = null)
    {
        $url = 'search-klinik';
        $data = 'search';
        if (!empty($id_produk_klinik)) {
            $url .= '/' . $id_produk_klinik;
            $data = "productKlinik";
        }

        return $this->_get($url, $data);
    }

    // Cart 
    public function getCart($id_order = null, $id_order_detail = null)
    {
        $url = 'add-cart';
        $data = 'search';

        if (!empty($id_order) && !empty($id_order_detail)) {
            $url .= '/' . $id_order . '/' . $id_order_detail;
            $data = "cartPasien";
        } else if (!empty($id_order)) {
            $url .= '/' . $id_order;
            $data = "cartPasien";
        } else {
            $data = "cartPasien";
        }

        return $this->_get($url, $data);
    }

    public function getCartPasien($id_order = null)
    {
        $url = 'add-cart';
        $data = 'cartPasien';
        if (!empty($id_order)) {
            $url .= '/' . $id_order;
            $data = "cartPasien";
        }

        return $this->_get($url, $data);
    }



    public function riwayatPayment($id_order = null)
    {
        $url = 'index-payment';
        $data = 'payment';
        if (!empty($id_order)) {
            $url .= '/' . $id_order;
            $data = "payment";
        }

        return $this->_get($url, $data);
    }

    public function riwayatOrderCart($id_order = null)
    {
        $url = 'riwayat-order/cart';
        $data = 'riwayat';
        if (!empty($id_order)) {
            $url .= '/' . $id_order;
            $data = "riwayat";
        }

        return $this->_get($url, $data);
    }

    public function ringkasanPembelian($id_order)
    {
        $url = 'ringkasan-pembelian/' . $id_order;
        $data = 'riwayat';

        return $this->_get($url, $data);
    }

    // Detail pasien tindakan order
    public function detailOrder($id_order_detail)
    {
        $url = 'detail-order/' . $id_order_detail;
        $data = 'detailOrder';

        return $this->_get($url, $data);
    }

    public function get_riwayat_order($id_order)
    {
        $url = 'riwayat-order/' . $id_order;
        $data = 'riwayat';

        return $this->_get($url, $data);
    }

    public function get_riwayat_order_by_status($status = 'lunas')
    {
        $url = ($status == 'lunas') ? 'riwayat-success' : 'riwayat-pending';
        $data = 'riwayat';

        return $this->_get($url, $data);
    }


    // Data Publik
    public function getListProdukByProvinsi()
    {
        $url = 'search-klinik';
        $data = 'search';

        return $this->_get($url, $data);
    }

    public function getProvinsi()
    {
        $url = 'provinsi';
        $data = 'provinsi';

        return $this->_get($url, $data);
    }

    public function getKabupatenKota()
    {
        $url = 'kabupatenkota';
        $data = 'kabupatenkota';

        return $this->_get($url, $data);
    }

    public function getKabupatenKotaByProvinsi()
    {
        $url = 'wilayah-kabupaten';
        $data = 'kabupatenKota';

        return $this->_get($url, $data);
    }
}
