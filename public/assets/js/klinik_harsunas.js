const daftar_klinik = [{"provinsi":"Banten","kabupaten_kota":"Kota Tangerang","cabang":"Kelapa Indah","id_klinik":"48","nama":"Kelapa Indah - Kota Tangerang"},{"provinsi":"Banten","kabupaten_kota":"Serang","cabang":"Mayor Syafei","id_klinik":"21","nama":"Mayor Syafei - Serang"},{"provinsi":"Banten","kabupaten_kota":"Tangerang Selatan","cabang":"Bintaro","id_klinik":"2","nama":"Bintaro - Tanggerang Selatan"},{"provinsi":"Banten","kabupaten_kota":"Tangerang Selatan","cabang":"Pamulang","id_klinik":"39","nama":"Pamulang - Tanggerang Selatan"},{"provinsi":"Banten","kabupaten_kota":"Tangerang Selatan","cabang":"Serpong","id_klinik":"14","nama":"Serpong - Tangerang Selatan"},{"provinsi":"DKI Jakarta","kabupaten_kota":"Jakarta Barat","cabang":"Kalideres","id_klinik":"49","nama":"Kalideres - Jakarta Barat"},{"provinsi":"DKI Jakarta","kabupaten_kota":"Jakarta Barat","cabang":"Kebayoran Lama","id_klinik":"31","nama":"Kebayoran Lama - Jakarta Barat"},{"provinsi":"DKI Jakarta","kabupaten_kota":"Jakarta Pusat","cabang":"Cideng","id_klinik":"40","nama":"Cideng - Jakarta Pusat"},{"provinsi":"DKI Jakarta","kabupaten_kota":"Jakarta Selatan","cabang":"Tebet","id_klinik":"50","nama":"Tebet - Jakarta Selatan"},{"provinsi":"DKI Jakarta","kabupaten_kota":"Jakarta Selatan","cabang":"Warung Buncit","id_klinik":"45","nama":"Warung Buncit - Jakarta Selatan"},{"provinsi":"DKI Jakarta","kabupaten_kota":"Jakarta Timur","cabang":"Cipinang","id_klinik":"9","nama":"Cipinang - Jakarta Timur"},{"provinsi":"DKI Jakarta","kabupaten_kota":"Jakarta Utara","cabang":"Plumpang","id_klinik":"43","nama":"Plumpang - Jakarta Utara"},{"provinsi":"Jawa Barat","kabupaten_kota":"Bandung","cabang":"Lengkong","id_klinik":"36","nama":"Lengkong - Bandung"},{"provinsi":"Jawa Barat","kabupaten_kota":"Bekasi","cabang":"Agus Salim","id_klinik":"10","nama":"Agus Salim - Bekasi"},{"provinsi":"Jawa Barat","kabupaten_kota":"Bekasi","cabang":"Jatiasih","id_klinik":"3","nama":"Jatiasih - Bekasi"},{"provinsi":"Jawa Barat","kabupaten_kota":"Bekasi","cabang":"Cibubur","id_klinik":"15","nama":"Jatisampurna - Cibubur"},{"provinsi":"Jawa Barat","kabupaten_kota":"Bekasi","cabang":"Juanda","id_klinik":"11","nama":"Juanda - Bekasi"},{"provinsi":"Jawa Barat","kabupaten_kota":"Bogor","cabang":"Sindang Barang","id_klinik":"32","nama":"Sindang Barang - Bogor"},{"provinsi":"Jawa Barat","kabupaten_kota":"Depok","cabang":"Cimanggis","id_klinik":"12","nama":"Cimanggis - Depok"},{"provinsi":"Jawa Barat","kabupaten_kota":"Depok","cabang":"Margonda","id_klinik":"13","nama":"Margonda - Depok"},{"provinsi":"Jawa Barat","kabupaten_kota":"Depok","cabang":"Sawangan","id_klinik":"24","nama":"Sawangan - Depok"},{"provinsi":"Jawa Barat","kabupaten_kota":"Karawang","cabang":"Telukjambe","id_klinik":"22","nama":"Telukjambe - Karawang"},{"provinsi":"Jawa Barat","kabupaten_kota":"Tasikmalaya","cabang":"Tasikmalaya","id_klinik":"47","nama":"Cibeureum - Tasikmalaya"},{"provinsi":"Jawa Tengah","kabupaten_kota":"Semarang","cabang":"Mintojiwo","id_klinik":"20","nama":"Mintojiwo - Semarang"},{"provinsi":"Jawa Timur","kabupaten_kota":"Surabaya","cabang":"Ngagel","id_klinik":"23","nama":"Ngagel - Surabaya"},{"provinsi":"Kalimantan Selatan","kabupaten_kota":"Banjarbaru","cabang":"Guntung Paikat","id_klinik":"37","nama":"Guntung Paikat - Banjarbaru"},{"provinsi":"Kalimantan Selatan","kabupaten_kota":"Banjarmasin","cabang":"Sungai Bilu","id_klinik":"25","nama":"Sungai Bilu - Banjarmasin"},{"provinsi":"Kalimantan Timur","kabupaten_kota":"Balikpapan","cabang":"DI Panjaitan","id_klinik":"29","nama":"DI Panjaitan - Balikpapan"},{"provinsi":"Kalimantan Timur","kabupaten_kota":"Samarinda","cabang":"Kadrie Oening","id_klinik":"19","nama":"Kadrie Oening - Samarinda"},{"provinsi":"Kepulauan Riau","kabupaten_kota":"Batam","cabang":"Belian","id_klinik":"26","nama":"Belian - Batam"},{"provinsi":"Sumatera Utara","kabupaten_kota":"Medan","cabang":"Setiabudi","id_klinik":"16","nama":"Setiabudi - Medan"}];

const daftar_klinik_gemuk = [
    {
        "provinsi": "DKI Jakarta",
        "kabupaten_kota": "Jakarta Selatan",
        "cabang": "Warung Buncit",
        "id_klinik": "35",
        "nama": "Warung Buncit - Jakarta Selatan"
    }, {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Bekasi",
        "cabang": "Jatiasih",
        "id_klinik": "3",
        "nama": "Jatiasih - Bekasi"
    }
];

const daftar_klinik_kategori_1 = [
    {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Karawang",
        "cabang": "Telukjambe",
        "id_klinik": "22",
        "nama": "Telukjambe - Karawang"
    }, {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Bekasi",
        "cabang": "Jatiasih",
        "id_klinik": "3",
        "nama": "Jatiasih - Bekasi"
    },  {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Bekasi",
        "cabang": "Juanda",
        "id_klinik": "11",
        "nama": "Juanda - Bekasi"
    }, {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Bekasi",
        "cabang": "Agus Salim",
        "id_klinik": "10",
        "nama": "Agus Salim - Bekasi"
    },  {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Bekasi",
        "cabang": "Cibubur",
        "id_klinik": "15",
        "nama": "Jatisampurna - Cibubur"
    }, {
        "provinsi": "DKI Jakarta",
        "kabupaten_kota": "Jakarta Barat",
        "cabang": "Kalideres",
        "id_klinik": "49",
        "nama": "Kalideres - Jakarta Barat"
    }, {
        "provinsi": "DKI Jakarta",
        "kabupaten_kota": "Jakarta Pusat",
        "cabang": "Cideng",
        "id_klinik": "40",
        "nama": "Cideng - Jakarta Pusat"
    }, {
        "provinsi": "DKI Jakarta",
        "kabupaten_kota": "Jakarta Selatan",
        "cabang": "Tebet",
        "id_klinik": "50",
        "nama": "Tebet - Jakarta Selatan"
    }, {
        "provinsi": "DKI Jakarta",
        "kabupaten_kota": "Jakarta Barat",
        "cabang": "Kebayoran Lama",
        "id_klinik": "31",
        "nama": "Kebayoran Lama - Jakarta Barat"
    }, {
        "provinsi": "DKI Jakarta",
        "kabupaten_kota": "Jakarta Selatan",
        "cabang": "Warung Buncit",
        "id_klinik": "45",
        "nama": "Warung Buncit - Jakarta Selatan"
    }, {
        "provinsi": "DKI Jakarta",
        "kabupaten_kota": "Jakarta Timur",
        "cabang": "Cipinang",
        "id_klinik": "9",
        "nama": "Cipinang - Jakarta Timur"
    }, {
        "provinsi": "DKI Jakarta",
        "kabupaten_kota": "Jakarta Utara",
        "cabang": "Plumpang",
        "id_klinik": "43",
        "nama": "Plumpang - Jakarta Utara"
    }, {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Depok",
        "cabang": "Margonda",
        "id_klinik": "13",
        "nama": "Margonda - Depok"
    }, {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Depok",
        "cabang": "Sawangan",
        "id_klinik": "24",
        "nama": "Sawangan - Depok"
    },  {
        "provinsi": "Jawa Timur",
        "kabupaten_kota": "Surabaya",
        "cabang": "Ngagel",
        "id_klinik": "23",
        "nama": "Ngagel - Surabaya"
    }, {
        "provinsi": "Banten",
        "kabupaten_kota": "Tangerang Selatan",
        "cabang": "Serpong",
        "id_klinik": "14",
        "nama": "Serpong - Tangerang Selatan"
    }, {
        "provinsi": "Banten",
        "kabupaten_kota": "Tangerang Selatan",
        "cabang": "Pamulang",
        "id_klinik": "39",
        "nama": "Pamulang - Tanggerang Selatan"
    }, {
        "provinsi": "Banten",
        "kabupaten_kota": "Tangerang Selatan",
        "cabang": "Bintaro",
        "id_klinik": "2",
        "nama": "Bintaro - Tanggerang Selatan"
    }, {
        "provinsi": "Banten",
        "kabupaten_kota": "Tangerang Selatan",
        "cabang": "Kelapa Indah",
        "id_klinik": "48",
        "nama": "Kelapa Indah - Kota Tangerang"
    }, {
        "provinsi": "Banten",
        "kabupaten_kota": "Serang",
        "cabang": "Mayor Syafei",
        "id_klinik": "21",
        "nama": "Mayor Syafei - Serang"
    }, {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Bogor",
        "cabang": "Sindang Barang",
        "id_klinik": "32",
        "nama": "Sindang Barang - Bogor"
    }, {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Bandung",
        "cabang": "Lengkong",
        "id_klinik": "36",
        "nama": "Lengkong - Bandung"
    }, {
        "provinsi": "Kepulauan Riau",
        "kabupaten_kota": "Batam",
        "cabang": "Belian",
        "id_klinik": "26",
        "nama": "Belian - Batam"
    }
];

const daftar_klinik_kategori_2 = [
    {
        "provinsi": "Jawa Tengah",
        "kabupaten_kota": "Semarang",
        "cabang": "Mintojiwo",
        "id_klinik": "20",
        "nama": "Mintojiwo - Semarang"
    }, {
        "provinsi": "Sumatera Utara",
        "kabupaten_kota": "Medan",
        "cabang": "Setiabudi",
        "id_klinik": "16",
        "nama": "Setiabudi - Medan"
    }, {
        "provinsi": "Kalimantan Selatan",
        "kabupaten_kota": "Banjarmasin",
        "cabang": "Sungai Bilu",
        "id_klinik": "25",
        "nama": "Sungai Bilu - Banjarmasin"
    }, {
        "provinsi": "Kalimantan Selatan",
        "kabupaten_kota": "Banjarbaru",
        "cabang": "Guntung Paikat",
        "id_klinik": "37",
        "nama": "Guntung Paikat - Banjarbaru"
    }, {
        "provinsi": "Kalimantan Timur",
        "kabupaten_kota": "Balikpapan",
        "cabang": "DI Panjaitan",
        "id_klinik": "29",
        "nama": "DI Panjaitan - Balikpapan"
    }, {
        "provinsi": "Kalimantan Timur",
        "kabupaten_kota": "Samarinda",
        "cabang": "Kadrie Oening",
        "id_klinik": "19",
        "nama": "Kadrie Oening - Samarinda"
    }
];

const daftar_klinik_kategori_3 = [
    {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Tasikmalaya",
        "cabang": "Tasikmalaya",
        "id_klinik": "47",
        "nama": "Cibeureum - Tasikmalaya"
    }
];

const daftar_klinik_jabodetabek = [
    {
        "provinsi": "DKI Jakarta",
        "kabupaten_kota": "Jakarta Barat",
        "cabang": "Kalideres",
        "id_klinik": "49",
        "nama": "Kalideres - Jakarta Barat"
    }, {
        "provinsi": "DKI Jakarta",
        "kabupaten_kota": "Jakarta Barat",
        "cabang": "Kalideres",
        "id_klinik": "49",
        "nama": "Kalideres - Jakarta Barat"
    }, {
        "provinsi": "DKI Jakarta",
        "kabupaten_kota": "Jakarta Barat",
        "cabang": "Kebayoran Lama",
        "id_klinik": "31",
        "nama": "Kebayoran Lama - Jakarta Barat"
    }, {
        "provinsi": "DKI Jakarta",
        "kabupaten_kota": "Jakarta Pusat",
        "cabang": "Cideng",
        "id_klinik": "40",
        "nama": "Cideng - Jakarta Pusat"
    }, {
        "provinsi": "DKI Jakarta",
        "kabupaten_kota": "Jakarta Selatan",
        "cabang": "Tebet",
        "id_klinik": "50",
        "nama": "Tebet - Jakarta Selatan"
    }, {
        "provinsi": "DKI Jakarta",
        "kabupaten_kota": "Jakarta Selatan",
        "cabang": "Warung Buncit",
        "id_klinik": "45",
        "nama": "Warung Buncit - Jakarta Selatan"
    }, {
        "provinsi": "DKI Jakarta",
        "kabupaten_kota": "Jakarta Timur",
        "cabang": "Cipinang",
        "id_klinik": "9",
        "nama": "Cipinang - Jakarta Timur"
    }, {
        "provinsi": "DKI Jakarta",
        "kabupaten_kota": "Jakarta Utara",
        "cabang": "Plumpang",
        "id_klinik": "43",
        "nama": "Plumpang - Jakarta Utara"
    }, {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Bogor",
        "cabang": "Sindang Barang",
        "id_klinik": "32",
        "nama": "Sindang Barang - Bogor"
    }, {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Depok",
        "cabang": "Cimanggis",
        "id_klinik": "12",
        "nama": "Cimanggis - Depok"
    }, {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Depok",
        "cabang": "Margonda",
        "id_klinik": "13",
        "nama": "Margonda - Depok"
    }, {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Depok",
        "cabang": "Sawangan",
        "id_klinik": "24",
        "nama": "Sawangan - Depok"
    }, {
        "provinsi": "Banten",
        "kabupaten_kota": "Kota Tangerang",
        "cabang": "Kelapa Indah",
        "id_klinik": "48",
        "nama": "Kelapa Indah - Kota Tangerang"
    }, {
        "provinsi": "Banten",
        "kabupaten_kota": "Tangerang Selatan",
        "cabang": "Bintaro",
        "id_klinik": "2",
        "nama": "Bintaro - Tanggerang Selatan"
    }, {
        "provinsi": "Banten",
        "kabupaten_kota": "Tangerang Selatan",
        "cabang": "Pamulang",
        "id_klinik": "39",
        "nama": "Pamulang - Tanggerang Selatan"
    }, {
        "provinsi": "Banten",
        "kabupaten_kota": "Tangerang Selatan",
        "cabang": "Serpong",
        "id_klinik": "14",
        "nama": "Serpong - Tangerang Selatan"
    }, {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Bekasi",
        "cabang": "Agus Salim",
        "id_klinik": "10",
        "nama": "Agus Salim - Bekasi"
    }, {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Bekasi",
        "cabang": "Jatiasih",
        "id_klinik": "3",
        "nama": "Jatiasih - Bekasi"
    }, {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Bekasi",
        "cabang": "Cibubur",
        "id_klinik": "15",
        "nama": "Jatisampurna - Cibubur"
    }, {
        "provinsi": "Jawa Barat",
        "kabupaten_kota": "Bekasi",
        "cabang": "Juanda",
        "id_klinik": "11",
        "nama": "Juanda - Bekasi"
    }
];