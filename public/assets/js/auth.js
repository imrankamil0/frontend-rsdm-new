

// Login
$('#form_login__').submit(function (e) { 
    $.ajax({
        type: "POST",
        url: BASE_API+"login-pasien",
        data: {
            username : $('#username_login').val(),
            password : $('#password_login').val(),
        },
        dataType: "JSON",
        success: function (response) {
            if(response.code == 200){
                localStorage.setItem('data_login', JSON.stringify(response.pasien)); 
                localStorage.setItem('token', response.token); 
                // alert('Selamat datang kembali '+response.pasien.name);
                $('#form_login').trigger('reset')
    
                $("#after_login").addClass("show");
                $("#loginComponent").removeClass("show");
            } else {
                alert(response.messages)
            }
        }, 
        fail: function(xhr, textStatus, errorThrown){
            console.log(xhr, textStatus, errorThrown)
            alert('Login gagal');
         }
    });
    return false;
    e.preventDefault();
});

// Forgot Password
$('#form_forgot').submit(function (e) { 
    $.ajax({
        type: "POST",
        url: BASE_API+"forgot",
        data: {
            email : $('#email_forgot').val()
        },
        dataType: "JSON",
        success: function (response) {
            console.log(response);
            if(response.successfully == 200){
                alert('Silahkan periksa email Anda untuk mereset password');
                $('#form_forgot').trigger('reset')
    
                $("#loginComponent").addClass("show");
                $("#ForgotPassword").removeClass("show");
            } else {
                alert(response.message)
            }
        },
        fail: function(xhr, textStatus, errorThrown){
            console.log(xhr, textStatus, errorThrown)
            alert('Gagal, silahkan ulangi');
        }
    });
    return false;
    e.preventDefault();
});

// Register
$('#form_buat_akun').submit(function (e) { 

    // if($('#id_kabupatenkota_daftar').val() == ''){
    //     alert('Wilayah Kabupaten / Kota Harus diisi');
    //     $('#id_kabupatenkota_daftar').focus()
    //     return false
    // }
    
    let data = $(this).serializeArray();

    $.ajax({
        type: "POST",
        url: BASE_API+"register",
        data: data,
        dataType: "JSON",
        success: function (response) {
            console.log(response);
            if(response.code == 200){
                // alert('Login berhasil! \nSilahkan lakukan login.');
                $('#form_buat_akun').trigger('reset');
    
                $("#CreateComponent").modal("hide");
                $("#loginComponent").modal("show");
            } else {
                alert(response.message)
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr, ajaxOptions, thrownError)
            alert('Gagal, silahkan ulangi.\nError : '+xhr.responseText);
        }
    });
    return false;
    e.preventDefault();
});

// Reset Password
$('#form_reset_password').submit(function (e) { 

    if($('#new_password').val() !== $('#conf_password').val()) {
        alert('Pastikan Password sama');
        $('#conf_password').focus();
        return false;
    }

    let data = $(this).serializeArray();

    $.ajax({
        type: "POST",
        url: BASE_API+"reset-password/"+$('#token_reset').val()+'/'+$('#id_user_reset').val(),
        data: data,
        dataType: "JSON",
        success: function (response) {
            console.log(response);
            if(response.code == 200){
                window.location.href = '/?reset-password=success';
                alert('Reset Password berhasil! \nSilahkan lakukan login dengan password yang baru');
                $('#form_reset_password').trigger('reset');
    
                // $("#loginComponent").modal("show");
            } else {
                alert(response.message)
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr, ajaxOptions, thrownError)
            alert('Gagal, silahkan ulangi.\nError : '+xhr.responseText);
        }
    });
    return false;
    // e.preventDefault();
});