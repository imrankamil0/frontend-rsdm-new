let MY_CART = {
    "header" : []
};

if(localStorage.getItem('MY_CART') != null) {
    MY_CART = JSON.parse(localStorage.getItem('MY_CART'));
} else {
    localStorage.setItem('MY_CART', JSON.stringify(MY_CART))
}

function find_product(id_product_klinik_detail){
    let finding_data = '';
    let id_klinik = '';
    // console.log(id_product_klinik_detail, LIST_DATA)
    LIST_DATA.map(provinsi => {
        for (var i = 0; i < provinsi.klinik.length; i++) {
            let _find_ = provinsi.klinik[i].product.filter(produk => produk.id_product_klinik_detail == id_product_klinik_detail)
            // console.log('proses ...',_find_, provinsi.klinik[i]);
            if(_find_.length > 0){
                finding_data = _find_[0] 
                id_klinik = provinsi.klinik[i].id_klinik
                break;
            }
        }
        // provinsi.klinik.map(klinik => {
        //     let _find_ = klinik.product.filter(produk => produk.id_product_klinik_detail == id_product_klinik_detail)
        //     finding_data = (_find_)? _find_[0] : '';
        //     console.log('cari ', _find_)
        // })
    })

    if(finding_data != '') finding_data.id_klinik = id_klinik;
    // console.log('proses finding data', id_product_klinik_detail, finding_data, id_klinik)

    return finding_data;
}

function value_form(name, form){
    console.log(name, form)
    let find = form.filter(data => data.name == name);
    return find[0].value;
}

function add_to_cart(id_product_klinik_detail){
    console.log('on cart', id_product_klinik_detail)
    let finding_data = find_product(id_product_klinik_detail);
    console.log('hasil finding data', finding_data)

    if(!finding_data) {
        alert('Data Produk tidak ditemukan');
        return 
    }

    let data = {
        "nama" : "",
        "alamat" : "",
        "berat_badan": "",
        "tinggi_badan": "",
        "tanggal_lahir": "",
        "tanggal_tindakan": "",
        "telepon": "",
        "id_voucher": "",
        "harga": finding_data.harga_online,
        "harga_fix": finding_data.harga_fix_online,
        "id_product": finding_data.product.id_product,
        "id_tindakan": finding_data.product.id_tindakan,
        "id_product_klinik": finding_data.id_product_klinik,
        "id_product_klinik_detail": finding_data.id_product_klinik_detail,
        "nama_produk": finding_data.name,
        "img_product": finding_data.image_product_klinik,
        "id_klinik": finding_data.id_klinik,
        "nama_klinik": finding_data.nama_klinik,
        "alamat_klinik": finding_data.alamat_klinik,
        "list_voucher": finding_data.voucher,
        "via_link": get_params('via_link'),
    }

    // console.log('data keranjang add', data, finding_data)

    MY_CART.header.unshift(data);

    localStorage.setItem('MY_CART', JSON.stringify(MY_CART));
    $('#modal_add_success').modal('show') 
    // alert('berhasil dimasukkan ke keranjang');
    refresh_produk_keranjang()
}

function edit_detail_cart(data_form){
    let index_of_form = value_form('index_of_form', data_form);

    let data_old = MY_CART.header[index_of_form];

    MY_CART.header[index_of_form].nama = value_form('nama', data_form);
    MY_CART.header[index_of_form].alamat = value_form('alamat', data_form);
    // MY_CART.header[index_of_form].berat_badan = value_form('berat_badan', data_form);
    // MY_CART.header[index_of_form].tinggi_badan = value_form('tinggi_badan', data_form);
    MY_CART.header[index_of_form].tanggal_lahir = value_form('tanggal_lahir', data_form);
    MY_CART.header[index_of_form].tanggal_tindakan = value_form('tanggal_tindakan', data_form);
    MY_CART.header[index_of_form].telepon = value_form('telepon', data_form);
    MY_CART.header[index_of_form].id_voucher = value_form('id_voucher', data_form);
    MY_CART.header[index_of_form].id_klinik = value_form('id_klinik', data_form);
    
    localStorage.setItem('MY_CART', JSON.stringify(MY_CART)); 
    // refresh_produk_keranjang();
    // alert('berhasil di edit');
}

function edit_voucher_cart(index_of_form, el, id_voucher='', kode_voucher=''){
    MY_CART.header[index_of_form].id_voucher = id_voucher;
    
    localStorage.setItem('MY_CART', JSON.stringify(MY_CART)); 
    // refresh_produk_keranjang();
    if(id_voucher != ''){
        $(el).parent().parent().parent().parent().prev().html(`<i class="fa fa-ticket mr-2 text-success" aria-hidden="true"></i> ${kode_voucher} <!-- | <i class="fa fa-trash mr-2 text-warning" aria-hidden="true" style="cursor:pointer" onclick="edit_voucher_cart(${index_of_form}, ${el})"></i> -->`)
        alert('Voucher berhasil ditambahkan');
    } else {
        $(el).html(`<span class="icon icon-gift mr-2"></span> Kode Promo`)
    }
}

function delete_detail_cart(index_of_form){
    
    var result = confirm("Apakah Anda yakin untuk menghapus?");

    if (result) {
        MY_CART.header.splice(index_of_form, 1);
        localStorage.setItem('MY_CART', JSON.stringify(MY_CART)); 
        refresh_produk_keranjang();
    } else {
        // Do nothing; they cancelled
    }
}

function checkout_cart(){

    let list_input = $('.list_produk_keranjang').find('input[required], textarea[required]')
    for (let i = 0; i < list_input.length; i++) {
        if(!list_input[i].value){
            alert(`Field ${list_input[i].name} wajib di isi`);
            list_input[i].focus()
            return false
        }
    }


    $.ajax({
        type: "POST",
        url: URL_KERANJANG + "checkout_cart",
        data: {
            data : MY_CART
        },
        dataType: "json",
        success: function (response) {
            if(response.code == 200){
                let id_order = response.order.id_order
                // console.log(response.cart[response.cart.length-1].id_order);
                localStorage.setItem('MY_CART', ''); 
                window.location.href = '/pendaftaran/ringkasan/'+id_order
            } else {
                alert('Terjadi Error ketika Checkout')
            }
        },
        fail: function (xhr) {
            alert('Mohon maaf, terjadi kesalahan')
        }
    });
}

$(document).on('submit', 'form.form_edit', function (e) {
    let data_form = $(this).serializeArray()
    edit_detail_cart(data_form)
    e.preventDefault();
    return false;
});

function form_edit_changing(el){
    let data_form = $(el).serializeArray()
    edit_detail_cart(data_form)
}
// $('.form_edit').submit(function (e) { 
// });

// ####################################################
// Template
// ####################################################
function template_produk(data){
    let html = `<div class="col-lg-3 col-6 mb-3">
                    <div class="wow fadeInUp" data-wow-delay=".0s" style="visibility: visible; animation-delay: 0s; animation-name: fadeInUp;">
                        <div class="card card-fill">
                            <div class="card-image">
                                <a href="${BASE_URL}pendaftaran/detail/${data.id_product_klinik_detail}?via_link=${get_params('via_link')}">
                                    <img src="${data.gambar_produk}" class="card-img-top img-hover" data-img="${data.gambar_produk}" data-img-hover="${data.gambar_produk}" alt="...">
                                </a>
                                ${(data.promo) `<small class="card-badge badge badge-warning text-uppercase mr-2 p-2">${data.promo}</small>`}
                            </div>
                            <div class="card-body p-3 p-lg-3">
                                <div class="justify-content-between align-items-center">
                                    <div>
                                         <small class="text-muted">
                                            Lokasi: <span class="text-primary"><strong>${data.klinik.nama}</strong></span>
                                        </small>
                                        <h2 class="card-title mb-1 h5 mb-2 ff-poppins fb-l text-primary" style="font-size: 20px">
                                            <a href="${BASE_URL}pendaftaran/detail/${data.id_product_klinik_detail}?via_link=${get_params('via_link')}" class="text-dark ff-poppins">
                                                ${data.nama}
                                            </a>
                                        </h2>
                                       
                                        <p class="">Rp <span>${Number(data.harga).toLocaleString()} </span> </p>

                                    </div>
                                    <!-- <div>
                                            <a href="${BASE_URL}pendaftaran/detail/${data.id_product_klinik_detail}?via_link=${get_params('via_link')}" class="d-inline-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to cart">
                                                <i class="icon icon-cart font-size-xl"></i>
                                            </a>
                                        </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`;
    return html;
}

function template_produk_keranjang(data, index){
    // console.log('keranjang', data);
    let produk_detail = find_product(data.id_product_klinik_detail);
    let template_voucher;

    if(data.id_voucher != '') {
        let is_voucher = data.list_voucher.filter(voucher => voucher.id_voucher == data.id_voucher);

        // console.log('IS_VOUCHER', is_voucher)
        template_voucher = (is_voucher.length > 0)? `<i class="fa fa-ticket mr-2 text-success" aria-hidden="true"></i> ${is_voucher[0].code_voucher}` : `<span class="icon icon-gift mr-2"></span> Kode Promo`
    } else { 
        template_voucher = `<span class="icon icon-gift mr-2"></span> Kode Promo`
    }

    let html = `<form class="form_edit" method="post" onsubmit="javascript:trigger_submit(this)" onchange="form_edit_changing(this)">
                    <input type="hidden" name="index_of_form" value="${index}">
                    <input type="hidden" name="id_voucher" value="${data.id_voucher}">
                    <input type="hidden" name="id_product" value="${data.id_product}">
                    <input type="hidden" name="id_tindakan" value="${data.id_tindakan}">
                    <input type="hidden" name="id_product_klinik" value="${data.id_product_klinik}">
                    <input type="hidden" name="id_product_klinik_detail" value="${data.id_product_klinik_detail}">
                    <input type="hidden" name="id_klinik" value="${data.id_klinik}">
                    <li id="book" class="close-m list-group-item p-4 mb-2 border-right-0 border-left-0 shadow-sm">
                        <div class="d-flex align-items-center box-client p-2 position-relative">
                            <div class="img-client" style="width: 100px !important">
                                <img src="${produk_detail.image_product_klinik}" class="img-responsive img-fluid">
                            </div>
                            <div class="info-client mt-3">
                                <h4 class="text-primary d-block mb-2" style="line-height: 5px;"><strong>${produk_detail.nameProductKlinikDetail}</strong></h4>
                                <span>
                                    Rp. ${Number(data.harga).toLocaleString()} 
                                    ${(Number(data.harga) < Number(data.harga_fix))? `<del class="text-muted"><small>Rp. ${Number(data.harga_fix).toLocaleString()}</small></del>` : ``}
                                </span>
                                <small class="d-block text-muted">Layanan ini untuk mu? <i class="fa fa-clipboard" aria-hidden="true" style="cursor:pointer" data-toggle="tooltip" data-placement="bottom" title="Klik untuk mengisi form ini sesuai dengan Data Akun kamu" onclick="copy_my_data_to_cart(this)"></i></small>
                            </div>
                            <div style="cursor: pointer; top:10px; right:10px" class="text-center position-absolute cur-close" id="clickme" onclick="delete_detail_cart(${index})"><i class="fa fa-trash text-danger" title="Hapus Keranjang"></i></div>
                        </div>

                        <div class="row no-gutters align-items-center">
                            <div class="col-md-12 p-2 pt-3 pb-4 px-4 bg-light mt-3" style="border-radius: 15px;">
                                <div class="form-group">
                                    <label class="text-muted" for="nama"><small>Nama Pasien</small></label>
                                    <input type="text" class="form-control form-control-simple" name="nama" placeholder="Nama Pasien" value="${data.nama}" required>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="text-muted" for="tinggi"><small>Tanggal Lahir</small></label>
                                        <input type="date" class="form-control form-control-simple tanggal_lahir" name="tanggal_lahir" max="${DATE_NOW}" value="${data.tanggal_lahir}" required>
                                    </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="text-muted" for="telepon"><small>Telepon</small></label>
                                            <input type="number" class="form-control form-control-simple" name="telepon" placeholder="Telepon Pasien" min="10" value="${data.telepon}" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="text-muted" for="exampleInputalamat"><small>Alamat Pasien</small></label>
                                    <!-- 
                                    <br>
                                    <div class="alamat-sesuai pl-3 mb-2">
                                        <input type="checkbox" class="custom-control-input" id="same_data${index}">
                                        <label class="custom-control-label" for="same_data${index}">Alamat sesuai akun terdaftar</label>
                                    </div>
                                    -->
                                    <textarea name="alamat" class="form-control form-control-simple" rows="2" placeholder="Alamat Pasien" name="alamat">${data.alamat}</textarea>
                                </div>

                                <!-- <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label class="text-muted" for="berat"><small>Berat (Kg)</small></label>
                                            <input type="number" class="form-control form-control-simple" name="berat_badan" placeholder="Berat badan Pasien (Kg)"  value="${data.berat_badan}">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group">
                                            <label class="text-muted" for="tinggi"><small>Tinggi (cm)</small></label>
                                            <input type="number" class="form-control form-control-simple" name="tinggi_badan" placeholder="Tinggi badan Pasien (cm)" value="${data.tinggi_badan}">
                                        </div>
                                    </div>
                                </div> -->

                                <div class="form-group">
                                    <label class="text-muted" for="tinggi"><small>Tanggal Tindakan</small></label>
                                    <input type="date" class="form-control form-control-simple tanggal_tindakan" min="${DATE_NOW}" name="tanggal_tindakan" value="${data.tanggal_tindakan}" required>
                                </div>
                                <div class="accordion" id="accordion${index}">
                                    <div class="card card-panel">
                                        <div class="card-header py-2 px-3" id="headingOne${index+1}" data-toggle="collapse" data-target="#card_kode_promo${index}" aria-expanded="true" aria-controls="card_kode_promo">
                                            ${template_voucher}
                                        </div>
                                        <div id="card_kode_promo${index}" class="collapse" aria-labelledby="headingOne${index}" data-parent="#accordion${index}">
                                            <div class="card-body pb-1" id="container_kode_promo">

                                                <div class="form-group mb-2"> 
                                                    <!-- <label class="text-muted">Atau</label> -->
                                                    <div class="input-group"> 
                                                        <input type="text" class="form-control coupon form-control-sm input_kode_voucher" name="kode_voucher" placeholder="Kode Voucher"> 
                                                        <span class="input-group-append"> 
                                                            <button type="button" class="btn btn-primary btn-sm box-shadow-none btn-apply coupon" data-list_voucher='${JSON.stringify(data.list_voucher)}' onclick="searching_voucher(this, ${index})">Apply</button> 
                                                        </span> 
                                                    </div>
                                                </div>

                                                <div class="container_voucher"></div>

                                                <!-- 
                                                ${(data.list_voucher)?
                                                    data.list_voucher.map(voucher => {
                                                        return `<div class="d-flex justify-content-between px-1 py-2">
                                                            <a onclick="edit_voucher_cart(${index}, this, ${voucher.id_voucher}, '${voucher.code_voucher}')">
                                                                <div>
                                                                    <small><i class="fa fa-ticket" aria-hidden="true"></i></small> ${voucher.code_voucher}
                                                                </div>
                                                            </a>
                                                        </div>`
                                                    }) 
                                                    :
                                                    ``
                                                } 
                                                -->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                    

                                <!-- <div class="text-center mt-4">
                                    <button type="submit" class="btn btn-outline-primary box-shadow-none px-5 py-1">Simpan</button>
                                </div> -->
                            </div>
                        </div>
                        
                    </li>
                </form>`;
    return html;
}

function copy_my_data_to_cart(el){
    $(el).parent().parent().parent().next().find('input[name=nama]').val(MY_DATA.name)
    $(el).parent().parent().parent().next().find('input[name=nik]').val(MY_DATA.nik)
    $(el).parent().parent().parent().next().find('input[name=telepon]').val(MY_DATA.telepon)
    $(el).parent().parent().parent().next().find('input[name=tanggal_lahir]').val(MY_DATA.tanggal_lahir)
    $(el).parent().parent().parent().next().find('input[name=berat_badan]').val(MY_DATA.berat_badan)
    $(el).parent().parent().parent().next().find('input[name=tinggi_badan]').val(MY_DATA.tinggi_badan)
    $(el).parent().parent().parent().next().find('textarea[name=alamat]').val(MY_DATA.alamat)

    $(el).parent().parent().parent().parent().parent().trigger('change')
}

function searching_voucher(el, index){
    let list_voucher = $(el).data('list_voucher')
    let find_code_string = $(el).parent().prev().val()
    
    if(find_code_string == '') return false;
    if(find_code_string.replace(" ", "") == '') return false;
    // console.log('FIND STTRING', find_code_string)

    let result = `<div class="d-flex justify-content-between px-1 py-2">
                    <small><a href="javascript:void(0)" class="text-muted"> ~ Voucher tidak ditemukan ~ </a></small>
                </div>`;

    if(list_voucher){
        // let _result_ = list_voucher
        //             .filter(voucher => voucher.code_voucher.indexOf(find_code_string) !== -1)
        //             .map(voucher => `  <div class="d-flex justify-content-between px-1 py-2">
        //                                     <a onclick="edit_voucher_cart(${index}, this, ${voucher.id_voucher}, '${voucher.code_voucher}')">
        //                                         <div>
        //                                             <small><i class="fa fa-ticket" aria-hidden="true"></i></small> ${voucher.code_voucher}
        //                                         </div>
        //                                     </a>
        //                                 </div>`);
        // // console.log('result',_result_)
        // if(_result_.length > 0) result = _result_;

        let _result_ = list_voucher.filter(voucher => voucher.code_voucher == find_code_string)
        console.log('VOUCHER', _result_)
        if(_result_.length > 0){
            $(el).parent().parent().parent().parent().parent().prev().html(`<i class="fa fa-ticket mr-2 text-success" aria-hidden="true"></i> ${_result_[0].code_voucher}`).trigger('click');
            $(el).parent().parent().parent().next('.container_voucher').html('');
            $(el).find('textarea[name=alamat]').focus();
            // console.log('FOCUS',$(el).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().prev('input[name=id_product_klinik]').val())
            $(el).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().find('input[name=id_voucher]').val(_result_[0].id_voucher)
            alert('Voucher ditemukan');

            // CHECK Promo JSM
            if([2,3].indexOf(_result_[0].id_promo) != -1){ 
                let el_tgl_tindakan = $(el).parent().parent().parent().parent().parent().parent().parent().parent().find('input[name=tanggal_tindakan]');
                if(el_tgl_tindakan.val()){
                    let day_tgl_tindakan = moment(el_tgl_tindakan.val()).weekday();
                    if(day_tgl_tindakan==0||day_tgl_tindakan==6||day_tgl_tindakan==5){
                        $(el_tgl_tindakan).flatpickr({
                            minDate: "today",
                            dateFormat: "Y-m-d",
                            disableMobile: "true",
                            "disable": [
                                function(date) {
                                    return (date.getDay() === 0 || date.getDay() === 6 || date.getDay() === 5);
                                }
                            ],
                            "locale": {
                                "firstDayOfWeek": 0
                            }
                        }).clear();
                        alert('Voucher ini hanya tersedia untuk Hari Senin s/d Kamis');
                        el_tgl_tindakan.focus();
                    }
                }

                // $(el).parent().parent().parent().parent().parent().parent().parent().parent().find('input[name=tanggal_tindakan]').val()
            }
            $(el).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().trigger('change')
            return
        }

    }

    $(el).parent().parent().parent().next('.container_voucher').html(result);

    // console.log('List Voucher', list_voucher, find_code_string, result)
}

$(document).on('change', 'input.tanggal_tindakan.flatpickr-input', function () {
    // console.log("CHANGE :",$(this).parent().next().find('button.coupon'))
    $(this).parent().next().find('button.coupon').trigger('click');
});

$(document).on('keypress', 'input.input_kode_voucher', function (event) {
    if (event.keyCode === 13) {
        $(this).next().children('button').click();
        // alert('enter');
        return false
    }

});
// $('form.form_edit').submit(function (e) { 
//     edit_produk_keranjang($(this).serializeArray())
//     console.log((this).serializeArray())
//     e.preventDefault();
//     return false;
// });

function trigger_submit(el){
    edit_produk_keranjang($(el).serializeArray())
    console.log((el).serializeArray())
    // e.preventDefault();
    return false;
}

// ####################################################
// API
// ####################################################
function refresh_produk(){
    $.ajax({
        type: "POST",
        url: BASE_API + "/produk_filter_wilayah",
        data: {
            id_lokasi : $('#lokasi_filter').val(),
            jenis_kelamin : $('input[name="jenis_kelamin_filter"]:checked').val(),
        },
        dataType: "JSON",
        success: function (result) {
            $('#container_list_produk').html('');

            let html = result.data_produk.map(produk => template_produk(produk))
            $('#container_list_produk').html(html);
        }
    });
}

function get_detail_produk(id){
    $.ajax({
        type: "POST",
        url: BASE_API + "/produk_detail/"+id,
        data: {
            id : id,
        },
        dataType: "JSON",
        success: function (result) {
            return result;
        }
    });
}


// ####################################################
// Render Cart HTML
// ####################################################
function refresh_total(){
    let total = total_produk_keranjang();
    $('.total_produk_keranjang').html(Number(total.total_produk).toLocaleString());
    $('.total_harga').html(Number(total.total_harga).toLocaleString());
}

function refresh_produk_keranjang(){
    let data = JSON.parse(localStorage.getItem('MY_CART'))
    // console.log("List Keranjang : ", data)
    if(!data) return

    let list = data.header;
    let html = `<li class="list-group-item p-4 bg-light text-center text-muted">
                    <b>Keranjang Kosong</b> <br>
                    <span>~ Silahkan Pilih produk terlebih dahulu ~</span>
                </li>`;
    $('#btn_container_checkout').hide();

    if(list.length > 0){
        $('#btn_container_checkout').show();
        html = list.map((produk, index) => template_produk_keranjang(produk, index));
    }

    $('#total_cart, .total_produk_keranjang').html(list.length)
    $('.list_produk_keranjang').html(html);
    $('[data-toggle="tooltip"]').tooltip();

    $(".tanggal_lahir").flatpickr({
        maxDate: "today",
        dateFormat: "Y-m-d",
        disableMobile: "true",
    });

    // console.log('DATA FLATFICKER : ', moment().add(2, 'days').format('YYYY-MM-DD'));
    $(".tanggal_tindakan").flatpickr({
        // minDate: moment().format('YYYY-MM-DD'),
        minDate: moment().add(2, 'days').format('YYYY-MM-DD'),
        dateFormat: "Y-m-d",
        disableMobile: "true",
        // "disable": [
        //     function(date) {
        //         return (date.getDay() === 0 || date.getDay() === 6 || date.getDay() === 5);
        //     }
        // ],
        // "locale": {
        //     "firstDayOfWeek": 1
        // }
    });
}

$(document).ready(function() {
    refresh_produk_keranjang()
});

refresh_produk_keranjang()