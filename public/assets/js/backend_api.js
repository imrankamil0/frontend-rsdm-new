// let LIST_DATA;

// Template Produk (satuan)
function html_produk(klinik, data){
    console.log('html produk', data)
    let html_produk = ``;

    // let __data__ = data.filter(produk => produk.gender == $('input[name=jenis_kelamin_filter]:checked').val());
    // if(__data__.length <= 1) return `<div class="col-12 mb-3"><center>Tidak Ada Layanan</center></div>`
    
    data.map((produk) => {
        html_produk +=  `<div class="col-lg-3 col-6 mb-3">
                    <div class="wow fadeInUp" data-wow-delay=".0s" style="visibility: visible; animation-delay: 0s; animation-name: fadeInUp;">
                        <div class="card card-fill shadow">
                            <div class="card-image">
                                <a href="${BASE_URL}pendaftaran/detail/${produk.id_product_klinik_detail}?via_link=${get_params('via_link')}">
                                    <img src="${produk.image_product_klinik}" class="card-img-top img-hover" data-img="${produk.image_product_klinik}" data-img-hover="${produk.image_product_klinik}" alt="${produk.name}">
                                </a>
                                ${(produk.promo != null) ? `<small class="card-badge badge badge-warning text-uppercase mr-2 p-2">${produk.promo}</small>` : ``}
                            </div>
                            <div class="card-body p-3 p-lg-3">
                                <div class="justify-content-between align-items-center">
                                    <div>
                                        <small class="text-muted">
                                            Lokasi: <span class="text-primary"><strong>${klinik}</strong></span>
                                        </small>
                                        <h3 class="card-title mb-1 h5 mb-2 ff-poppins fb-l" style="font-size: 18px">
                                            <a href="${BASE_URL}pendaftaran/detail/${produk.id_product_klinik_detail}?via_link=${get_params('via_link')}" class="text-primary">
                                                ${produk.nameProductKlinikDetail}
                                            </a>
                                        </h3>
                                        
                                        <p class="text-dark mb-0">Rp <span>${Number(produk.harga_online).toLocaleString()} </span> </p>
                                        ${(produk.harga_online < produk.harga_fix_online)? `<del class="text-muted"><small>Rp. ${Number(produk.harga_fix_online).toLocaleString()}</small></del>` : ``}
                                        

                                    </div>
                                    <!-- <div>
                                            <a href="${BASE_URL}pendaftaran/detail" class="d-inline-block" data-toggle="tooltip" data-placement="top" title="" data-original-title="Add to cart">
                                                <i class="icon icon-cart font-size-xl"></i>
                                            </a>
                                        </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`;
    })

    // console.log('return html', html_produk)
    return html_produk;
}

// Template Kode Promo
function html_kode_promo(data){
    return  `<div class="d-flex justify-content-between p-1 pt-3 pb-3">
                <a href="#">
                    <div>
                        <small><i class="fa fa-ticket" aria-hidden="true"></i></small> ${data.kode}
                    </div>
                    <!-- <span><small>-Rp 500.000</small></span> -->
                </a>
            </div>`;
}

function getAllListProduk(id=null){
    let url = BASE_API+'search-klinik';
    if(id != null) url += '/'+id;

    let data = null;

    $.ajax({
        type: "GET",
        async: false,
        url: url,
        data: {
            id_wilayah: $('#lokasi_filter').val(),
            jenis_kelamin: $('input[name=jenis_kelamin_filter]:checked').val(),
        },
        dataType: "json",
        success: function (response) {
            console.log(response);
            if(response.code == 200){
                data = response.product
            } else {
                alert(response.message)
            }
        },
        error: function(xhr, textStatus, errorThrown){
            console.log(xhr, textStatus, errorThrown)
            alert('Error, ketika mengambil List Produk');
        }
    });

    return data;
}

function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

function render_filter_wilayah(){
    console.log('Data : ',LIST_DATA);
    let uniq_provinsi = [];
    let uniq_kabupaten_kota = [];

    LIST_DATA.filter(function(item){
        var i = uniq_provinsi.findIndex(x => (x.id_provinsi == item.id_provinsi && x.provinsi == item.provinsi));
        if(i <= -1){
              uniq_provinsi.push(item);
        }
        return null;
    });

    LIST_DATA.filter(function(item){
        var i = uniq_kabupaten_kota.findIndex(x => (x.id_kabupatenkota == item.id_kabupatenkota && x.kabupatenkota == item.kabupatenkota));
        if(i <= -1){
              uniq_kabupaten_kota.push(item);
        }
        return null;
    });

    // console.log('UNIQ : ', uniq_provinsi, uniq_kabupaten_kota);
    
    let html_provinsi = uniq_provinsi.map(provinsi => {
        return `<optgroup label="${provinsi.provinsi}"> 
                    ${ 
                        uniq_kabupaten_kota.filter(kabkota => kabkota.id_provinsi == provinsi.id_provinsi)
                                            .map(kabkota => `<option value="${kabkota.id_kabupatenkota}">${kabkota.kabupatenkota}</option>`).join(' ')
                    } 
                </optgroup>`
    })

    $('#lokasi_filter').html(`<option value="" selected="">Pilih Wilayah</option> ${html_provinsi}`)
}

(function(){
    render_filter_wilayah()

    $.ajax({
        type: "GET",
        url: BASE_API+"provinsi",
        dataType: "JSON",
        success: function (response) {
            if(response.code == 200){
                let html_provinsi = response.provinsi.map(provinsi => `<option value="${provinsi.id_provinsi}">${provinsi.provinsi}</option>`);
                $('#id_provinsi_daftar').html(`<option value="" selected>Pilih Provinsi</option> ${html_provinsi}`)

                $('#id_kabupatenkota_daftar').html(`<option value="" selected>Pilih Kabupaten Kota</option>`).prop('disabled', true);
            } else {
                alert('Terjadi Error. Silahkan Refresh Kembali')
            }
        },
        error: function (xhr) {
            alert('Mohon maaf, terjadi kesalahan')
        }
    });

    $('#id_provinsi_daftar').on('change', function () {
        if($(this).val() == ''){
            $('#id_kabupatenkota_daftar').val('').prop('disabled', true)
        }

        let kabupatenkota = LIST_KABUPATENKOTA.filter(provinsi => provinsi.id_provinsi==$(this).val());
        kabupatenkota = kabupatenkota[0].kabupatenkota.map(kabupatenkota => `<option value="${kabupatenkota.id_kabupatenkota}">${kabupatenkota.kabupatenkota}</option>`).join(' ');
        $('#id_kabupatenkota_daftar').html(`<option value="" selected>Pilih Kabupaten Kota</option> ${kabupatenkota}`).prop('disabled', false);
    });

    // $.ajax({
    //     type: "GET",
    //     url: BASE_API+"kabupatenkota",
    //     dataType: "JSON",
    //     success: function (response) {
    //         if(response.code == 200){
    //             let html_kabupatenkota = response.kabupatenkota.map(kabupatenkota => `<option value="${kabupatenkota.id_kabupatenkota}">${kabupatenkota.kabupatenkota}</option>`);
    //             $('#id_kabupatenkota_daftar').html(`<option value="" selected>Pilih Kabupaten Kota</option> ${html_kabupatenkota}`)
    //         } else {
    //             alert('Terjadi Error. Silahkan Refresh Kembali')
    //         }
    //     },
    //     error: function (xhr) {
    //         alert('Mohon maaf, terjadi kesalahan')
    //     }
    // });
})()

// Produk Non-Promo
function getProduk(id=null){
    let url = BASE_API+'list-product/non-promo';
    if(id != null) url += '/'+id;

    let data = null;

    $.ajax({
        type: "GET",
        async: false,
        url: url,
        data: {
            id_wilayah: $('#lokasi_filter').val(),
            jenis_kelamin: $('input[name=jenis_kelamin_filter]:checked').val(),
        },
        dataType: "json",
        success: function (response) {
            console.log(response);
            console.log(response);
            if(response.code == 200){
                data = response.data
            } else {
                alert(response.message)
            }
        },
        error: function(xhr, textStatus, errorThrown){
            console.log(xhr, textStatus, errorThrown)
            alert('Error, ketika mengambil List Produk');
        }
    });

    return data;
}

// Produk Promo
function getProdukPromo(id=null){
    let url = BASE_API+'list-product/promo';
    if(id != null) url += '/'+id;

    let data = null;

    $.ajax({
        type: "GET",
        async: false,
        url: url,
        data: {
            id_wilayah: $('#lokasi_filter').val(),
            jenis_kelamin: $('input[name=jenis_kelamin_filter]:checked').val(),
        },
        dataType: "json",
        success: function (response) {
            console.log(response);
            if(response.code == 200){
                data = response.data
            } else {
                alert(response.message)
            }
        },
        error: function(xhr, textStatus, errorThrown){
            console.log(xhr, textStatus, errorThrown)
            alert('Error, ketika mengambil Produk Promo');
        }
    });

    return data;
}

// Voucher Produk
function getVoucher(id=null){
    let url = BASE_API+'list-product/voucher';
    if(id != null) url += '/'+id;

    let data = null; 

    $.ajax({
        type: "GET",
        async: false,
        url: url,
        // data: {
        //     id_wilayah: $('#lokasi_filter').val(),
        //     jenis_kelamin: $('input[name=jenis_kelamin_filter]:checked').val(),
        // },
        dataType: "json",
        success: function (response) {
            console.log(response);
            if(response.code == 200){
                data = response.data
            } else {
                alert(response.message)
            }
        },
        error: function(xhr, textStatus, errorThrown){
            console.log(xhr, textStatus, errorThrown)
            alert('Error, ketika mengambil Voucher');
        }
    });

    return data;
}

// Kode Promo (Voucher)
function getKodePromo(id=null){
    let url = BASE_API+'kode-promo';
    if(id != null) url += '/'+id;

    let data = null; 

    $.ajax({
        type: "GET",
        async: false,
        url: url,
        // data: {
        //     id_wilayah: $('#lokasi_filter').val(),
        //     jenis_kelamin: $('input[name=jenis_kelamin_filter]:checked').val(),
        // },
        dataType: "json",
        success: function (response) {
            console.log(response);
            if(response.code == 200){
                data = response.data
            } else {
                alert(response.message)
            }
        },
        error: function(xhr, textStatus, errorThrown){
            console.log(xhr, textStatus, errorThrown)
            alert('Error, ketika mengambil Kode Promo');
        }
    });

    return data;
}



// #######################################################
// Render HTML
// #######################################################

function render_list_produk(){
    let id_kabupatenkota = $('#lokasi_filter').val();
    let gender = $('input[name=jenis_kelamin_filter]:checked').val();

    if(id_kabupatenkota == '') {
        $('#alert-button-provinsi').fadeIn({
            duration: 800
        });
        return;
    }

    let kabupatenkota = LIST_DATA.filter(kabupatenkota => kabupatenkota.id_kabupatenkota == id_kabupatenkota)

    $('#result_klinik').html(kabupatenkota[0].provinsi +' - '+ kabupatenkota[0].kabupatenkota);
    $('#result_gender').html((gender == 'pria')? 'Laki-laki' : 'Perempuan');

    let data = LIST_DATA.filter(data => data.id_kabupatenkota==id_kabupatenkota)

    let html = ``;

    data.map(klinik => {
        html += klinik.klinik.map(produk => {
                console.log('produk filter',produk)
                let produk_filter = produk.product.filter(produk => produk.gender == $('input[name=jenis_kelamin_filter]:checked').val() && produk.is_active)
                return html_produk(produk.name, produk_filter)
            }
        ).join(' '); 
    })

    if(html == ``) html = `<div class="col-12 mb-3"><center>Tidak Ada Layanan</center></div>`
    // if(IS_LOGIN && MY_PROVINSI != id_provinsi) html = `<div class="col-12 mb-3"><center>Tidak Ada Layanan</center></div>`
    // console.log('is_login myprovinsi', IS_LOGIN, MY_PROVINSI, id_provinsi)

    $('#container_filter').show();
    $('#alert-button-provinsi').hide();
    $('html,body').animate({
        scrollTop: $("#container_filter").offset().top},
        'slow');
    $('#container_list_produk').html(html);
}

function render_detail_produk(id){
    let data = getAllListProduk(id);
    
    $('.produk-owl').html(`<img src="${data.produk_gambar}" class="img-fluid">`)
    $('#detail_nama').html(data.nama)
    $('#detail_klinik').html(data.klinik.name)
    $('#detail_harga').html(data.harga)
    $('#detail_harga_fix').html(data.harga_fix)
    $('#detail_promo').html(data.promo)
    $('.deskripsi-layanan').html(data.deskripsi)
}

function render_kode_promo(id=null){
    let data = getKodePromo(id);
    let html = data.map(kode => html_kode_promo(kode));

    $('#container_kode_promo').html(html);
}

$('#form_filter').submit(function (e) { 
    render_list_produk();
    e.preventDefault();
    return false;
});

$('#btn_submit_filter').click(function (e) { 
    render_list_produk();
});