<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- Mobile Web-app fullscreen -->

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">

    <!-- Meta tags -->

    <meta name="description" content="Rumah Sunat dr Mahdian pendaftraan online">
    <meta name="author" content="DokterM">
    <link rel="icon" href="assets/assets/svg/favicon.ico">

    <!-- Title -->

    <title>Rumah Sunat dr Mahdian - Layanan Online</title>

    <!-- Vendor stylesheets -->

    <link rel="stylesheet" media="all" href="assets/css/vendor/animate.css" />
    <link rel="stylesheet" media="all" href="assets/css/vendor/font-awesome.css" />
    <link rel="stylesheet" media="all" href="assets/css/vendor/linear-icons.css" />
    <link rel="stylesheet" media="all" href="assets/css/vendor/owl.carousel.css" />
    <link rel="stylesheet" media="all" href="assets/css/vendor/jquery.lavalamp.css" />

    <!-- Template stylesheets -->

    <link rel="stylesheet" media="all" href="assets/css/style.css" />

    <!--HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!--[if lt IE 9]><script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script> <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

</head>

<body>
    <div class="loader">
        <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    <?php include('includes/header.php') ?>



    <?php 
        if(isset($_GET['pages'])){
            $pages = $_GET['pages'];
     
            switch ($pages) {
                case 'home':
                    include "pages/home.php";
                    break;

                case 'detail':
                    include "pages/detail.php";
                    break;

                case 'ringkasan':
                    include "pages/ringkasan.php";
                    break;

                case 'pembayaran':
                    include "pages/pembayaran.php";
                    break;
                          
                default:
                    case '404':
                    include "pages/404.php";
                    break;
            }
        }else{
            include "pages/home.php";
        }
 
    ?>

    <?php include('includes/footer.php') ?>
    

    <!-- Vendor Scripts -->

    <script src="assets/js/vendor/jquery.min.js"></script>
    <script src="assets/js/vendor/bootstrap.bundle.js"></script>
    <script src="assets/js/vendor/in-view.min.js"></script>
    <script src="assets/js/vendor/jquery.lavalamp.js"></script>
    <script src="assets/js/vendor/owl.carousel.js"></script>
    <script src="assets/js/vendor/rellax.js"></script>
    <script src="assets/js/vendor/wow.js"></script>
    <script src="assets/js/vendor/tabzy.js"></script>
    <script src="assets/js/vendor/isotope.pkgd.js"></script>

    <!-- Template Scripts -->

    <script src="assets/js/main.js"></script>
    <script src="assets/js/custom.js"></script>


    <script type="text/javascript">
        $( "#clickme" ).click(function() {
            var result = confirm("Apakah Anda yakin untuk menghapus?");

            if (result) {
                $( "#book" ).hide( "slow", function() {
            
                });
            } else {
                // Do nothing; they cancelled
            }
            
        });
         $( "#clickme-2" ).click(function() {
            var result = confirm("Apakah Anda yakin untuk menghapus?");

            if (result) {
                $( "#book-2" ).hide( "slow", function() {
            
                });
            } else {
                // Do nothing; they cancelled
            }
            
        });

        $( "#BuatAkun" ).click(function() {
            $("#LoginAccount").removeClass("show");
            $("#LoginAccount").hide();
            $(".modal-backdrop").removeClass("show");
            $(".modal-backdrop").hide();
        });
    </script>

    </body>

</html>
